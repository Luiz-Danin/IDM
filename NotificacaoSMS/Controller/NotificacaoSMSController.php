<?php
require_once dirname(__DIR__).'/Model/NotificacaoSMS.php';

class NotificacaoSMSController
{
    
    public function notifica_credito($conexao)
    {
        die('credito');
        set_time_limit(0);
        $matriculas_alunos = new NotificacaoSMS($conexao);
        $alunos = $matriculas_alunos->notifica_credito();
        
        if( count($alunos)>0 && !empty($alunos) )
        {
            $array_novo = array();
            $numero=' ';
            $curso='';
            
            foreach ($alunos as $aluno)
            {
                if($numero!=$aluno['ddd_celular'].$aluno['celular'])
                {
                    $array_novo[] = $aluno; 
                }
                else
                {
                    if($curso != $aluno['id_produto'])
                    {
                        $array_novo[] = $aluno;
                    }
                }
                
                $numero = $aluno['ddd_celular'].$aluno['celular'];
                $curso = $aluno['id_produto'];
            }
            
            $c = 0;
            foreach ($array_novo as $aluno)
            {
                $trim_ddd = trim($aluno['ddd']);
                $trim_telefone = trim($aluno['telefone']);
                
                $trim_ddd_celular = trim($aluno['ddd_celular']);
                $trim_celular = trim($aluno['celular']);
                
                if( !empty( $trim_ddd ) && !empty( $trim_telefone ) )
                {
                    $numero = $aluno['ddd'].$aluno['telefone'];
                }
                else if( !empty( $trim_ddd_celular ) && !empty( $trim_celular ) )
                {
                    $numero = $aluno['ddd_celular'].$aluno['celular'];
                }
                
                if( isset($numero) )
                {
                    $curso = ($aluno['id_produto']==8)?"Curso de Inglês":"Curso de Espanhol";
                    $curso = $this->limitarTexto($curso, 20);
                    $mensagem = "www.idmcursos.com.br informa: sua fatura gerada, por cartao de credito, para ".$curso." esta pendente há 1 dia. Acesse  o site e realize o pagamento.";
//                    $enviar = $this->enviar($numero, $mensagem);
                }
                
                if ($enviar->status == 1)
                {
                    $array['enviados'][$c] = isset($numero)?$numero:0;
                }
                else
                {
                    $array['falha'][$c] = isset($numero)?$numero:0;
                }
                
                $c++;
            }
            
            $msg_enviados = '';
            $msg_nao_enviado = '';
            echo "<pre>";
            print_r($array);
            echo "<br><br><br>";
            if( isset($array['enviados']) )
            {
                $msg_enviados = "Total enviados: ".count($array['enviados'])."<br>";
            }
            elseif (isset ($array['falha']) )
            {
                $msg_nao_enviado = "Total não enviados: ".count($array['falha']);
            }
            
            echo $msg_enviados;
            echo $msg_nao_enviado;
        
        }
        else
        {
            echo 'Não há dados para faturas pendentes por cartão de crédito';
        }
        
       
    }

    public function notifica_debito($conexao)
    {
        die('debito');
        set_time_limit(0);
        $matriculas_alunos = new NotificacaoSMS($conexao);
        $alunos = $matriculas_alunos->notifica_debito();
        
        if( count($alunos)>0 && !empty($alunos) )
        {
            $array_novo = array();
            $numero=' ';
            $curso='';
            
            foreach ($alunos as $aluno)
            {
                if($numero!=$aluno['ddd_celular'].$aluno['celular'])
                {
                    $array_novo[] = $aluno; 
                }
                else
                {
                    if($curso != $aluno['id_produto'])
                    {
                        $array_novo[] = $aluno;
                    }
                }
                
                $numero = $aluno['ddd_celular'].$aluno['celular'];
                $curso = $aluno['id_produto'];
            }
            
            $c = 0;
            foreach ($array_novo as $aluno)
            {
                
                $trim_ddd = trim($aluno['ddd']);
                $trim_telefone = trim($aluno['telefone']);
                
                $trim_ddd_celular = trim($aluno['ddd_celular']);
                $trim_celular = trim($aluno['celular']);
                
                if( !empty( $trim_ddd ) && !empty( $trim_telefone ) )
                {
                    $numero = $aluno['ddd'].$aluno['telefone'];
                }
                else if( !empty( $trim_ddd_celular ) && !empty( $trim_celular ) )
                {
                    $numero = $aluno['ddd_celular'].$aluno['celular'];
                }
                
                if( isset($numero) )
                {
                    $curso = ($aluno['id_produto']==8)?"Curso de Inglês":"Curso de Espanhol";
                    $curso = $this->limitarTexto($curso, 20);
                    $mensagem = "www.idmcursos.com.br informa: sua fatura gerada, via debito, para ".$curso." esta pendente há 1 dia. Acesse  o site e realize o pagamento.";
                    
//                    $enviar = $this->enviar($numero, $mensagem);
                }
                
                if ($enviar->status == 1)
                {
                    $array['enviados'][$c] = isset($numero)?$numero:0;
                }
                else
                {
                    $array['falha'][$c] = isset($numero)?$numero:0;
                }
                
                $c++;
            }
            
            $msg_enviados = '';
            $msg_nao_enviado = '';
            echo "<pre>";
            print_r($array);
            echo "<br><br><br>";
            if( isset($array['enviados']) )
            {
                $msg_enviados = "Total enviados: ".count($array['enviados'])."<br>";
            }
            elseif (isset ($array['falha']) )
            {
                $msg_nao_enviado = "Total não enviados: ".count($array['falha']);
            }
            
            echo $msg_enviados;
            echo $msg_nao_enviado;
        }
        else
        {
            echo 'Não há dados para Faturas pendentes por Débito';
        }
    }

    public function notifica_boleto($conexao)
    {
        die('boleto');
        set_time_limit(0);
        $matriculas_alunos = new NotificacaoSMS($conexao);
        $alunos = $matriculas_alunos->notifica_boleto();
        
        if( count($alunos)>0 && !empty($alunos) )
        {
            
            $array_novo = array();
            $numero=' ';
            $curso='';
            
            foreach ($alunos as $aluno)
            {
                if($numero!=$aluno['ddd_celular'].$aluno['celular'])
                {
                    $array_novo[] = $aluno; 
                }
                else
                {
                    if($curso != $aluno['id_produto'])
                    {
                        $array_novo[] = $aluno;
                    }
                }
                
                $numero = $aluno['ddd_celular'].$aluno['celular'];
                $curso = $aluno['id_produto'];
            }
            
            $c = 0;
            foreach ($array_novo as $aluno)
            {
                $trim_ddd = trim($aluno['ddd']);
                $trim_telefone = trim($aluno['telefone']);
                
                $trim_ddd_celular = trim($aluno['ddd_celular']);
                $trim_celular = trim($aluno['celular']);
                
                if( !empty( $trim_ddd ) && !empty( $trim_telefone ) )
                {
                    $numero = $aluno['ddd'].$aluno['telefone'];
                }
                else if( !empty( $trim_ddd_celular ) && !empty( $trim_celular ) )
                {
                    $numero = $aluno['ddd_celular'].$aluno['celular'];
                }
                
                if( isset($numero) )
                {
                    //$aluno['cod_interme'] = '44c5d9e824170c2d18cca9554c2059e2';
                    
                    $response = $this->get_transacao_tray($aluno['cod_interme']);
                    $response_linha = (array)$response->data_response->transaction->payment->linha_digitavel;
                    
                    $codbarras = $response_linha[0];
                    $codbarras = str_replace(array('.', ' ', '-', '_', '*','+','(',')'), '', $codbarras);
                    
                    $curso = ($aluno['id_produto']==8)?"Curso de Inglês":"Curso de Espanhol";
                    $curso = $this->limitarTexto($curso, 20);
                    
                    $codbarras = str_replace(array('.', ' ', '-', '_', '*','+','(',')'), '', $codbarras);
                
                    $mensagem = "www.idmcursos.com.br informa: sua fatura gerada para ".$curso." está pendente há 3 dias. Boleto: ".$codbarras;
                    
                    //$enviar = $this->enviar($numero, $mensagem);
                }
                
                if ($enviar->status == 1)
                {
                    $array['enviados'][$c] = isset($numero)?$numero:0;
                }
                else
                {
                    $array['falha'][$c] = isset($numero)?$numero:0;
                }
                
                $c++;
            }
            
            $msg_enviados = '';
            $msg_nao_enviado = '';
            echo "<pre>";
            print_r($array);
            echo "<br><br><br>";
            if( isset($array['enviados']) )
            {
                $msg_enviados = "Total enviados: ".count($array['enviados'])."<br>";
            }
            elseif (isset ($array['falha']) )
            {
                $msg_nao_enviado = "Total não enviados: ".count($array['falha']);
            }
            
            echo $msg_enviados;
            echo $msg_nao_enviado;
        }
        else
        {
            echo 'Não há dados para Faturas pendentes por Boleto';
        }
        
        
    }

    public function matricula_sem_geracao_fatura($conexao)
    {
        die('geracao fatura');
        set_time_limit(0);
        $matriculas_alunos = new NotificacaoSMS($conexao);
        $alunos = $matriculas_alunos->matricula_sem_geracao_fatura();
        
        if( count($alunos)>0 && !empty($alunos) )
        {
            $c = 0;
            foreach ($alunos as $aluno)
            {
                $mensagem = "www.idmcursos.com.br informa: sua inscricao no IDM Cursos foi realizada com sucesso. Acesse o site e gere sua fatura.";
                
                $trim_ddd = trim($aluno['ddd']);
                $trim_telefone = trim($aluno['telefone']);
                
                $trim_ddd_celular = trim($aluno['ddd_celular']);
                $trim_celular = trim($aluno['celular']);
                
                if( !empty( $trim_ddd ) && !empty( $trim_telefone ) )
                {
                    $numero = $aluno['ddd'].$aluno['telefone'];
                }
                else if( !empty( $trim_ddd_celular ) && !empty( $trim_celular ) )
                {
                    $numero = $aluno['ddd_celular'].$aluno['celular'];
                }
                
                if( isset($numero) )
                {
//                    $enviar = $this->enviar($numero, $mensagem);
                }
                
                if ($enviar->status == 1)
                {
                    $array['enviados'][$c] = isset($numero)?$numero:0;
                }
                else
                {
                    $array['falha'][$c] = isset($numero)?$numero:0;
                }
                
                $c++;
            }
            
            $msg_enviados = '';
            $msg_nao_enviado = '';
            echo "<pre>";
            print_r($array);
            echo "<br><br><br>";
            if( isset($array['enviados']) )
            {
                $msg_enviados = "Total enviados: ".count($array['enviados'])."<br>";
            }
            elseif (isset ($array['falha']) )
            {
                $msg_nao_enviado = "Total não enviados: ".count($array['falha']);
            }
            
            echo $msg_enviados;
            echo $msg_nao_enviado;
        
        }
        else
        {
            echo 'Não há dados para Matricula sem geração de Fatura';
        }
        
    }

    public function curso_liberado($conexao)
    {
        die('curso liberado');
        set_time_limit(0);
        $cursos_alunos = new NotificacaoSMS($conexao);
        
        $alunos = $cursos_alunos->curso_liberado();
        
        if( count($alunos)>0 && !empty($alunos) )
        {
            
            $c = 0;
            foreach ($alunos as $aluno)
            {
                $curso = ($aluno['id_curso']==635)?'Inglês': ($aluno['id_curso']==636)? 'Espanhol':'';
                
                $trim_ddd = trim($aluno['ddd']);
                $trim_telefone = trim($aluno['telefone']);
                
                $trim_ddd_celular = trim($aluno['ddd_celular']);
                $trim_celular = trim($aluno['celular']);
                
                if( !empty( $trim_ddd ) && !empty( $trim_telefone ) )
                {
                    $numero = $aluno['ddd'].$aluno['telefone'];
                }
                else if( !empty( $trim_ddd_celular ) && !empty( $trim_celular ) )
                {
                    $numero = $aluno['ddd_celular'].$aluno['celular'];
                }
                
                $curso = $this->limitarTexto($curso, 30);
                $mensagem = "www.idmcursos.com.br informa: seu acesso para o Curso de ".$this->retirar_acentos( trim($curso) )." encontra-se liberado.";
                if( isset($numero) )
                {
//                    $enviar = $this->enviar($numero, $mensagem);
                }
                
                if ($enviar->status == 1)
                {
                    $array['enviados'][$c] = isset($numero)?$numero:0;
                }
                else
                {
                    $array['falha'][$c] = isset($numero)?$numero:0;
                }
                
                $c++;
            }
            
            $msg_enviados = '';
            $msg_nao_enviado = '';
            echo "<pre>";
            print_r($array);
            echo "<br><br><br>";
            if( isset($array['enviados']) )
            {
                $msg_enviados = "Total enviados: ".count($array['enviados'])."<br>";
            }
            elseif (isset ($array['falha']) )
            {
                $msg_nao_enviado = "Total não enviados: ".count($array['falha']);
            }
            
            echo $msg_enviados;
            echo $msg_nao_enviado;
        }
        else
        {
            echo 'Não há dados para Cursos Matriculados';
        }
    }
    
    private function enviar($celular, $mensagem)
    {
        $ch = curl_init();

        $post = array(
            'token' => 'XHJKORWT0L37TQ83XAONTR5US2LPJEL3',
            'chave' => '8VG8LLRBKN2JPVUQDGSSX8LT05YABSTR',
            'instituicao' => 'IDMCURSOS',
            'celular' => $celular,
            'mensagem' => $mensagem
        );

        curl_setopt($ch, CURLOPT_URL, 'https://www.ovum.com.br/api_sms/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

        $requisicao = curl_exec($ch);
        return json_decode($requisicao);
        curl_close ($ch); 
    }
    
    private function get_transacao_tray($codigo)
    {
        $params['token_transaction'] = $codigo;

        $params['token_account'] = 'a44677dac7cba94';   
        
        $urlPost = "https://api.sandbox.traycheckout.com.br/v2/transactions/get_by_token";
        
        ob_start();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlPost);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt ($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_exec($ch);

        /* XML de retorno */
        $resposta = simplexml_load_string(ob_get_contents());
        ob_end_clean();
        curl_close($ch);
        return $resposta;
    }
    
    private function retirar_acentos($mensagem)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(ç)/","/(Ç)/"),explode(" ","a A e E i I o O u U n N c C"),$mensagem);
    }
    
    private function alterar_caracteres($mensagem)
    {
        return preg_replace(array("/(á)/", "/(à)/", "/(ã)/", "/(â)/", "/(Á)/", "/(À)/", "/(Ã)/", "/(Â)/", "/(é)/", "/(ê)/", "/(É)/", "/(Ê)/", "/(í)/", "/(Í)/", "/(ó)/", "/(õ)/", "/(ô)/", "/(Ó)/", "/(Õ)/", "/(Ô)/", "/(ú)/", "/(ü)/", "/(Ú)/", "/(Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"),
            explode(" ","&aacute; &agrave; &atilde; &acirc; &Aacute; &Agrave; &Atilde; &Acirc; &eacute; &ecirc; &Eacute; &Ecirc; &iacute; &Iacute; &oacute; &otilde; &ocirc; &Oacute; &Otilde; &Ocirc; &uacute; &uuml; &Uacute; &Uuml; &ntilde; &Ntilde; &ccedil; &Ccedil;"),$mensagem);
    }
    
    private  function limitarTexto($texto, $limite)
    {
       $contador = strlen($texto);
       
       if ($contador >= $limite)
       {
           $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' '));
           return $texto;
       }
       else
       {
           return $texto;
       }
   }
    
    
}