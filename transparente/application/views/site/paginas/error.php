<?php //$this->load->view('aluno/includes/head.php'); ?>
<style>
    #main {
        height: 100vh;
        background-color: #007b5e;
    }
</style>
<div class="d-flex justify-content-center align-items-center" id="main">
    <h1 class="mr-3 pr-3 align-top border-right inline-block align-content-center">Erro encontrado</h1>
    <div class="inline-block align-middle">
        <h2 class="font-weight-normal lead" id="desc"><?php echo ( isset($mensagem) ) ? $mensagem : "404!";?></h2>
    </div>
</div>
<?php die;?>