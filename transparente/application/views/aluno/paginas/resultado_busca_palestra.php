<?php $this->load->view('aluno/includes/head.php'); ?>

<style>
    /* Tabs*/
section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}
#tabs{
	background: #fff;
    color: #eee;
}
#tabs h6.section-title{
    color: #212529;
}

#tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #f3f3f3;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 4px solid !important;
    font-size: 20px;
    font-weight: bold;
}
#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #212529;
    font-size: 20px;
}

label
{
    color:#000;
}

.tab-content>.active 
{
    color:#000;
}

/*botão assistir video*/
.page {
    padding: 15px 0 0;
}

.bmd-modalButton {
/*    display: block;
    margin: 15px auto;
    padding: 5px 15px;*/
}

.close-button {
    overflow: hidden;
}

.bmd-modalContent {
    box-shadow: none;
    background-color: transparent;
    border: 0;
    margin-top: 200px;
}

.bmd-modalContent .close {
    font-size: 30px;
    line-height: 30px;
    padding: 7px 4px 7px 13px;
    text-shadow: none;
    opacity: .7;
    color:#f00;
}

.bmd-modalContent .close span {
    display: block;
}

.bmd-modalContent .close:hover,
.bmd-modalContent .close:focus {
    opacity: 1;
    outline: none;
}

.bmd-modalContent iframe {
    display: block;
    margin: 0 auto;
}

.modal-footer
{
    background-color: #f5f5f5;
    border-top: 1px solid #ddd;
    box-shadow: inset 0 1px 0 #ffffff;
}
</style>
<!-- Tabs -->
<section id="tabs">
	<div class="container-fluid">
		<!--<h6 class="section-title h1">Palestras</h6>-->
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Palestras Disponíveis</a>
						<!--<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Cadastrar Palestras</a>-->
						<!--<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Palestras Disponíveis</a>-->
						<!--<a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">About</a>-->
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <p style="margin-left:15px; margin-right: 15px"> <a class="btn btn-outline-success" href="<?php echo base_url().'pagina_cadastro_palestra'; ?>">Cadastrar sua Palestra <i class="fa fa-video-camera" aria-hidden="true"></i></a></p>
                                        
                                        <div class="container-fluid">
                                            <?php

                                                if( count($palestras_busca)>0 )
                                                    {
                                            ?>
                                            <div class="exibe-palestra-matricula">
                                                
                                           
                                                <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Palestra</th>
                                                        <th>Descrição</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                            <?php
                                                        foreach ($palestras_busca as $disponiveis)
                                                        {
                                                            
                                                            if( isset($disponiveis['matriculado']) )
                                                            {
                                                                echo '<tr>';
                                                                    echo '<td>'.$disponiveis['nome'].'</td>';
                                                                    echo '<td>'.$disponiveis['descricao'].'</td>';
                                                                    echo '<td><button id="'.codifica($disponiveis['id_palestra']).'" value="'.codifica($disponiveis['id_historico']).'" type="button" class="btn btn-outline-primary bmd-modalButton assistir" data-toggle="modal" data-bmdsrc="'.$disponiveis['link_palestra'].'" data-bmdwidth="740" data-bmdheight="580" data-target="#myModal" data-bmdvideofullscreen="true">Assistir</button></td>';
                                                                    //echo '<td><button type="button" class="btn btn-outline-primary">Certificado</button></td>';
                                                                echo '</tr>';
                                                            }
                                                            else
                                                            {
                                                                echo '<tr>';
                                                                    echo '<td>'.$disponiveis['nome'].'</td>';
                                                                    echo '<td>'.$disponiveis['descricao'].'</td>';
                                                                    echo '<td>'
                                                                    . '<div class="btn-matricula">'
                                                                    . '<button type="button" class="btn btn-outline-dark matricula-palestra" value="'.codifica($disponiveis['id_palestra']).'">Inscrever</button>'
                                                                    . '</div>'
                                                                    . '</td>';
                                                                echo '<td></td>';
                                                                echo '</tr>';
                                                            }
                                                        }
                                                      echo '</tbody>';
                                                    echo '</table>';
                                                    echo '</div>';
                                                    }
                                                    else
                                                    {
                                                        echo '<div class="alert alert-dark" role="alert">
                                                                Nenhum resultado encontrado. Que tal cadastrar sua Palestra? <a href="'.base_url().'pagina_cadastro_palestra" class="alert-link">Cadastrar Palestra</a>.
                                                              </div>';
                                                    }
                                                ?>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
<!--                                        <form method="post" enctype="multipart/form-data" action="<?php //echo base_url();?>send">
                                            <br>
                                            <br>
                                            <label for="title">Titulo do Vídeo:</label>
                                            <input class="form form-control"  type="text" name="video[titulo]" maxlength="100" value="" />
                                            <br>
                                            <label for="description">Descrição para o Vídeo:</label>
                                            <textarea class="form form-control"  name="video[descricao]" cols="20" rows="1" maxlength="5000" ></textarea>
                                            <br>
                                            <label for="tags">Tags: [ separa por ';' ]</label>
                                            <input class="form form-control"  type="text" name="video[tags]" value="" />
                                            <br>
                                            <label for="tags">Classificação indicativa: (Op)</label><input class="form form-control" type="text" name="video[classificacao_indicativa]" value="">
                                            <br>
                                            <label for="tags">URL de Retorno: (Op)</label>
                                            <input class="form form-control" type="text" name="video[url_retorno]" value="">
                                            <br>
                                            <label for="file">Arquivo de Video:</label> 
                                            <input  class="form form-control" type="file" name="arquivo" >
                                            <br>
                                            <button class="btn btn-primary btn-block" type="submit">Enviar</button>
                                        </form>-->
                                    </div>
					<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
						
                                            
                                        </div>
					</div>
<!--					<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
						Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
					</div>-->
				</div>
			
			</div>
		</div>
	</div>
</section>
<!-- ./Tabs -->
<?php 
     if (isset($links))
     {
        echo $links; 
     }
?>
<footer>
	<div class="modal fade" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content bmd-modalContent">

				<div class="modal-body">
          
          <div class="close-button">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="embed-responsive embed-responsive-16by9">
					            <iframe class="embed-responsive-item" frameborder="0"></iframe>
          </div>
          <div class="modal-footer">
              <div class="text-avaliacao"></div>
              <div class="btn-avaliacao">
            <!--<button class="star_palestra" value=""><i data="" value="1" class="fa fa-star-o"></i></button>
                <button class="star_palestra" value=""><i data="" value="2" class="fa fa-star-o"></i></button>
                <button class="star_palestra" value=""><i data="" value="3" class="fa fa-star-o"></i></button>
                <button class="star_palestra" value=""><i data="" value="4" class="fa fa-star-o"></i></button>
                <button class="star_palestra" value=""><i data="" value="5" class="fa fa-star-o"></i></button>-->
              </div>
          </div>
				</div>

			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
  
      </footer>

<script>
//$("button.btn.btn-outline-dark.assistir").load("check_avaliacao", function(response){
           //var id = $(this).attr("id");
        //alert(id);
        
//});

$("button.btn.btn-outline-primary.assistir").on( 'click',function(e){
    
    var id = $(this).attr("id");
    $("button.star_palestra").val(id);

    var id = $(this).attr("id");
    var id_h = $(this).val();
    $("i.fa.fa-star-o").attr("data", id_h);

    //alert(id_h);

    var base_url = "<?php echo base_url(); ?>";
        
        $.ajax({
        method: "POST",
        url: base_url+"check_avaliacao",
        dataType: "json",
        data: { id:id, id_h:id_h},
        beforeSend: function() {
            
        },
        success:function (response) {
            //console.log(id);
            //console.log(id_h);
            
            if(response.status==1)
            {
                $(".text-avaliacao").text("Obrigado por avaliar:");

                
                switch(response.dados.qualificacao) {
                    case "20":
                      $(".btn-avaliacao").html('<button data="" class="star_palestra" value=""><i style="color:#F4B30A" data="" value="20" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="40" class="fa fa-star-o"></i></button><button class="star_palestra" value=""><i data="" value="60" class="fa fa-star-o"></i></button><button class="star_palestra" value=""><i data="" value="80" class="fa fa-star-o"></i></button><button class="star_palestra" value=""><i data="" value="100" class="fa fa-star-o"></i></button>');
                      break;
                    case "40":
                      $(".btn-avaliacao").html('<button data="" class="star_palestra" value=""><i style="color:#F4B30A" data="" value="20" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="40" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="60" class="fa fa-star-o"></i></button><button class="star_palestra" value=""><i data="" value="80" class="fa fa-star-o"></i></button><button class="star_palestra" value=""><i data="" value="5100" class="fa fa-star-o"></i></button>');
                      break;
                    case "60":
                      $(".btn-avaliacao").html('<button data="" class="star_palestra" value=""><i style="color:#F4B30A" data="" value="20" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="40" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="60" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="80" class="fa fa-star-o"></i></button><button class="star_palestra" value=""><i data="" value="100" class="fa fa-star-o"></i></button>');
                      break;
                    case "80":
                      $(".btn-avaliacao").html('<button data="" class="star_palestra" value=""><i style="color:#F4B30A" data="" value="20" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="40" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="60" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="80" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="100" class="fa fa-star-o"></i></button>');
                      break;
                    case "100":
                      $(".btn-avaliacao").html('<button data="" class="star_palestra" value=""><i style="color:#F4B30A" data="" value="20" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="40" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="60" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="80" class="fa fa-star"></i></button><button class="star_palestra" value=""><i style="color:#F4B30A" data="" value="100" class="fa fa-star"></i></button>');
                      break;
                    default:
                      $(".btn-avaliacao").html('<button value=""><i data="" value="1" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="2" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="3" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="4" class="fa fa-star"></i></button><button class="star_palestra" value=""><i data="" value="5" class="fa fa-star"></i></button>');
                }
                
                //$(".btn-certificado-participante").html('<button type="button" class="btn btn-outline-primary">Certificado</button>');
                //$(".btn-certificado-participante").html('<button type="button" class="btn btn-outline-primary">Certificado</button>');
            }
            else if(response.status==0)
            {
              $(".text-avaliacao").text("Avalie, clique na estela:");
              $("button.star_palestra").val(id);
              $(".btn-avaliacao").html('<button class="star_palestra" value=""><i data="" value="20" class="fa fa-star-o avaliado"></i></button><button class="star_palestra" value=""><i data="" value="40" class="fa fa-star-o avaliado"></i></button><button class="star_palestra" value=""><i data="" value="60" class="fa fa-star-o avaliado"></i></button><button class="star_palestra" value=""><i data="" value="80" class="fa fa-star-o avaliado"></i></button><button class="star_palestra" value=""><i data="" value="100" class="fa fa-star-o avaliado"></i></button>');
            }
        },
        complete: function (data) {
            //completeAjax(); 
            
            $("button.star_palestra").val(id);
            $("i.fa.fa-star-o").attr("data", id_h);
            
            /*$("i.fa.fa-star-o").on( 'click',function(e){
    
                var base_url = "<?php //echo base_url(); ?>";

                    var valor = $(this).attr('value');
                var id_h = $("button.btn.btn-outline-dark.assistir").val();
                
                //alert(id_h);
                //var id = $("button.btn.btn-outline-dark.assistir").data();
                var id = $("button.star_palestra").val();
                alert(id);
                
                $.ajax({
                    method: "POST",
                    url: base_url+"avaliar",
                    //dataType: "html",
                    data: { valor:valor, id_h:id_h, id:id},
                    beforeSend: function() {

                    },
                    success:function (response) {

                    },
                    complete: function (data) {
                        //completeAjax(); 
                    }
                });
            });*/
            
        }
    });
        
});

//button.close
$("button.close").on( 'click',function(e){
    document.location.href = document.location.href;
});
</script>

<script>
//$("i.fa.fa-star-o").on( 'click',function(e){
$(document).on('click','i.fa.fa-star-o.avaliado',function(){ 
    
    var base_url = "<?php echo base_url(); ?>";
    
    var valor = $(this).attr('value');
    
    //var id_h = $("button.btn.btn-outline-dark.assistir").val();
    var id_h = $("i.fa.fa-star-o").attr("data");
    
    //var id = $("button.btn.btn-outline-dark.assistir").data();
    var id = $("button.star_palestra").val();
    //alert(id_h);
    $.ajax({
        method: "POST",
        url: base_url+"avaliar",
        //dataType: "html",
        data: { valor:valor, id_h:id_h, id:id},
        beforeSend: function() {
            
        },
        success:function (response) {
            alert("Obrigado por avaliar"); 
        
            $('#myModal').modal('hide'); 
            document.location.href = document.location.href;
        },
        complete: function (data) {
            //completeAjax(); 
        }
    });
});
</script>

<script>
$(document).ready(function(){
    
$("button.matricula-palestra").on('click', function(e){
     //event.preventDefault();

          //alert("fjdki");
    //var valor = $(this).val();
    var base_url = "<?php echo base_url(); ?>";
    var valor = $(this).attr('value');
          
    $.ajax({
        method: "POST",
        url: base_url+"matricula",
        dataType: "html",
        data: { valor:valor },
        beforeSend: function() {
            //$('.enviar-curso').prop('disabled', true);
            //$('button[value="='+valor+'"]').prop('disabled', true);
            
            $('button.btn.btn-outline-dark.matricula-palestra').prop('disabled', true);
            
            //$('.btn-matricula').html('<button class="btn btn-success" disabled><span class="spinner-border spinner-border-sm"></span>Aguarde..</button>');
        },
        success:function (response) {

            //$(".exibe-palestra-matricula").hide().html(response).fadeIn("slow");
            //$(".exibe-palestra-matricula").load();
            //document.location.href = document.location.href;
            document.location.href = base_url+"palestra_matriculada";
        },
        complete: function (data) {
            //completeAjax(); 
        }
    });
          
    });
    
    });
</script>
<script>
$(function () {
  $('[data-toggle="popover"]').popover()
})
    
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<script>

(function($) {
    
    $.fn.bmdIframe = function( options ) {
        var self = this;
        var settings = $.extend({
            classBtn: '.bmd-modalButton',
            defaultW: 840,
            defaultH: 560
        }, options );
      
        $(settings.classBtn).on('click', function(e) {
          var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;
          
          var dataVideo = {
            'src': $(this).attr('data-bmdSrc'),
            'height': $(this).attr('data-bmdHeight') || settings.defaultH,
            'width': $(this).attr('data-bmdWidth') || settings.defaultW
          };
          
          if ( allowFullscreen ) dataVideo.allowfullscreen = "";
          
          // stampiamo i nostri dati nell'iframe
          $(self).find("iframe").attr(dataVideo);
        });
      
        // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale è chiusa
        this.on('hidden.bs.modal', function(){
          $(this).find('iframe').html("").attr("src", "");
        });
      
        return this;
    };
  
})(jQuery);




jQuery(document).ready(function(){
  jQuery("#myModal").bmdIframe();
});
</script>

<script>
    /*$(document).on('click','button.star_palestra',function(){ 
        alert("Obrigado por avaliar"); 
        
        $('#myModal').modal('hide');
        document.location.href = document.location.href;
    });*/
</script>

<script>
$(document).ready(function(){
    
    /*function highlightStar(obj, id) {
		removeHighlight(id);
		$('.demo-table #tutorial-' + id + ' i').each(function(index) {
			$(this).addClass('highlight');
			if (index == $('.demo-table #tutorial-' + id + ' li').index(obj)) {
				return false;
			}
		});
	}*/ 
    
        //$(document).on('mouseover','button.star_palestra',function(){ 
            //$("button.")attr("id").val("color-star");
            //$(this).attr('id',  "color-star");
            
            //$("#color-star:first").children().css("color", "#F4B30A");
    
        //});
    
    
    //$(document).on('mouseout','button.star_palestra',function(){ 
            //$("button.star_palestra").find( "li" ).css( "background-color", "red" );
            
            
            //$("button.star_palestra").children().css("color", "#F4B30A");
    
        //});
    
});
</script>