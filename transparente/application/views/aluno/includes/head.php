<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Palestras</title>
        <base href="<?= base_url() ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="public/css/fontawersome/css/font-awesome.min.css">

        <script src="public/js/jquery/jquery-3.4.1.min.js"></script>
        <script src="public/js/popper.min.js"></script>
        <script src="public/js/bootstrap/js/bootstrap.min.js"></script>
        <script src="public/js/typeahead.js"></script>
        <style>
            html{
                overflow-x: hidden;
            }
            .navbar-dark .navbar-nav .nav-link
            {
                color:#FFF;
            }
        </style>
    </head>
    <header>
<!--        <nav class="navbar navbar-expand-lg navbar-light bg-light">-->
        <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
            <!--<a class="navbar-brand" href="<?php //echo base_url(); ?>curso_matriculado"><img title="IDM Cursos - Cursos de Idiomas" style="width: 65px;" class="logo_palestra" src="<?php //echo base_url();?>public/img/logo-idm.png"></a>-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>curso_matriculado"><i class="fa fa-graduation-cap"></i>Cursos de Idiomas Matriculados</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>curso_disponivel"><i class="fa fa-book"></i>
                                Cursos de Idiomas Disponiveis</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>