<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Curso/index';
$route['inicio'] = 'Curso/index';
$route['inicio/(:any)'] = 'Curso/index/$1';

$route['update'] = 'Curso/update_nome';

$route['gera_certificado/(:any)/(:any)/(:any)/(:any)'] = 'Curso/gera_certificado/$1/$2/$3/$4';

$route['retorno_pagamento'] = 'RetornoPagamento';

$route['pagamento'] = 'Curso/pagamento';

$route['matricula'] = 'Historico/realiza_matricula';

$route['codifica_dados'] = 'Curso/codifica_dados_enviados';
$route['consulta/(:any)'] = 'Curso/consulta/$1';

$route['aceite'] = 'Curso/do_aceite';


$route['curso_disponivel/(:any)'] = 'Curso/curso_disponivel/$1';
$route['curso_disponivel'] = 'Curso/curso_disponivel';

//curso_matriculado
$route['curso_matriculado/(:any)'] = 'Curso/curso_matriculado/$1';
$route['curso_matriculado'] = 'Curso/curso_matriculado';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;