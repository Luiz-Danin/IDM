<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RetornoPagamento extends CI_Controller
{
    public function index()
    {
        $resposta = $this->input->post("resposta", TRUE);
        
        $resposta = $this->security->xss_clean($resposta);
        
        $resposta_decodificada = json_decode($resposta);

        $cod_status = $resposta_decodificada->cod_status;
        $pedido = $resposta_decodificada->pedido;
        
        if( $cod_status==3 )
        {
            $this->load->model("Curso_model","curso");
            $this->curso->update_situacao_pagamento($pedido, $cod_status);
        }
    }
}