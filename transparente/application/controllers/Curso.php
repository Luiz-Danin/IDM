<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso extends CI_Controller
{
    private $fpdf;
    
    public function __construct()
    {
        parent::__construct();
 
        // load Session Library
        $this->load->library('session');
        // load url helper
        $this->load->helper('url');
    }
    
    public function index()
    {
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $this->load->model("Curso_model","curso");
        $usuario = $this->curso->get_usuario_id_unico($usuario_id_unico);
        
        $cursos = $this->get_historico($usuario_id_unico);
        
        $dados = [
                    'cursos'=>$cursos,
                    'identificador_usuario'=>$usuario[0]['id_aluno']
                 ];
        
        $this->load->view('aluno/paginas/curso_matriculado', $dados);
    }
    
    private function get_historico($usuario_id_unico)
    {
        $palestra_usuario = $this->curso->get_usuario_id_unico($usuario_id_unico);
        
        $historico = $this->curso->get_historico($palestra_usuario[0]['id_aluno']);
        
        $this->load->library("pagination");
        
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        
        $config = array();
        //
        $config['full_tag_open'] = '<ul style="margin-left:15px; margin-right: 15px" class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Proxima&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="'.base_url().'inicio/'.$page.'" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        //
        $config["base_url"] = base_url() . "inicio";
        $config["total_rows"] = count( $historico );
        $config["per_page"] = PER_PAGE;
        $config["uri_segment"] = 2;
        
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        //
        $historico_2 = $this->curso->get_historico_2($palestra_usuario[0]['id_aluno'], $config["per_page"], $page);
        return array('id_aluno'=>$palestra_usuario[0]['id_aluno'], 'historico'=>$historico_2, 'links'=>$links);
    }
    
    private function consulta_certificado()
    {
        $this->pl->add_certificado( $id_usuario, $id_historico );
    }

    private function view_pagamento_palestrante($dados)
    {
        $this->load->view('aluno/paginas/pagamento', $dados);
    }
    
    private function view_pagamento_participante($dados)
    {
        $this->load->view('aluno/paginas/pagamento_participante', $dados);
    }
    
    public function pagamento()
    {
        
        $id_palestra_cod = $this->input->post("id_curso", TRUE);
        $id_curso = decodifica($id_palestra_cod);
        
        $id_historico_cod = $this->input->post("id_historico", TRUE);
        $id_historico = decodifica($id_historico_cod);
        
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $this->load->model("Curso_model","curso");
        $aluno = $this->curso->get_usuario_id_unico($usuario_id_unico);
        $id_aluno = $aluno[0]['id_aluno'];
        
        if ( !empty($id_curso) && !empty($id_aluno) && !empty($id_historico) )
        {
            
            $id_transacao = $this->gera_id_transacao($id_aluno);
            $descricao = 'Certificado do IDMCURSOS';
            $resposta = $this->gera_transacao_ovum($aluno[0], $id_transacao, $descricao, 10);
            $resposta_decodificada = json_decode($resposta, true);
            
            if( isset( $resposta_decodificada['situacao'] ) && ($resposta_decodificada['situacao']=='Sucesso') )
            {
                $codigo_intermediador = $resposta_decodificada['dados'][0]['transacao'];
                $this->curso->gera_pagamento($id_aluno, $id_historico, $id_transacao, $id_curso);
                
                $dados = [
                            'identificador'=>$resposta_decodificada['dados'][0]['identificador']
                         ];
                
                $this->view_pagamento_participante($dados);
            }
            else
            {
                $dado =  ['mensagem'=>'Falha do intermedador ao processar a transacao'];
                $this->erro($dado);
            }
        }
        else
        {
            $dado =  ['mensagem'=>'Falta parametros para o processamento do pagamento '];
            $this->erro($dado);
        }
        
    }

    
    public function cadastro_palestra()
    {
        $this->load->model("Palestra_model","pl");
        
        $this->load->view('aluno/paginas/pagina_cadastro_palestra');
    }

    private function gera_id_transacao($id_aluno)
    {
        $posfix = strtoupper(substr(md5(uniqid() . $id_aluno ), 0, 12));

        return "IDM" . $posfix;
    }
    
    private function gera_transacao_ovum($aluno, $transacao, $descricao, $valor)
    {
        if( empty($aluno['nome']) )
        {
            $aluno['nome'] = 'Não informado';
        }
        
        $url = PAGAMENTO_URL;
        $data["autenticacao"]["chave"] = IDM_CHAVE;
        $data["autenticacao"]["token"] = IDM_TOKEN;
        $data["cliente"]["id"] = $aluno['id_aluno'];
        $data["cliente"]["nome"] = $aluno['nome'];
        $data["cliente"]["email"] = $aluno['email'];
        $data["transacao"]["tipo"] = "S";
        $data["transacao"]["descricao"] = $descricao;
        $data["transacao"]["valor"] = $valor;
        $data["transacao"]["pedido"] = $transacao;
        $data["loja"]["url_retorno"] = base_url()."retorno_pagamento";
        
        $data = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        return curl_exec($ch);
    }
    
    public function update_nome()
    {
       $nome =  $this->input->post("nome", TRUE);
       $nome = filter_var($nome, FILTER_SANITIZE_STRING);
       $id_unico = $this->session->userdata('usuario_id_unico');

       if( !empty( trim($nome) ) && !empty( trim($id_unico) ) )
       {
           $this->load->model("Curso_model","curso");
         
           $is_update = $this->curso->update_nome($nome, $id_unico);
           
           if($is_update)
           {
               echo json_encode(['status'=>TRUE]);
           }
           else
           {
               echo json_encode(['status'=>FALSE]);
           }
       }
    }

    public function curso_disponivel()
    {
        $this->load->model("Curso_model","curso");
        
        $id_unico = $this->session->userdata('usuario_id_unico');
         
        $aluno = $this->curso->get_usuario_id_unico($id_unico);
        $nome_aluno = (isset($aluno[0]['nome']))?$aluno[0]['nome']:'';
        
        $this->load->library("pagination");
        
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        
        $config = array();
        //
        $config['full_tag_open'] = '<ul style="margin-left:15px; margin-right: 15px" class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Proxima&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="'.base_url().'curso_disponivel/'.$page.'" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        //
        
        $config["base_url"] = base_url() . "curso_disponivel";
        $config["total_rows"] = count( $this->curso->get_cursos_disponiveis() );
        $config["per_page"] = PER_PAGE;
        $config["uri_segment"] = 2;
        
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        //
        $result = $this->curso->get_cursos_disponiveis_2($config["per_page"], $page);

        $data = ['cursos'=>$result, 'nome_aluno'=>$nome_aluno, 'links'=>$links];    

        $this->load->view('aluno/paginas/curso_disponivel', $data);
    }
    
    private function palestra_historico_usuario($id_aluno,  $limit=null, $start=null)
    {
        
        if( !empty( trim($id_aluno) ) )
        {
            
            $historico_usuario = $this->curso->get_historico_usuario_2($id_aluno, $limit, $start);
            $palestra_historico_usuario = [];
            
            for ($i = 0 ; $i<count($historico_usuario) ; $i++)
            {
                $result = $this->curso->get_palestra_by_id_palestra($historico_usuario[$i]['id_curso']);
                
                $result[0]['pago'] = $this->curso->check_pagamento_historico($historico_usuario[$i]['id_aluno'], $historico_usuario[$i]['id_historico']);
                
                if($result[0]['pago'])
                {
                    
                    $cod_autenticacao = $this->gera_cod_autenticacao($historico_usuario[$i]['id_aluno']);
                    
                    $id = $historico_usuario[$i]['id_aluno'] + $historico_usuario[$i]['id_historico'];
                    $identificador = $this->gera_identificador($id);
                    
                    $certificado = $this->curso->certificado_participante($historico_usuario[$i]['id_aluno'], $historico_usuario[$i]['id_historico'], $cod_autenticacao, $identificador);
                    
                    if( $certificado['status'] )
                    {
                        $recuperar_arquivo_ovumdocs =  $this->recuperar_arquivo_ovumdocs( $certificado['codigo_autenticacao'] );
                        //echo"<pre>";var_dump($recuperar_arquivo_ovumdocs);die;
                        
                        $status = $recuperar_arquivo_ovumdocs['status'];
                        
                        if($status!=1)
                        {
                            
                            $i=0;
                            while ( $i<4 )
                            {
                                $recuperar_arquivo_ovumdocs =  $this->recuperar_arquivo_ovumdocs( $certificado['codigo_autenticacao'] );
                                $status = $recuperar_arquivo_ovumdocs['status'];

                                if($status!==1)
                                {
                                    $result[0]['url_certificado'] = "";
                                    sleep(2);
                                }
                                elseif( $status == 1 )
                                {
                                    $i = 5;
                                    $result[0]['url_certificado'] = $recuperar_arquivo_ovumdocs['arquivo']['link'];
                                }
                                
                                $i++;
                            }
                        
                        }
                        else
                        {
                            $result[0]['url_certificado'] = $recuperar_arquivo_ovumdocs['arquivo']['link'];
                        }
                    }
                    else
                    {
                        $id_certificado = $certificado['codigo_autenticacao']['id_certificado'];
                        $id_historico = $historico_usuario[$i]['id_historico'];
                        
                        $url_certificado = base_url().'gera_certificado/'.codifica($id_certificado).'/'.codifica($historico_usuario[$i]['id_aluno']).'/'. codifica($historico_usuario[$i]['id_curso']).'/'.codifica($id_historico);
                        $add_arquivo_docs = $this->add_arquivo_docs( $certificado['codigo_autenticacao']['codigo_autenticacao'], $url_certificado, $id_historico );
                        $status = $add_arquivo_docs[0]['status'];
                        
                        if($status!=1)
                        {
                            $i=0;
                            while ( $i<4 )
                            {
                                $add_arquivo_docs =  $this->add_arquivo_docs( $certificado['codigo_autenticacao']['codigo_autenticacao'], $url_certificado, $id_historico );
                                $status = $add_arquivo_docs[0]['status'];

                                if($status!==1)
                                {
                                    $result[0]['url_certificado'] = "";
                                    sleep(2);
                                }
                                elseif( $status == 1 )
                                {
                                    $i = 5;
                                    $result[0]['url_certificado'] = $add_arquivo_docs[0]['arquivo']['link'];
                                }
                                
                                $i++;
                            }
                        }
                        else
                        {
                            $result[0]['url_certificado'] = $add_arquivo_docs[0]['arquivo']['link'];
                        }
                        
                    }
                    
                }
                $result[0]['matriculado'] = TRUE;
                $result[0]['estudar'] = $this->curso->estudar_aluno($historico_usuario[$i]['id_historico']);
                $result[0]['id_historico'] = $historico_usuario[$i]['id_historico'];
                $result[0]['primeiro_dia'] = date('d/m/Y', strtotime($historico_usuario[$i]['primeiro_dia']));;
                $palestra_historico_usuario = array_merge($palestra_historico_usuario, $result);
            }
            
            return $palestra_historico_usuario;
        }
    }
    
    private function add_arquivo_docs($id_arquivo, $url_certificado, $id_historico)
    {
        $url = DOCS_URL . 'salvar';
        
        $post['auth'] = array(
            'chave' => IDM_CHAVE,
            'token' => IDM_TOKEN
        );
        
        $post['arquivo'] = array(
            array(
                'id_arquivo' => $id_arquivo,
                'tipo' => '1',
                'descricao' => 'Certificado IDMCURSOS',
                'processar' => '1',
                'url' => $url_certificado,
                'url_retorno' => ''
            )
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        $resposta_docs = curl_exec($ch); # o retorno é um Objeto JSON
        $resposta = json_decode($resposta_docs, TRUE);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        $this->curso->add_log_docs($url_certificado, $id_historico, $id_arquivo, $resposta_docs, $code);
        curl_close($ch);
        
        return $resposta;
    }
    
    private function recuperar_arquivo_ovumdocs($identificador) 
    {

        $url =  DOCS_URL . 'buscar';

        $post['auth'] = array(
        'chave' => IDM_CHAVE,
        'token' => IDM_TOKEN
        );

        $post['id_arquivo']  = $identificador;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $requisicao = curl_exec($ch);#$requisição é um Objeto JSON
        $resposta = json_decode($requisicao, TRUE);#$requisição é um Objeto JSON
        curl_close($ch);

        return $resposta;
      }

    
    private function gera_identificador($id)
    {
        $hash = date('Y') . '-' . strtoupper(substr(md5( $id . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5( $id . ':' . microtime()), 0, 4));
        $autenticacao = strtoupper(substr(md5($hash), 0, 8));
        
        return date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
    }
    
    private function gera_cod_autenticacao($id_usuario)
    {
        $hash = date('Y') . '-' . strtoupper(substr(md5( $id_usuario . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5( $id_usuario . ':' . microtime()), 0, 4));
        $autenticacao = strtoupper(substr(md5($hash), 0, 8));
        
        return date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
    }

    
    public function gera_certificado($id_certificado, $id_usuario, $id_palestra, $id_historico)
    {
        $id_certificado = decodifica($id_certificado);
        $id_usuario = decodifica($id_usuario);
        $id_palestra = decodifica($id_palestra);
        $id_historico = decodifica($id_historico);
        
        $this->load->model("Curso_model","curso");
        
        $certificado_palestra = $this->curso->get_certificado($id_certificado, $id_usuario, $id_palestra, $id_historico);
        $id_aluno = $certificado_palestra[0]['id_aluno'];
        $usuario = $this->curso->get_usuario($id_aluno);
        $nome_palestrante = $usuario['nome'];
        
        $is_pago = $this->curso->check_pagamento_historico($id_usuario, $id_historico);
        
        if($is_pago)
        {
            
            if( isset($certificado_palestra[0]) && count($certificado_palestra[0])>0 )
            {

                $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
                require_once(APPPATH.'libraries/fpdf/Fpdf.php');
                $this->fpdf = new FPDF($params);

                $this->fpdf->AliasNbPages();                
                $this->fpdf->AddPage();
                $this->fpdf->Image(base_url().'public/img/certificado_idm_2.jpg', 0, 0, 297, 210 , 'JPG'); //logo
                $this->fpdf->setY("71");
                $this->fpdf->setX("5");
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 10, 'conferido a '.utf8_decode($certificado_palestra[0]['nome']) , 0, 1, 'C');
                
                if( $certificado_palestra[0]['rg'] != ""){
                    $this->fpdf->setY("82");
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->Cell(0, 7, utf8_decode("Curso de ".$certificado_palestra[0]['nome_palestra'].' - Turma '.$certificado_palestra[0]['turma']), 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->Cell(0, 7, 'Documento: ' . utf8_decode($certificado_palestra[0]['rg']), 0, 1, 'C');
                }
                else
                {
                    $this->fpdf->setY("82");
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->Cell(0, 7, utf8_decode("Curso de ". $certificado_palestra[0]['nome_palestra'].' - Turma '.$certificado_palestra[0]['turma']), 0, 1, 'C');
                }

                $descricao_palestra = str_replace("\n", "", $certificado_palestra[0]['descricao_palestra']);
                $descricao_palestra = str_replace(".", ",", $descricao_palestra);
                $descricao_palestra = substr($descricao_palestra, 0, -1); 

                $this->fpdf->setY(110);
                $this->fpdf->setX(8);
                $this->fpdf->MultiCell(280, 6, utf8_decode("Conteúdo: \n" ), 50, 'C');
                $this->fpdf->MultiCell(280, 6, utf8_decode( $descricao_palestra.'.' ), 50, 'J');

                //Inicio de ajuste para nome de curso que excede o limite da linha
                $this->fpdf->setY(95);
                $this->fpdf->setX(50);
                $this->fpdf->setX(50);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº: ') . $certificado_palestra[0]['identificador'], 0, 1);

                $this->fpdf->setY(103);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 1, utf8_decode('Código de autenticação: ').$certificado_palestra[0]['codigo_autenticacao'], 0, 1);

                $this->fpdf->setY(95);

                $this->fpdf->setX(180);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode("Data: ".date('d/m/Y', strtotime( $certificado_palestra[0]['data_encerramento'] ) ) ), 0, 1);

                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, 'Autenticar em: www.ovumdoc.com', 0, 1);

                $this->fpdf->setX(180);
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetXY(10, 125);

                $this->fpdf->SetLineWidth(0.2);

                //$this->fpdf->Line(40, 175, 125, 175);
                //$this->fpdf->SetXY(50, 180);
                //$this->fpdf->Cell(0, 1, 'Assinatura do(a) Aluno(a)', 0, 0);

                $this->fpdf->Image('public/img/rubrica_2.png', 210, 165, 40);

                $this->fpdf->Line(191, 179, 265, 179);

                $this->fpdf->SetXY(190, 183);

                $this->fpdf->Cell(0, 1, 'Prof. Me. Ezelildo G Dornelas', 0, 0);

                $this->fpdf->SetXY(220, 188);

                $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);
                
                return $this->fpdf->Output();
            }
        }
    }
    
    public function curso_matriculado()
    {
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
         
        $this->load->model("Curso_model","curso");
        
        $curso_usuario = $this->curso->get_usuario_id_unico($usuario_id_unico);
        
        $get_historico_usuario = $this->curso->get_historico_usuario($curso_usuario[0]['id_aluno']);
        
        if(count($get_historico_usuario)==0)
        {
            redirect("curso_disponivel");
        }
        
        $this->load->library("pagination");
        
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        
        $config = array();
        //
        $config['full_tag_open'] = '<ul style="margin-left:15px; margin-right: 15px" class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Proxima&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="'.base_url().'curso_matriculado/'.$page.'" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        //
        
        $config["base_url"] = base_url() . "curso_matriculado";
        $config["total_rows"] = count( $this->curso->get_historico_usuario($curso_usuario[0]['id_aluno']) );
        $config["per_page"] = PER_PAGE;
        $config["uri_segment"] = 2;
        
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        $palestra_historico_usuario = $this->palestra_historico_usuario($curso_usuario[0]['id_aluno'], $config["per_page"], $page);
        
        if( count($palestra_historico_usuario)>0 )
        {
            $result = $palestra_historico_usuario;
            $data = ['palestras_disponiveis'=>$result,'links'=>$links, 'identificador_usuario'=>$curso_usuario[0]['id_aluno']];
            $this->load->view('aluno/paginas/curso_matriculado', $data);
        }
        else
        {
            $data = ['palestras_disponiveis'=>[]];
            $this->load->view('aluno/paginas/curso_matriculado', $data);
        }
    }

    private function location()
    {
        die();
        $ip = $this->get_client_ip(); 
        if($ip=='UNKNOWN')
        {
            return $ip;
        }
        else
        {
            $json  = file_get_contents("http://ipinfo.io/$ip/geo");
            $json  =  json_decode($json ,true);
            
            if( isset($json['country']) && isset($json['region']) && isset($json['city']) )
            {
                $country =  $json['country'];
                $region= $json['region'];
                $city = $json['city'];
                return $city."/".$region."/".$country;
            }
            else
            {
                return "UNKNOWN";
            }
        }
    }
    

    private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    public function insert_usuario_termo_uso($dados)
    {
        
        $dados  = [
                   'id_aluno'=> $dados['id_usuario'],
                   'id_termo'=> $dados['id_termo_uso'] 
                  ];
        
        $is_aceite = $this->curso->insert_usuario_termo_uso($dados);
        
        if($is_aceite)
        {
            $this->index();
        }
    }
    
    public function do_aceite()
    {
        $this->load->model("Curso_model","curso");
        
        $dados_aceite = $this->input->post();
        
        
        $request =  decodifica( $dados_aceite['request'] );
        $request = json_decode($request,TRUE);
        
        $consulta_usuario = $this->curso->consulta_usuario($request);
        if(count($consulta_usuario)>0)
        {
            $this->curso_matriculado();
        }
        else
        {
            $id_instituicao =  decodifica($dados_aceite['id_instituicao']);

            $id_termo_uso = decodifica($dados_aceite['termo_uso']);

            $id_usuario = $this->curso->usuario($request);

            $this->curso->instituicao_usuario($id_usuario, $id_instituicao);

            $dados_termo_uso = [
                                'id_usuario'=> $id_usuario,
                                'id_termo_uso'=> $id_termo_uso
                               ];

            $this->insert_usuario_termo_uso($dados_termo_uso);
        }
    }

    public function erro($data=null)
    {
        $this->load->view('site/paginas/error', $data);
    }
    
    public function codifica_dados_enviados()
    {
        $response = file_get_contents("php://input");
        $response_security = $this->security->xss_clean($response);
        
        $response_decode = json_decode($response_security, TRUE);
        
        if( isset($response_decode['autenticacao']['chave']) && isset($response_decode['autenticacao']['token']) && isset($response_decode['usuario']['id_unico']) && !empty($response_decode['autenticacao']['chave']) && !empty($response_decode['autenticacao']['token']) && !empty($response_decode['usuario']['id_unico']) )
        {
            $codifica_response = codifica($response_security);
            echo $codifica_response;
        }
        else
        {
            
            $not_found = json_encode(['404'=>'404']);
            echo codifica($not_found);   
        }
        
    }
    
    public function not_found()
    {
        $this->erro(['mensagem'=>'Falha ao Enviar Dados para Codificação' ]);
    }

    public function consulta($response)
    {
        $response_new = $this->security->xss_clean($response);
        $response = decodifica($response_new);
        $request = json_decode( $response, true );
        if( isset($request['404']) )
        {
            $this->not_found();
            die;
        }
        
       if( (count($response)>1) || ($response!=''))
        {
            $this->check_estrutura($request, 'autenticacao_centralizadora');
            $this->check_dados($request, 'autenticacao_centralizadora');

            $this->load->model("Curso_model",'curso');
            
            $response_autenticacao = $this->autentica_instituicao($request['autenticacao']['chave'], $request['autenticacao']['token']);

            if( ( isset($response_autenticacao['situacao']) ) && ($response_autenticacao['situacao']=='Sucesso') )
            {
                $id_instituicao = $this->curso->instituicao($response_autenticacao);
                $this->check_estrutura($request, 'dados_aluno');
                $this->check_dados($request, 'dados_aluno');
                
                $this->usuario($request, $id_instituicao);
            }
            else
            {
                $this->erro(['mensagem'=>$response_autenticacao['mensagem'] ]);
            }
        }
        else
        {
            $data = ['mensagem'=>'Dados vazios'];
             $this->erro($data);
        }

    }
    
    private function usuario($request, $id_instituicao)
    {
        
        $dados_aluno = $this->curso->consulta_usuario($request);
        $id_usuario = ( isset($dados_aluno[0]['id_aluno']) ) ? $dados_aluno[0]['id_aluno'] : "";
        $is_usuario_termo_uso = $this->curso->usuario_termo_uso($id_usuario);
        
        if(!$is_usuario_termo_uso)
        {
            $termo_uso_ativo = $this->curso->termo_uso_ativo();
            $data = [
                    'request'=> json_encode($request), 
                    'id_instituicao'=>$id_instituicao,
                    'termo_uso_ativo'=>$termo_uso_ativo
                    ];
            
            $this->load->view('site/paginas/termo_uso', $data);
        }
        else
        {
            $session = array(
                        'usuario_id_unico' => (!isset($request['usuario']['id_unico'])) ? '' : $request['usuario']['id_unico'] ,
                        'usuario_nome' => (!isset($request['usuario']['nome'])) ? '' : $request['usuario']['nome'],
                        'usuario_email' => (!isset($request['usuario']['email'])) ? '' : $request['usuario']['email'],
                        'usuario_documento' => (!isset($request['usuario']['documento'])) ? '' : $request['usuario']['documento'],
                        'usuario_logado' => TRUE
                    );
            $this->session->set_userdata($session);
            
            $this->curso_matriculado();
        }
    }

        private function show_mensagem_dados( string $tipo)
        {
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    $data = ['mensagem'=>'E necessario definir corretamente tipo e valores de autenticacao para chave, token e id_instituicao'];
                    $this->erro($data);
                    exit();
                    break;
                case 'dados_aluno':
                    $data = ['mensagem'=>'E necessario definir corretamente tipo e valores de autenticacao para o usuario'];
                    $this->erro($data);
                    exit();
                    break;
                default:
                    $data = ['mensagem'=>'Tipo não definido para mensagem'];
                    $this->erro($data);
                    exit();
            endswitch;
            
        }
        
        private function show_mensagem_estrutura( string $tipo)
        {
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    $data = ['mensagem'=>'E necessario definir corretamente as chaves para autenticacao'];
                    $this->erro($data);
                    break;
                case 'dados_aluno':
                    $data = ['mensagem'=>'E necessario definir corretamente as chaves para o usuario'];
                    $this->erro($data);
                    break;
                default:
                    $data = ['mensagem'=>'Tipo não definido para mensagem'];
                    $this->erro($data);
            endswitch;
            
        }

        private function check_estrutura( $dados, string $tipo)
        {
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    if(array_key_exists('autenticacao', $dados))
                    {
                        $dados = $dados['autenticacao'];
                        if( !array_key_exists('chave', $dados) || !array_key_exists('token', $dados) )
                        {
                            $this->show_mensagem_estrutura($tipo);
                        }
                    }
                    else
                    {
                        $this->show_mensagem_estrutura($tipo);
                    }
                    break;
                case 'dados_aluno':
                    if(array_key_exists('usuario', $dados))
                    {
                        $dados = $dados['usuario'];
                        if( !array_key_exists('id_unico', $dados) || !array_key_exists('nome', $dados) || !array_key_exists('email', $dados) || !array_key_exists('documento', $dados) )
                        {
                            $this->show_mensagem_estrutura( $tipo);
                        }
                    }
                    else
                    {
                        echo json_encode( ['status'=>0,'Mensagem'=>'E necessario definir corretamente a chave usuario'], JSON_FORCE_OBJECT );
                        exit;
                    }
                    break;
                default:
                    echo "Tipo de estrutura não definida";
            endswitch;
        }
        
        private function check_dados( array $dados, string $tipo)
        {
            
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    
                    $chave = filter_var( $dados['autenticacao']['chave'], FILTER_SANITIZE_STRING);
                    $token = filter_var ( $dados['autenticacao']['token'], FILTER_SANITIZE_STRING);
                    
                    if( !is_string($dados['autenticacao']['chave']) || !is_string($dados['autenticacao']['token']) )
                    {
                        $this->show_mensagem_dados($tipo);
                        
                        if( empty( trim($chave) ) || empty( trim($token) ) )
                        {
                            $this->show_mensagem_dados($tipo);
                        }
                    }
                    break;
                case 'dados_aluno':
                    $id_unico = filter_var($dados['usuario']['id_unico'], FILTER_SANITIZE_STRING);
                    $nome = filter_var($dados['usuario']['nome'], FILTER_SANITIZE_STRING);
                    $email = filter_var($dados['usuario']['email'], FILTER_SANITIZE_STRING);
                    $documento = filter_var($dados['usuario']['documento'], FILTER_SANITIZE_STRING);
                    
                    if( !is_string($dados['usuario']['id_unico']) )
                    {
                        $this->show_mensagem_dados($tipo);
                    }
                    elseif ( empty( trim($id_unico) ) )
                    {
                        $this->show_mensagem_dados($tipo);
                    }
                    break;
                default:
                    echo "Tipo de estrutura não definida";
            endswitch;
            
        }

        public function autentica_instituicao($chave=null, $token=null, $id_instituicao=null)
        {
            if( !empty( trim($chave) ) && !empty( trim($token) ) )
            {
                $url = INSTITUICAO_URL;
                $data["autenticacao"]["chave"] = $chave;
                $data["autenticacao"]["token"] = $token;
                $data["autenticacao"]["servico"] = INSTITUICAO_SERVICO;

                $data = json_encode($data);
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                if($httpCode==200)
                {
                    $autenticacao = json_decode( $result, true);
                    return $autenticacao;
                }
                else
                {
                    echo 'Falha na Comunicação';
                }

                curl_close($ch);
            }
            else
            {
                echo json_encode( ['Mensagem'=>'Parametros obrigatorios de autenticacao vazio'], JSON_FORCE_OBJECT );
                exit();
            }
        }
}