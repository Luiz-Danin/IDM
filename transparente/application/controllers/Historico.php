<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historico extends CI_Controller
{
    public function realiza_matricula()
    {
        
        $id_palestra_cod = $this->input->post("valor", TRUE);
        $id_palestra = decodifica($id_palestra_cod);
        
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $this->load->model("Curso_model", "curso");
        $palestra_usuario = $this->curso->get_usuario_id_unico($usuario_id_unico);
        
        if( isset($palestra_usuario[0]['id_aluno']))
        {
            $id_usuario = $palestra_usuario[0]['id_aluno'];
        }
        
        
        if( !empty($id_palestra) && isset($id_usuario)  && !empty( trim($palestra_usuario[0]['nome']) ) )
        {
            
            $curso = $this->curso->get_palestra_by_id_palestra($id_palestra);
            $turma_ativa = $this->curso->get_turma_ativa($id_palestra);
            
            $ultimo_dia = ( isset( $turma_ativa['data_encerramento'] ) ) ? $turma_ativa['data_encerramento'] : NULL;
            
            $historico = $this->curso->add_historico_participante($id_usuario, $id_palestra, $ultimo_dia );
            
            if( !empty($historico) && count($turma_ativa)>0 )
            {
                $this->curso->aluno_turma($id_usuario, $turma_ativa['id_turma']);
                $this->curso->add_aluno_curso_turma($turma_ativa['id_turma'], $historico);
                
                $this->session->set_flashdata('feedback_matricula', 'Matricula realizada com sucesso no <strong>Curso '.$curso[0]['nome'].'</strong>');
            }
            else
            {
                $this->session->set_flashdata('feedback_matricula_falha', 'Falha ao realizar a matricula no <strong>Curso '.$curso[0]['nome'].'</strong>');
            }
            
        }
        else
        {
            $this->session->set_flashdata('feedback_matricula_falha', 'Problema ao realizar a matricula');
        }
    }
    
    private function table_palestras_disponiveis($palestras_disponiveis)
    {
        echo '<table class="table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Inscrever</th>
                    </tr>
                </thead>
                <tbody>';
                foreach ($palestras_disponiveis as $disponiveis)
                {
                    if( isset($disponiveis['matriculado']) )
                    {
                        echo '<tr>';
                            echo '<td>'.$disponiveis['nome'].'</td>';
                            echo '<td><button type="button" class="btn btn-outline-dark bmd-modalButton" data-toggle="modal" data-bmdsrc="https://www.youtube.com/embed/hftefBW0bmM" data-bmdwidth="640" data-bmdheight="480" data-target="#myModal" data-bmdvideofullscreen="true">Assistir</button></td>';
                        echo '</tr>';
                    }
                    else
                    {
                        echo '<tr>';
                            echo '<td>'.$disponiveis['nome'].'</td>';
                            echo '<td>'
                            . '<div class="btn-matricula">'
                            . '<button type="button" class="btn btn-outline-dark matricula-palestra" value="'.codifica($disponiveis['id_palestra']).'">Inscrever</button>'
                            . '</div>'
                            . '</td>';
                        echo '</tr>';
                    }
                }
        echo '</tbody>
                </table>';
    }

    private function palestra_historico_usuario($id_usuario)
    {
        if( !empty($id_usuario) )
        {
            $historico_usuario =  $this->pl->get_historico_usuario($id_usuario);
            
            $palestra_historico_usuario = [];
            
            for ($i = 0 ; $i<count($historico_usuario) ; $i++)
            {
                $result = $this->pl->get_palestra_by_id_palestra($historico_usuario[$i]['id_palestra']);
                $result[0]['matriculado'] = TRUE;
                $palestra_historico_usuario = array_merge($palestra_historico_usuario, $result);
            }
            //echo '<pre>';var_dump($palestra_historico_usuario);die;
            
            return $palestra_historico_usuario;
        }
    }

    public function palestras_disponiveis($usuario_id_unico)
    {
        $this->load->model("Palestra_model","pl");
        
        $palestra_usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        
        $palestra_historico_usuario = $this->palestra_historico_usuario($palestra_usuario[0]['id_usuario']);
        
        if(count($palestra_historico_usuario)>0 )
        {
            
            $get_palestras_disponiveis = $this->pl->get_palestras_disponiveis();
            
            $count_palestras_disponiveis = count($palestra_historico_usuario);
            
            $result = array_merge($palestra_historico_usuario, $get_palestras_disponiveis);
            
            return $result;
        }
        
        
        return $this->pl->get_palestras_disponiveis();
    }
    
}