<?php
defined('BASEPATH') OR exito('No direct script access allowed');

class Curso_model extends CI_Model
{
    
    public function update_nome($nome, $id_unico)
    {
        $this->db->set('nome', $nome);
        $this->db->where('id_unico', $id_unico);
        
        return $this->db->update('aluno');
    }

    public function estudar_aluno($id_historico)
    {
        
        return $this->estudarAlunoAVA($id_historico);
    }
    
    private function estudarAlunoAVA($id_historico)
    {
        $alunoAVA = $this->getDataAlunoAVA($id_historico);
        
        if( count($alunoAVA)>0 )
        {
            
            $arrayAlunoAVA['turma'] = $alunoAVA[0]['id_turma_ava'];
            $arrayAlunoAVA['email'] = $alunoAVA[0]['email'];
            $arrayAlunoAVA['nome'] =  $alunoAVA[0]['nome'];
            $arrayAlunoAVA['aluno'] = $alunoAVA[0]['id_aluno'];
            $arrayAlunoAVA['curso'] = $alunoAVA[0]['id_ava'];
            $arrayAlunoAVA['tipo'] = 4;

            $IDIOMAS = array(
                            'nome' => AVA_NOME,
                            'chave' => AVA_CHAVE,
                            'token' => AVA_TOKEN
            );

            return base64_encode(json_encode(array('instituicao' => $IDIOMAS,'dados'=> $arrayAlunoAVA)));
        }
        
        return '';
    }
    
    private function getDataAlunoAVA($id_historico)
    {
        $query = $this->db->query("SELECT a.email, a.nome,a.id_aluno,id_turma,id_turma_ava,cu.id_ava  
                                    FROM `aluno_curso_turma` 
                                    act join turma_ava ta  using(id_turma)
                                    join aluno_curso ac using(id_historico)
                                    join curso_ava cu using (id_curso)
                                    join aluno a using (id_aluno) 
                                    where date( ac.ultimo_dia )>=CURDATE() AND id_historico=".$id_historico);

        return $query->result_array();
    }
    
    public function get_usuario($id_usuario)
    {
         $this->db->select();
        $this->db->from('aluno');
        $this->db->where('id_aluno', $id_usuario);
        
        $usuario = $this->db->get()->result_array();
        
        if( count($usuario)>0 )
        {
            return $usuario[0];
        }
        
        return $usuario;
    }
    
    public function is_pago($id_usuario, $id_historico)
    {
        $this->db->select();
        $this->db->from('historico');
        
        $this->db->join('usuario_pagamento', 'historico.id_historico = usuario_pagamento.id_historico');
        
        $this->db->where('usuario_pagamento.id_usuario', $id_usuario);
        $this->db->where('usuario_pagamento.id_historico', $id_historico);
        $this->db->where('usuario_pagamento.situacao', '3');
        
        $pago  = $this->db->get()->result_array();
        
        if( count($pago[0])>0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function add_log_docs($url_certificado, $id_historico, $codigo_autenticacao, $resposta_docs, $code_status)
    {
        $add_log_docs = ['url'=>$url_certificado, 'id_historico'=>$id_historico, 'codigo_autenticacao'=>$codigo_autenticacao, 'resposta_docs'=>$resposta_docs,'codigo_http'=>$code_status, 'data'=> date("Y-m-d H:i:s") ];
        
        $this->db->insert("log_docs", $add_log_docs);
    }
    
    public function get_certificado($id_certificado, $id_usuario, $id_palestra, $id_historico)
    {
        $this->db->select("aluno.nome as nome, aluno.rg, aluno.id_aluno , curso.nome as nome_palestra, curso.descricao as descricao_palestra, "
                . "turma.data_inicio as data_palestra, curso.ementa descricao_palestra, "
                . "aluno_curso.id_aluno as id_usuario_palestrante,"
                . "certificado.id_certificado as id_certificado, certificado.codigo_autenticacao,"
                . "certificado.id_historico as id_historico, certificado.identificador as identificador,"
                . "turma.data_encerramento as data_encerramento, turma.id_turma as turma");
        $this->db->from('aluno_curso_turma');
        
        $this->db->join('aluno_curso', 'aluno_curso.id_historico = aluno_curso_turma.id_historico');
        $this->db->join('turma', 'turma.id_turma = aluno_curso_turma.id_turma');
        $this->db->join('curso', 'aluno_curso.id_curso = curso.id_curso');
        $this->db->join('aluno', 'aluno_curso.id_aluno = aluno.id_aluno');
        $this->db->join('certificado', 'aluno_curso.id_historico = certificado.id_historico');
        
        $this->db->where('curso.id_curso', $id_palestra);
        $this->db->where('aluno.id_aluno', $id_usuario);
        $this->db->where('aluno_curso.id_historico', $id_historico);
        $this->db->where('certificado.id_certificado', $id_certificado);
        
        return $this->db->get()->result_array();
    }
    
    public function edit_situacao_palestra($id_palestra, $situacao)
    {
        $this->db->set('situacao', $situacao);
        $this->db->where('id_palestra', $id_palestra);
        
        $this->db->update('palestra');
    }
    
    public function consulta_palestra_identificador($identificador)
    {
        $this->db->select("palestra.id_palestra");
        $this->db->from('modulo_palestra');
        
        $this->db->join('palestra', 'modulo_palestra.id_palestra = palestra.id_palestra');
        $this->db->join('modulo', 'modulo_palestra.id_modulo = modulo.id_modulo');
        $this->db->join('material_modulo', 'modulo.id_modulo = material_modulo.id_modulo');
        $this->db->join('material', 'material_modulo.id_material = material.id_material');
        
        $this->db->where('material.identificador', $identificador);
        
        return $this->db->get()->result_array();
    }

    public function add_material_modulo($id_material, $id_modulo)
    {
        $material_modulo = ['id_material'=>$id_material, 'id_modulo'=>$id_modulo ];
        return $this->db->insert("material_modulo", $material_modulo);
    }

    public function add_material($idenficador, $url)
    {
        $material = ['identificador'=>$idenficador, 'url'=>$url,'situacao'=>1 ];
        $this->db->insert("material", $material);
        
        return $this->db->insert_id();
    }

    public function add_modulo_palestra($id_modulo, $id_palestra)
    {
        $modulo_palestra = ['id_modulo'=>$id_modulo, 'id_palestra'=>$id_palestra ];
        $this->db->insert("modulo_palestra", $modulo_palestra);
    }

    public function add_modulo()
    {
        $modulo = ['nome'=>'Modulo 1', 'situacao'=>1 ];
        $this->db->insert("modulo", $modulo);
        
        return $this->db->insert_id();
    }
    
    private  function limitarTexto($texto, $limite)
    {
       $contador = strlen($texto);
       
       if ($contador >= $limite)
       {
           $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' '));
           return $texto;
       }
       else
       {
           return $texto;
       }
   }
   
   public function add_palestra($id_usuario_palestrante, $nome, $descricao, $location)
   {
        $descricao = $this->limitarTexto($descricao, 250);
        
        $palestra = ['id_usuario_palestrante'=>$id_usuario_palestrante, 'nome'=>$nome, 'data_criacao'=>date("Y-m-d H:i:s"), 'privada'=>0, 'dt_inicio'=>date("Y-m-d H:i:s"), 'local'=>$location,'descricao'=>$descricao, 'situacao'=>1 ];
        $this->db->insert("palestra", $palestra);
        
        return $this->db->insert_id();
    }

    public function add_palestra_avaliacao_cupom($id_avaliacao, $cupom)
    {
        $avaliacao_palestra = ['id_avaliacao'=>$id_avaliacao, 'cupom'=>$cupom, 'situacao'=>1 ];
        $this->db->insert("palestra_avaliacao_cupom", $avaliacao_palestra);
        
        //return $this->db->insert_id();
    }

    public function avaliacao_palestra($id_aluno, $valor, $id_historico, $id_palestra)
    {
        $consulta_avaliacao_palestra = $this->consulta_avaliacao_palestra($id_aluno, $id_historico, $id_palestra);
        
        if( count($consulta_avaliacao_palestra)>0 )
        {
            return ['status'=>TRUE, 'id_avaliacao'=>$consulta_avaliacao_palestra[0]];
        }
        else
        {
            $id_avaliacao_palestra = $this->add_avaliacao_palestra($id_aluno, $valor, $id_historico, $id_palestra);
            return ['status'=>FALSE ,'id_avaliacao'=>$id_avaliacao_palestra];
        }
    }

    public function add_avaliacao_palestra($id_aluno, $valor, $id_historico, $id_palestra)
    {
        $avaliacao_palestra = ['id_palestra'=>$id_palestra, 'id_historico'=>$id_historico, 'id_aluno'=>$id_aluno, 'qualificacao'=>$valor, 'data'=>date("Y-m-d H:i:s") ];
        $this->db->insert("palestra_avaliacao", $avaliacao_palestra);
        
        return $this->db->insert_id();
    }
    
    public function consulta_avaliacao_palestra($id_aluno, $id_historico, $id_palestra)
    {
        $this->db->select();
        $this->db->from('palestra_avaliacao');
        $this->db->where('id_aluno', $id_aluno);
        $this->db->where('id_historico', $id_historico);
        $this->db->where('id_palestra', $id_palestra);
        
        return $this->db->get()->result_array();
    }
    
    public function consulta_palestra_avaliacao_cupom($id_avaliacao)
    {
        $this->db->select();
        $this->db->from('palestra_avaliacao_cupom');
        $this->db->where('id_avaliacao', $id_avaliacao);
        
        return $this->db->get()->result_array();
    }

    public function add_historico_participante($id_aluno, $id_curso, $ultimo_dia)
    {
        $historico = ['id_aluno'=>$id_aluno, 'id_curso'=>$id_curso, 'primeiro_dia'=>date("Y-m-d"), 'primeira_hora'=>date("H:i:s"), 'ultimo_dia'=>$ultimo_dia,'situacao'=>1, 'id_instituicao_unidade'=>4];
        $this->db->insert("aluno_curso", $historico);
        
        return $this->db->insert_id();
    }
    
    public function get_turma_ativa($id_curso)
    {
        $this->db->select();
        $this->db->from('turma');
        $this->db->where('id_curso', $id_curso);
        $this->db->where('situacao', 1);
        
        $turma_ativa = $this->db->get()->result_array();
        
        if( count($turma_ativa)>0 )
        {
            return $turma_ativa[0];
        }
        return $turma_ativa;
    }
    
    public function aluno_turma($id_aluno, $id_turma)
    {
        $aluno_turma = ['id_aluno'=>$id_aluno, 'id_turma'=>$id_turma];
        
        return $this->db->insert("aluno_turma", $aluno_turma);
    }

    public function add_aluno_curso_turma($id_turma, $id_historico)
    {
        $aluno_curso_turma = ['id_turma'=>$id_turma, 'id_historico'=>$id_historico];
        
        return $this->db->insert("aluno_curso_turma", $aluno_curso_turma);
    }
    
    public function base_legal($texto)
    {
        $texto = ['texto'=>$texto];
        return $this->db->insert("base_legal", $texto);
    }
    
    public function update_situacao_pagamento($id_transacao, $situacao)
    {
        $this->db->set('situacao', $situacao);
        $this->db->where('id_transacao', $id_transacao);
        
        $this->db->update('transacao');
    }
    
    public function check_avaliacao_palestra($id_palestra)
    {
        $this->db->select();
        $this->db->from('palestra_avaliacao');
        //$this->db->join('historico','palestra_avaliacao.id_palestra = historico.id_palestra');
        
        $this->db->where('palestra_avaliacao.id_palestra', $id_palestra);
        
        return $this->db->get()->result_array();
    }
    
    public function gera_pagamento($id_aluno, $id_historico, $id_transacao, $id_curso)
    {
        $descricao = ($id_curso=='635') ? 'Certificado Curso Idiomas Ingles' : 'Certificado Curso de Idiomas Espanhol';
        $id_produto = ($id_curso=='635') ? 7 : 8;
        
        $usuario_pagamento = ['id_aluno'=>$id_aluno,'id_produto'=>2,'id_historico'=>$id_historico, 'id_transacao'=>$id_transacao, 'data_transacao'=> date("Y-m-d H:i:s"),'situacao'=>1 ];
        $is_add_transacao = $this->add_transacao($id_transacao, $id_aluno, $descricao);
        
        if($is_add_transacao)
        {
            $this->add_item_transacao($id_transacao, $id_produto, $id_historico);
        }
    }
    
    private function add_transacao($id_transacao, $id_aluno, $descricao)
    {
        $add_transacao = ['id_transacao'=>$id_transacao, 'id_aluno'=>$id_aluno, 'data_transacao'=>date("Y-m-d H:i:s"),'descricao'=>$descricao, 'situacao'=>1, 'valor'=>14.9,'id_instituicao_unidade'=>4 ];
        return $this->db->insert("transacao", $add_transacao);
    }


    private function add_item_transacao($id_transacao,$id_produto,$id_historico)
    {
        $add_item_transacao = ['transacao'=>$id_transacao,'id_produto'=>$id_produto,'historico'=>$id_historico, 'valor'=>14.9, 'data_registro'=>date("Y-m-d H:i:s") ];
        $this->db->insert("item_transacao", $add_item_transacao);
    }

    public function add_usuario_palestrante_pagamento($id_usuario, $id_historico, $id_transacao, $codigo_intermediador)
    {
        $usuario_pagamento = ['id_usuario'=>$id_usuario,'id_produto'=>1,'id_historico'=>$id_historico, 'id_transacao'=>$id_transacao, 'codigo_intermediador'=>$codigo_intermediador, 'data_criacao'=> date("Y-m-d H:i:s"),'situacao'=>1 ];
        
        $this->db->insert("usuario_pagamento", $usuario_pagamento);
    }
    
    public function certificado($id_usuario, $id_historico, $cod_autenticacao, $identificador)
    {
        $certificado = $this->consulta_certificado($id_usuario, $id_historico);
        
        if( count($certificado)>0 )
        {
            return ['status'=> TRUE, 'codigo_autenticacao'=>$certificado[0]['codigo_autenticacao'] ];
        }
        else
        {
            $autenticacao = $this->add_certificado_palestrante($id_usuario, $id_historico, $cod_autenticacao, $identificador);
            
            return ['status'=> FALSE, 'codigo_autenticacao'=>$autenticacao ];
        }
    }
    
    private function consulta_certificado($id_historico)
    {
        $this->db->select();
        $this->db->from('certificado');
        $this->db->where('id_historico', $id_historico);
        
        return $this->db->get()->result_array();
    }
    
    public function certificado_participante($id_usuario, $id_historico, $cod_autenticacao, $identificador)
    {
        $certificado = $this->consulta_certificado($id_historico);
        
        if( count($certificado)>0 )
        {
            return ['status'=> TRUE, 'codigo_autenticacao'=>$certificado[0]['codigo_autenticacao'] ];
        }
        else
        {
            $autenticacao = $this->add_certificado_participante($id_historico, $cod_autenticacao, $identificador);
            return ['status'=> FALSE, 'codigo_autenticacao'=>$autenticacao ];
        }
    }


    public function add_certificado_palestrante($id_usuario, $id_historico, $autenticacao, $identificador )
    {
        $certificado = ['id_usuario'=>$id_usuario,'id_produto'=>1,'id_historico'=>$id_historico, 'identificador'=>$identificador, 'codigo_autenticacao'=>$autenticacao, 'situacao'=>0 ];
        
        $this->db->insert("certificado", $certificado);
        return [ 'id_certificado'=>$this->db->insert_id(), 'codigo_autenticacao'=>$autenticacao ];
    }
    
    public function add_certificado_participante($id_historico, $autenticacao, $identificador )
    {
        $certificado = ['id_historico'=>$id_historico, 'identificador'=>$identificador, 'codigo_autenticacao'=>$autenticacao, 'situacao'=>0 ];
        
        $this->db->insert("certificado", $certificado);
        return [ 'id_certificado'=>$this->db->insert_id(), 'codigo_autenticacao'=>$autenticacao ];
    }

    public function add_historico($id_usuario, $id_palestra)
    {
        $historico = ['id_usuario'=>$id_usuario,'id_palestra'=>$id_palestra, 'data'=>date("Y-m-d H:i:s"),'tipo'=>1, 'situacao'=>0 ];
        $this->db->insert("historico", $historico);
        
        return $this->db->insert_id();
    }
    
    public function get_historico_usuario($id_aluno)
    {
        $this->db->select();
        $this->db->from('aluno_curso');
        $this->db->where('id_aluno', $id_aluno);
        
        $this->db->order_by("id_historico", "DESC");
        
        return $this->db->get()->result_array();
    }
    
    public function get_historico_usuario_2($id_aluno, $limit=null, $start=null) 
    {
        
        $this->db->select();
        $this->db->from('aluno_curso');
        $this->db->where('id_aluno', $id_aluno);
        
        $this->db->order_by("id_historico", "DESC");
        $this->db->limit($limit, $start);
        
        return $this->db->get()->result_array();
    }
    
    public function get_palestra_by_id_palestra($id)
    {
        $this->db->select();
        $this->db->from('curso');
        
        $this->db->where('id_curso', $id);

        return $this->db->get()->result_array();
    }
    
    public function get_cursos_disponiveis()
    {
        $this->db->select();
        $this->db->from('curso');
        $this->db->where_in('id_curso', [635, 636]);
        
        $this->db->order_by("id_curso", 'DESC');
        
        return $this->db->get()->result_array();
    }
    
    public function get_cursos_disponiveis_2($limit = null, $start = null)
    {
        $this->db->select();
        $this->db->from('curso');
        $this->db->where_in('id_curso', [635, 636]);
        
        $this->db->order_by("id_curso", 'DESC');
        $this->db->limit($limit, $start);
        
        return $this->db->get()->result_array();
    }
    
    public function check_pagamento_historico($id_usuario, $id_historico)
    {
        $this->db->select();
        $this->db->from('aluno_curso');
        $this->db->join('item_transacao','aluno_curso.id_historico=item_transacao.historico');
        $this->db->join('transacao','item_transacao.transacao=transacao.id_transacao');
        
        $this->db->where('transacao.id_aluno', $id_usuario);
        $this->db->where('item_transacao.historico', $id_historico);
        $this->db->where('transacao.situacao', '3');
        
        $check_pagamento_historico = $this->db->get()->result_array();
        
        if( count($check_pagamento_historico)>0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function get_link_palestra($id_palestra)
    {
        $this->db->select("material.url");
        $this->db->from('modulo_palestra');
        $this->db->join('palestra', 'modulo_palestra.id_palestra = palestra.id_palestra');
        $this->db->join('modulo', 'modulo_palestra.id_modulo = modulo.id_modulo');
        $this->db->join('material_modulo', 'modulo.id_modulo = material_modulo.id_modulo');
        $this->db->join('material', 'material_modulo.id_material = material.id_material');
        
        $this->db->where('palestra.id_palestra', $id_palestra);
        
        return $this->db->get()->result_array();
    }
    

    public function get_historico_2($id_aluno, $limit=null, $start=null)
    {
        $this->db->select();
        $this->db->from('aluno_curso');
        $this->db->where('id_aluno', $id_aluno);
        $this->db->order_by("id_historico", "DESC");
        
        $this->db->limit($limit, $start);
        
        return $this->db->get()->result_array();
    }
    
    public function get_historico($id_aluno)
    {
        $this->db->select();
        $this->db->from('aluno_curso');
        $this->db->where('id_aluno', $id_aluno);
        $this->db->order_by("id_historico", "DESC");
        
        return $this->db->get()->result_array();
    }
    
    public function get_usuario_palestrante($id_usuario)
    {
        $this->db->select();
        $this->db->from('usuario');
        $this->db->join('palestra', 'usuario.id_usuario = palestra.id_usuario_palestrante');
        $this->db->where('usuario.id_usuario', $id_usuario);
        $this->db->limit(1);
        
        $usuario_palestrante = $this->db->get()->result_array();
        return $usuario_palestrante[0];
    }

    public function get_usuario_id_unico($usuario_id_unico)
    {
        $this->db->select();
        $this->db->from('aluno');
        $this->db->where('id_unico', $usuario_id_unico);
        
        return $this->db->get()->result_array();
    }

    public function usuario($dados)
    {
        $dados_aluno = $this->consulta_usuario($dados);
        if( count($dados_aluno)==0 )
        {
            return $this->add_usuario($dados);
        }
        else
        {
            $session = array(
                        'usuario_id_unico' => $dados_aluno[0]['id_unico'],
                        'usuario_nome' => $dados_aluno[0]['nome'],
                        'usuario_email' => $dados_aluno[0]['email'],
                        //'usuario_documento' => $dados_aluno[0]['documento'],
                        'usuario_logado' => TRUE
                    );
            $this->session->set_userdata($session);
            
            return $dados_aluno[0]['id_aluno'];
        }
    }

    public function consulta_usuario($dados_aluno)
    {
        $this->db->select();
        $this->db->from('aluno');
        $this->db->where('id_unico',$dados_aluno['usuario']['id_unico']);
        $this->db->where('situacao', 1);
        
        return $this->db->get()->result_array();
    }
    
    private function add_usuario($dados_aluno)
    {
        $usuario = ['id_unico'=>$dados_aluno['usuario']['id_unico'],'nome'=>$dados_aluno['usuario']['nome'], 'email'=>$dados_aluno['usuario']['email'], 'situacao_cadastro'=>1, 'situacao'=>1 ];
        $this->db->insert("aluno", $usuario);
        
        $session = array(
                        'usuario_id_unico' => $dados_aluno['usuario']['id_unico'],
                        'usuario_nome' => $dados_aluno['usuario']['nome'],
                        'usuario_email' => $dados_aluno['usuario']['email'],
                        //'usuario_documento' => $dados_aluno['usuario']['documento'],
                        'usuario_logado' => TRUE
                    );
        $this->session->set_userdata($session);
        
        return $this->db->insert_id();
    }

    public function instituicao($dados_instituicao)
    {
        $instituicao = $this->consulta_instituicao($dados_instituicao);

        if( count($instituicao)==0 )
        {
            return $this->add_instituicao($dados_instituicao);
        }
        else
        {
            return $instituicao[0]['id_instituicao'];
        }
    }
    
    private function consulta_instituicao($dados_instituicao)
    {
        $this->db->select();
        $this->db->from('instituicao');
        $this->db->where('id_externo',$dados_instituicao['dados']['id']);
        $this->db->where('situacao',1);
        
        $instituicao = $this->db->get()->result_array();
        
        return $instituicao;
    }
    
    private function add_instituicao($dados_instituicao)
    {
        $instituicao = ['id_externo'=>$dados_instituicao['dados']['id'], 'nome'=>$dados_instituicao['dados']['nome'], 'identificador_externo'=>$dados_instituicao['dados']['identificador'], 'situacao'=>1 ];
        $this->db->insert("instituicao", $instituicao);
        
        return $this->db->insert_id();
    }
    
    public function instituicao_usuario($id_usuario, $id_instituicao)
    {
        $instituicao_usuario = $this->consulta_instituicao_usuario($id_usuario);
        
        if( count($instituicao_usuario)==0 )
        {
            $this->add_instituicao_usuario($id_usuario, $id_instituicao);
        }
    }

    private function consulta_instituicao_usuario($id_usuario)
    {
        $this->db->select();
        $this->db->from('aluno_instituicao');
        $this->db->where('id_aluno', $id_usuario);
        $instituicao_usuario = $this->db->get()->result_array();
        
        return $instituicao_usuario;
    }

    
    public function add_instituicao_usuario($id_usuario, $id_instituicao)
    {
        $instituicao_usuario = ['id_aluno'=>$id_usuario, 'id_instituicao'=>$id_instituicao];
        $this->db->insert("aluno_instituicao", $instituicao_usuario);
    }

    public function usuario_termo_uso($id_usuario)
    {
        $this->db->select();
        $this->db->from('aluno_termo');
        $this->db->where('id_aluno', $id_usuario);
        $usuario_termo_uso = $this->db->get()->result_array();
        
        if( count($usuario_termo_uso)>0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }

    public function insert_usuario_termo_uso($dados)
    {
        return $this->db->insert("aluno_termo", $dados);
    }

    public function termo_uso_ativo()
    {
        $this->db->select();
        $this->db->from('termo');
        $this->db->where('status','1');
        $termo_uso_ativo = $this->db->get()->result_array();
        
        return $termo_uso_ativo[0];
    }
}