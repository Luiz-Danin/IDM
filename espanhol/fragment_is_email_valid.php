<?php
require_once '../ingles/config.php';
require_once 'EntidadeInterface.php';
require_once 'Aluno.php';
require_once 'ServiceDB.php';

$aluno = new Aluno();
$serviceDB = new ServiceDb($conexao, $aluno);
$senha = $serviceDB->findEmailEmptyPassword( $_GET['login']);

if( is_null($senha['senha']) )
	echo json_encode('null');
else if ($senha['senha']=='')
	echo json_encode('false');
else if ( isset($senha['senha']) )
	echo json_encode('true');