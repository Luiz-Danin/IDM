<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
echo '<form action="geraPagamento.php" class="form-header" id="formLogin"  method="POST" target="_blank">
				<input type="hidden" name="flagOpcaPagamento" value="CartaoCredito">
				<div class="form-group">
										<select name="Forma" required="" class="form-control" id="Forma" >
											<option value="">Selecione a Bandeira do Cartão</option>
											<option value="visa"">Visa</option>
											<option value="mastercard">Master Card</option>
											<option value="amex">American Express</option>
											<option value="diners">Diners Club International</option>
											<option value="elo">Elo</option>
											<option value="aura">Aura</option>
											<option value="jcb">JCB</option>
										</select>
				</div>
				<!--<div class="form-group">
										<select name="parcelas" required="" class="form-control" id="parcelas" >
										<option value="">Selecione a quantidade de Parcelas</option>
											<option value="1"">Valor: 1 x R$135,00</option>
											<option value="2">Valor: 2 x R$67,50</option>
											<option value="3">Valor: 3 x R$45,00</option>
											<option value="4">Valor: 4 x R$33,75</option>
											<option value="5">Valor: 5 x R$27,00</option>
											<option value="6">Valor: 6 x R$22,50</option>
										</select>
				</div>-->

							<div class="form-group">
								<input autocomplete="off" class="form-control input-lg" name="CartaoNumero" id="CartaoNumero" minlength="14" maxlength="22"
																	type="text" placeholder="Número do Cartão  (Somente Números)" pattern="\d{14,22}" required title="Digite o Número do Cartão">
							</div>
							<div class="form-group">
								<input autocomplete="off" class="form-control input-lg" name="NomePortador" id="NomePortador" pattern="[A-zÀ-ž]+[ ][A-zÀ-ž\s]+"
																	type="text" placeholder="Nome do Portador (Igual ao do Cartão)" required title="Digite o Nome do Portador do Cartão">
							</div>
							<div class="form-group">
								<input autocomplete="off" class="form-control input-lg" name="Expiracao" id="Expiracao" maxlength="5" onkeypress="mascaraExpiracao( this, event )"
																	type="text" placeholder="Validade (Ex: 06/22)"  required title="Digite a Validade do Cartão">
							</div>
							<div class="form-group">
								<input autocomplete="off" class="form-control input-lg" name="CodigoSeguranca" id="CodigoSeguranca" maxlength="4"
																	type="text" placeholder="Código de Segurança (Ex:495)" required title="Código de Segurança (Geralmente impresso no verso do Cartão. Ex:495)">
							</div>
							<div class="form-group last">
								<input id="submitLogin" name="enviar" type="submit" class="btn-secondary btn-block btn-lg" value="Finalizar">
								<div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
							</div>
							<p class="privacy text-center">Seu Pagamento será Processado pela<a href="https://www.cielo.com.br/" target="_blank"> 
																					<img src="images/cielo.png" width="40"> </a></p>
		</form>';
?>
<script type="text/javascript">
function mascaraExpiracao( campo, e )
{
	var kC = (document.all) ? event.keyCode : e.keyCode;
	var data = campo.value;
	
	if( kC!=8 && kC!=46 )
	{
		if( data.length==2 )
		{
			campo.value = data += '/';
		}
		else
			campo.value = data;
	}
}
</script>
<script type="text/javascript">
$("#parcelas").change(function () {
/*   var str = "";
  var total = 135.00;
  
  str = $( "#parcelas option:selected" ).val();
  var valor_parcelado = (total/str);
  var valor_parcelado = parseFloat(valor_parcelado).toFixed(2);
  var valor_parcelado = valor_parcelado.replace(".",",")
  
  $("p#quantidadeParcela").text("Valor: "+str+" x R$"+valor_parcelado); */

  //$('#parcelas option:selected').text("Valor: "+str+" x R$"+valor_parcelado);
});

</script>
<script type="text/javascript">
$(document).ready(function () {
	
	  $("#CartaoNumero").keypress(function (e) {
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	               return false;
	    }
	   });

	  $("#CodigoSeguranca").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });


	  $("#cpf").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });

	  $("#Expiracao").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });

	  
});
</script>
<script>
$(document).ready(function () {
	$('#NomePortador').keypress(function(key) {
		console.log(key.charCode);
		if ((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) 
				&& (key.charCode != 45) && (key.charCode != 32) && (key.charCode != 0) && (key.charCode != 224) 
				&& (key.charCode != 225) && (key.charCode != 193) && (key.charCode != 227) 
				&& (key.charCode != 233) && (key.charCode != 193) && (key.charCode != 201) && (key.charCode != 205) 
				&& (key.charCode != 237) && (key.charCode != 211) && (key.charCode != 243) 
				&& (key.charCode != 250) && (key.charCode != 218) && (key.charCode != 227) 
				&& (key.charCode != 245) && (key.charCode != 244)  && (key.charCode != 231) 
				&& (key.charCode != 195) && (key.charCode != 213) && (key.charCode != 38) && (key.charCode != 212) 
				&& (key.charCode != 199) ) return false;

	});
});
</script>
