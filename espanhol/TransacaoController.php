<?php
require_once 'EntidadeInterfaceTransacao.php';
require_once 'Transacao.php';
require_once 'ServiceDBTransacao.php';

class TransacaoController
{
	public function saveTransacao(Request $request, $id_transacao, $id_aluno,$conexao)
	{
		if ( $request->isElement('enviar') )
		{
			$transacao = new Transacao();
			$transacao->setIdTransacao( $id_transacao );
			$transacao->setIdAluno($id_aluno);
			$transacao->setDataTransacao( date('Y-m-d H:i:s') );
			$transacao->setDescricao('Compra Curso de Idiomas Espanhol' );
			$transacao->setSituacao('0');
			$transacao->setTipoPagamento('0');
			$transacao->setValor( (float)14.90);
			$transacao->setDataVencimento( date('Y-m-d H:i:s') );
			$transacao->setIdInstituicaoUnidade(2);
			
			$servideDbTransacao = new ServiceDBTransacao($conexao, $transacao);
			$isTransacao = $servideDbTransacao->isVerifyTransacao();
			
			if (!$isTransacao)
			{
				$servideDbTransacao->inserirTransacao();
			}
                        else
                        {
                            return false;
                        }
				
		}
	}
	
	public function getDataTransacaoByIdTransacao($id_transacao, $conexao)
	{
		
		if ($id_transacao)
		{
			$transacao = new Transacao();
			$transacao->setIdTransacao( $id_transacao );
			
			$serviceDbTransacao = new ServiceDBTransacao($conexao, $transacao);
			$dataTransacao = $serviceDbTransacao->findTransacaoByIdTransacao();
			
			return $dataTransacao;
		}
		else 
			return false;
	}
	
	public function saveLogTransacao($id_transacao, $id_aluno, $token, $conexao, $intermediadorPagamento)
	{
		$transacao = new Transacao();
		$serviceDbTransacao = new ServiceDBTransacao($conexao, $transacao);
		
		$serviceDbTransacao->inserirLogTransacao($id_transacao, $id_aluno, $token, $intermediadorPagamento);
		
	}
        
        public function saveLogTransacaoCredito($id_transacao, $id_aluno, $token, $conexao, $intermediadorPagamento, $situacao)
        {
            $transacao = new Transacao();
            $serviceDbTransacao = new ServiceDBTransacao($conexao, $transacao);
            $serviceDbTransacao->inserirLogTransacaoCredito($id_transacao, $id_aluno, $token, $intermediadorPagamento, $situacao);
        }
        
        public function updateTransacao($id_transacao, $situacao, $tipo_pagamento, $conexao)
        {
            if( !empty($id_transacao) && !empty($situacao) && !empty($tipo_pagamento) )
            {
                $transacao = new Transacao();
                $transacao->setIdTransacao($id_transacao);
                $transacao->setSituacao($situacao);
                $transacao->setTipoPagamento($tipo_pagamento);

                $serviceDbTransacao = new ServiceDBTransacao($conexao, $transacao);
                return $serviceDbTransacao->updateTransacao();
            }
            
        }
	
}
