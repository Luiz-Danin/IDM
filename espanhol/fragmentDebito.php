<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
echo '<form action="geraPagamento.php" class="form-header" id="formLogin"  method="POST" target="_blank">
					<input type="hidden" name="flagOpcaPagamento" value="Debito">
					<div class="form-group">
										<select name="Forma" required="" class="form-control selectpicker" id="Forma" >
											<option value="">Selecione o Banco</option>
											<option value="22"">Bradesco</option>
											<option value="23">Banco do Brasil</option>
											<option value="7">Itaú</option>
											<option value="21">HSBC</option>
											<option value="14">Pella</option>
										</select>
					</div>
							<p></p>
							<div class="form-group">
								<input autocomplete="off" class="form-control input-lg" name="cpf" id="cpf" type="text" minlength="11" maxlength="11" placeholder="CPF (Somente Números)" pattern="[0-9]{3}[0-9]{3}[0-9]{3}[0-9]{2}" title="Digite seu CPF" required>
							</div>
							<div class="form-group last">
								<input id="submitLogin" name="enviar" type="submit" class="btn-secondary btn-block btn-lg" value="Finalizar">
								<div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
							</div>
							<p class="privacy text-center">Seu Pagamento será Processado por<a href="https://www.traycheckout.com.br/" target="_blank">  
																					<img src="images/tray.png" width="40"> </a></p>
		</form>';
?>
<script type="text/javascript">
$(document).ready(function () {
	  $("#cpf").keypress(function (e) {
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	               return false;
	    }
	   });
});
</script>