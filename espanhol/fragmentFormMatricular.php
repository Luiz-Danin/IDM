<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
session_start();
require_once '../ingles/config.php';
require_once 'EntidadeInterface.php';
require_once 'Aluno.php';
require_once 'ServiceDB.php';

$aluno = new Aluno();
$serviceDB = new ServiceDb($conexao, $aluno);

$dadosMatricula = $serviceDB->findDadosMatricula( $_SESSION['id_aluno'], $_POST['hash']);


echo $html = '<div class="col-md-5" id="tela">
							<div class="signup-header wow fadeInUp">
								<h3 class="form-title text-center">Confirmar Dados</h3>
								<div id="feedback"></div>
								<form class="form-header" method="POST">
									<input type="hidden" name="flagMatricula" value="4">
									<input type="hidden" name="id_aluno" id="id_aluno" value="'.$_SESSION['id_aluno'].'">
									<div class="form-group">
										<input value="'.$dadosMatricula['cep'].'" class="form-control input-lg" name="cep" id="cep"
											type="text" placeholder="CEP (Somente Números)"  maxlength="8" pattern="\d{8}" required title="Somente números, apenas 8 digitos">
									</div>
									<div class="form-group">
										<input value="'.$dadosMatricula['endereco'].'" class="form-control input-lg" name="endereco" id="endereco"
											type="text" placeholder="Endereço" pattern="([A-z0-9À-ž\s]){2,}" required title="Digite seu Endereço">
									</div>
									<div class="form-group">
										<input value="'.$dadosMatricula['numero'].'" class="form-control input-lg" name="numero" id="numero"
											type="text" placeholder="Número" pattern="\d*" required title="Somente Números">
									</div>
									<div class="form-group">
										<input value="'.$dadosMatricula['bairro'].'" class="form-control input-lg" name="bairro" id="bairro"
											type="text" placeholder="Bairro" pattern="[A-zÀ-ž\s]{4,}" required title="Digite seu Bairro">
									</div>
									<div class="form-group">
										<input value="'.$dadosMatricula['cidade'].'" class="form-control input-lg" name="cidade" id="cidade"
											type="text" placeholder="Cidade" pattern="[A-zÀ-ž]+" required title="Digite sua Cidade">
									</div>';
?>
									
									<div class="form-group">
										<select name="estado" required="" class="form-control" id="estado">
											<option class="estado" value="" <?=($dadosMatricula['estado']== 'Não Informado')?'selected':''?> >Selecione</option>
                            				<option class="estado" value="AC" <?=($dadosMatricula['estado']== 'AC')?'selected':''?> >Acre</option>
											<option class="estado" value="AL" <?=($dadosMatricula['estado']== 'AL')?'selected':''?> >Alagoas</option>
											<option class="estado" value="AM" <?=($dadosMatricula['estado']== 'AM')?'selected':''?> >Amazonas</option>
											<option class="estado" value="AP" <?=($dadosMatricula['estado']== 'AP')?'selected':''?> >Amapá</option>
											<option class="estado" value="BA" <?=($dadosMatricula['estado']== 'BA')?'selected':''?> >Bahia</option>
											<option class="estado" value="CE" <?=($dadosMatricula['estado']== 'CE')?'selected':''?> >Ceará</option>
											<option class="estado" value="DF" <?=($dadosMatricula['estado']== 'DF')?'selected':''?> >Distrito Federal</option>
											<option class="estado" value="ES" <?=($dadosMatricula['estado']== 'ES')?'selected':''?> >Espirito Santo</option>
											<option class="estado" value="GO" <?=($dadosMatricula['estado']== 'GO')?'selected':''?> >Goiás</option>
											<option class="estado" value="MA" <?=($dadosMatricula['estado']== 'MA')?'selected':''?> >Maranhão</option>
											<option class="estado" value="MG" <?=($dadosMatricula['estado']== 'MG')?'selected':''?> >Minas Gerais</option>
											<option class="estado" value="MS" <?=($dadosMatricula['estado']== 'MS')?'selected':''?> >Mato Grosso do Sul</option>
											<option class="estado" value="MT" <?=($dadosMatricula['estado']== 'MT')?'selected':''?> >Mato Grosso</option>
											<option class="estado" value="PA" <?=($dadosMatricula['estado']== 'PA')?'selected':''?> >Pará</option>
											<option class="estado" value="PB" <?=($dadosMatricula['estado']== 'PB')?'selected':''?> >Paraíba</option>
											<option class="estado" value="PE" <?=($dadosMatricula['estado']== 'PE')?'selected':''?> >Pernambuco</option>
											<option class="estado" value="PI" <?=($dadosMatricula['estado']== 'PI')?'selected':''?> >Piauí</option>
											<option class="estado" value="PR" <?=($dadosMatricula['estado']== 'PR')?'selected':''?> >Paraná</option>
											<option class="estado" value="RJ" <?=($dadosMatricula['estado']== 'RJ')?'selected':''?> >Rio de Janeiro</option>
											<option class="estado" value="RN" <?=($dadosMatricula['estado']== 'RN')?'selected':''?> >Rio Grande do Norte</option>
											<option class="estado" value="RO" <?=($dadosMatricula['estado']== 'RO')?'selected':''?> >Rondônia</option>
											<option class="estado" value="RR" <?=($dadosMatricula['estado']== 'RR')?'selected':''?> >Roraima</option>
											<option class="estado" value="RS" <?=($dadosMatricula['estado']== 'RS')?'selected':''?> >Rio Grande do Sul</option>
											<option class="estado" value="SC" <?=($dadosMatricula['estado']== 'SC')?'selected':''?> >Santa Catarina</option>
											<option class="estado" value="SE" <?=($dadosMatricula['estado']== 'SE')?'selected':''?> >Sergipe</option>
											<option class="estado" value="SP" <?=($dadosMatricula['estado']== 'SP')?'selected':''?> >São Paulo</option>
											<option class="estado" value="TO" <?=($dadosMatricula['estado']== 'TO')?'selected':''?> >Tocantins</option>
										</select>
									</div>
<?php
									
									echo '<div class="form-group row">
										<div class="form-group col-xs-6 col-sm-4 col-md-3 col-lg-3">
											<input value="'.$dadosMatricula['ddd_celular'].'" class="form-control input-lg phone" name="ddd"
											id="ddd" type="text" placeholder="DDD" maxlength="2" pattern="\d{2}" required title="Somente números, apenas 2 digitos">
										</div>
										<div class="form-group col-xs-6 col-sm-8 col-md-9 col-lg-9">
											<input value="'.$dadosMatricula['celular'].'" class="form-control input-lg phone" name="celular"
											id="celular" type="text" placeholder="Celular (Somente Números)" maxlength="9" pattern="\d{8}|\d{9}" required title="Somente números, 8 ou 9 dígitos">
										</div>
									</div>
									<div class="form-group last">
										<input name="enviar" id="submitFormMatricula" type="submit" class="btn-secondary btn-block btn-lg" value="Continuar">
										<div class="fb-login-button" data-max-rows="1"
											data-size="small" data-button-type="login_with"
											data-show-faces="false" data-auto-logout-link="false"
											data-use-continue-as="false">
									</div>
									</form>
									<p></p>
								</div>
							</div>';
?>
<script>
$( document ).ready(function() {
	$("#submitFormMatricula").click(function() {

			var cep = $("#cep").val();
			var endereco = $("#endereco").val();
			var numero = $("#numero").val();
			var bairro = $("#bairro").val();
			var cidade = $("#cidade").val();
			var estado = $('#estado option:selected').val();
			var ddd =  $("#ddd").val();;
			var celular =  $("#celular").val();
			var id_aluno = $("#id_aluno").val();

			if(cep.length!==0 && endereco.length!==0 && numero.length!==0 && bairro.length!==0 && cidade.length!==0 && estado.length!==0 && ddd.length!==0 && celular.length!==0)
			{
				$.ajax(
						{
						  type: "POST",
						  url: 'fragmentPagamento.php',
						  data: { cep:cep, endereco:endereco, numero:numero, bairro:bairro, cidade:cidade, estado:estado, ddd:ddd, celular:celular, id_aluno:id_aluno},
						  dataType: "html",
						  success: function (data) {
							  window.stop();
							$("#form").html(data);
						        },
					      error: function() {
					           console.log('error');
					        }
						});
			}

			return false;
	});
});
</script>
<script>
$( document ).ready(function() {

	function limpa_formulário_cep() {
        $("#endereco").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#estado").val("");
    }

	$('.close').click(function () {
	      $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
	    });

	$("#cep").blur(function() {

        var cep = $(this).val().replace(/\D/g, '');
        
        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if(validacep.test(cep)) {

                $("#endereco").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                //$("#estado").val("...");

                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        $("#endereco").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#numero").val("");
                        $("#estado").val(dados.uf);
                        $(".alert").hide();
                    }
                    else {
                        limpa_formulário_cep();

                        $("#feedback").html("<div class='alert btn-info text-center' role='alert'>" +
                                "<a href='#' class='close' data-dismiss='alert' aria-label='Close'>&times;</a>Atenção: CEP não encontrado.</div>");

                        $(".alert").click(function() {
                            $(".alert").hide();
                          });
                    }
                });
            }
            else {
                limpa_formulário_cep();
                
                $("#feedback").html("<div class='alert btn-info text-center' role='alert'>" +
                "<a href='#' class='close' data-dismiss='alert' aria-label='Close'>&times;</a>Atenção: Formato de CEP inválido.</div>");
                
		        $(".alert").click(function() {
		            $(".alert").hide();
		          });
            }
        }
        else {
            limpa_formulário_cep();
        }
	});
});
</script>
<script type="text/javascript">
$(document).ready(function () {
	  $("#celular").keypress(function (e) {
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	               return false;
	    }
	   });

	  $("#ddd").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });

	  $("#cep").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });

	  $("#numero").keypress(function (e) {
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		               return false;
		    }
		   });
	});
</script>
<script>
    $('#cidade').keydown(function(e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
</script>