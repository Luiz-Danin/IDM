<?php
require_once 'EntidadeInterfaceTransacao.php';

class Transacao implements EntidadeInterfaceTransacao
{
	private $table = "transacao";
	private $id_transacao;
	private $id_aluno;
	private $data_transacao;
	private $descricao;
	private $situacao;
	private $tipo_pagamento;
	private $valor;
	private $data_vencimento;
	private $id_instituicao_unidade;
	
	
	/**
	 * @param string $table
	 */
	public function setTable($table)
	{
		$this->table = $table;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTable()
	{
		return $this->table;
	}
	
	public function getIdTransacao()
	{
		return $this->id_transacao;
	}
	
	public function setIdTransacao($id_transacao)
	{
		$this->id_transacao = $id_transacao;
	}
	
	public function getIdAluno()
	{
		return $this->id_aluno;
	}
	
	public function setIdAluno($id_aluno)
	{
		$this->id_aluno = $id_aluno;
	}
	
	public function getDataTransacao()
	{
		return $this->data_transacao;
	}
	
	public function setDataTransacao($data_transacao)
	{
		$this->data_transacao = $data_transacao;
	}
	
	public function getDescricao()
	{
		return $this->descricao;
	}
	
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
	}
	
	public function getSituacao()
	{
		return $this->situacao;
	}
	
	public function setSituacao($situacao)
	{
		$this->situacao = $situacao;
	}
	
	public function getTipoPagamento()
	{
		return $this->tipo_pagamento;
	}
	
	public function setTipoPagamento($tipo_pagamento)
	{
		$this->tipo_pagamento = $tipo_pagamento;
	}
	
	public function getValor()
	{
		return $this->valor;
	}
	
	public function setValor($valor)
	{
		$this->valor = $valor;
	}
	
	public function getDataVencimento()
	{
		return $this->data_vencimento;
	}
	
	public function setDataVencimento($data_vencimento)
	{
		$this->data_vencimento = $data_vencimento;
	}
	
	public function getIdInstituicaoUnidade()
	{
		return $this->id_instituicao_unidade;
	}
	
	public function setIdInstituicaoUnidade($id_instituicao_unidade)
	{
		$this->id_instituicao_unidade = $id_instituicao_unidade;
	}
	
}