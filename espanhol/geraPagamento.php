<?php
session_start();
require_once '../ingles/config.php';
require_once 'AlunoController.php';
require_once 'TransacaoController.php';
require_once 'Request.php';

$id_aluno = $_SESSION['id_aluno'];

$alunoController = new AlunoController();
$id_transacao = $alunoController->generateIdTransacaoAluno($conexao, $id_aluno);

$transacaoController = new TransacaoController();
$transacaoController->saveTransacao(new Request(), $id_transacao, $id_aluno, $conexao);

$transacaoAluno = $transacaoController->getDataTransacaoByIdTransacao($id_transacao, $conexao);

$alunoTransacao = $alunoController->getAlunoByIdAluno($conexao, $id_aluno);

$request = new Request();

if ( $request->getKey('flagOpcaPagamento')=='CartaoCredito' )
{
	$nome = $alunoTransacao['nome'];
	$email = $alunoTransacao['email'];
	$telefone = $alunoTransacao['celular'];
	$endereco = $alunoTransacao['endereco'];
	$numero = $alunoTransacao['numero'];
	$cep = $alunoTransacao['cep'];
	$bairro = $alunoTransacao['bairro'];
	$cidade = $alunoTransacao['cidade'];
	$estado = $alunoTransacao['estado'];
	$ddd = $alunoTransacao['ddd_celular'];
	
	$Forma= $request->getKey('Forma');
	$cpf = $request->getKey('cpf');
	
	//$Parcelas = ( $request->getKey('parcelas')  ) ? $request->getKey('parcelas') : '';
        $Parcelas = 1;
	$numerocartao= ( $request->getKey('CartaoNumero') ) ? $request->getKey('CartaoNumero') : '';
	$NomePortador= ( $request->getKey('NomePortador') ) ?  $request->getKey('NomePortador') : '';
	$Expiracao= ( $request->getKey('Expiracao') ) ? $request->getKey('Expiracao') : '';
	$CodigoSeguranca= ( $request->getKey('CodigoSeguranca') ) ? $request->getKey('CodigoSeguranca') : '';
	
	
	
/* 	$Parcelas = 1;
	$Forma = $this->input->post('meiopagamento');
	$numerocartao = $this->input->post('CartaoNumero');
	$Expiracao = $this->input->post('Expiracao');
	$CodigoSeguranca = $this->input->post('CodigoSeguranca');
	$NomePortador = $this->input->post('NomePortador'); */
	if ($Expiracao == '') {
		$Expiracao = "00/00";
	}
	$ext = explode("/", $Expiracao);
	$mesvalidade = $ext[0];
	$anovalidade = $ext[1] + 2000;
	$va = $anovalidade . $mesvalidade;
	$agora = date('Y-m-d\TH:i:s');
	
	$valor = ($transacaoAluno['valor'])*100;//descomentar em produção
        //$valor = 100;//comentar em produção
	
	$descricao = $transacaoAluno['descricao'];
	$id_transacao = $id_transacao;
	
	if ($Parcelas > 1) {
		$parcelamento = 2;
	}
	
	if ($Parcelas == 1) {
		$parcelamento = 1;
	}
	
	$string = <<<XML
<?xml version='1.0' encoding="ISO-8859-1"?>
<requisicao-transacao id='1030463848' versao='1.2.1'>
	 <dados-ec>
	        <numero>1030463848</numero>
	        <chave>28b92fc1ac9d6b9527af579a99ac03611791458416b43b0d24423ddafac68d24</chave>
	 </dados-ec>
	<dados-portador>
        <numero>$numerocartao</numero>
        <validade>$va</validade>
        <indicador>1</indicador>
        <codigo-seguranca>$CodigoSeguranca</codigo-seguranca>
    </dados-portador>
 <dados-pedido>
        <numero>$id_transacao</numero>
        <valor>$valor</valor>
        <moeda>986</moeda>
        <data-hora>$agora</data-hora>
        <descricao>$descricao</descricao>
        <idioma>PT</idioma>
        <soft-descriptor>Curso Online</soft-descriptor>
    </dados-pedido>
 	<forma-pagamento>
        <bandeira>$Forma</bandeira>
        <produto>$parcelamento</produto>
        <parcelas>$Parcelas</parcelas>
    </forma-pagamento>
 <url-retorno>null</url-retorno>
    <autorizar>3</autorizar>
    <capturar>true</capturar>
</requisicao-transacao>
XML;
	
     
	$url = 'https://ecommerce.cbmp.com.br/servicos/ecommwsec.do';
	
	$ch = curl_init();
	flush();
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, 'mensagem=' . $string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 40);
	$string = curl_exec($ch);
	
	curl_close($ch);
	$xml = simplexml_load_string($string);
	$pg = 0;
	
	if ($xml->tid) {
		$td = (String) $xml->tid;
		$cod_status = $xml->autorizacao->codigo;
		
		if ($cod_status == 5) {
			//$cod_status_banco = 7;
                        
                        $token = $xml->tid;
                        $intermediadorPagamentoCartao = 'cielo';
                        $tipo_pagamento = 'CartaoDeCredito';
                        $situacao_banco = "4";
                        
			echo"<script>alert('Transacao nao Aprovada!'); </script>";
			echo'<script>location.href="https://www.idmcursos.com.br/espanhol/"</script>';
                        $transacaoController->saveLogTransacaoCredito($id_transacao, $id_aluno, $token, $conexao, $intermediadorPagamentoCartao,$situacao);
                        $transacaoController->updateTransacao($id_transacao, $situacao_banco, $tipo_pagamento, $conexao);
		}
		
		if ($cod_status == 4 || $cod_status == 6) {
			echo" utf8_decode(<script>alert('Transacao Aprovada!'); </script>)";
			$isMatricula = $alunoController->saveMatriculaAluno($conexao, $id_aluno, new Request() );
                        
                        $token = $xml->tid;
                        
                        $intermediadorPagamentoCartao = 'cielo';
                        $transacaoController->saveLogTransacao($id_transacao, $id_aluno, $token, $conexao, $intermediadorPagamentoCartao);
                        
                        $tipo_pagamento = 'CartaoDeCredito';
                        $situacao_banco = "3";
                        $transacaoController->updateTransacao($id_transacao, $situacao_banco, $tipo_pagamento, $conexao);
			echo'<script>location.href="https://www.idmcursos.com.br/espanhol/"</script>';
		}
		
	}
    
    else{
     echo"<script>alert('Transacao nao Aprovada!'); </script>";
	 echo'<script>location.href="https://www.idmcursos.com.br/espanhol/"</script>'; 
    }
		//print_r($xml);
	
}
elseif ( $request->getKey('flagOpcaPagamento')=='BoletoBancario' ||  $request->getKey('flagOpcaPagamento')=='Debito')
{
	$nome = $alunoTransacao['nome'];
	$email = $alunoTransacao['email'];
	$telefone = $alunoTransacao['celular'];
	$endereco = $alunoTransacao['endereco'];
	$numero = $alunoTransacao['numero'];
	$cep = $alunoTransacao['cep'];
	$bairro = $alunoTransacao['bairro'];
	$cidade = $alunoTransacao['cidade'];
	$estado = $alunoTransacao['estado'];
	$ddd = $alunoTransacao['ddd_celular'];
	
	$cpf = $request->getKey('cpf');
	
	$TelefoneFixo = $ddd.$telefone;
	
	
	$params['token_account'] = 'a44677dac7cba94';        
//	$params['token_account'] = '32e9c1b008b07a0';     
	$params['customer[name]'] = $nome;
	$params['customer[cpf]'] = $cpf;
	$params['customer[email]'] = $email;
	$params['customer[contacts][][type_contact]'] = 'M';
	$params['customer[contacts][][number_contact]'] = $TelefoneFixo;
	
	$params['customer[addresses][][type_address]'] = 'B';
	$params['customer[addresses][][postal_code]'] =$cep;
	$params['customer[addresses][][street]'] = $endereco;
	$params['customer[addresses][][number]'] = $numero;
	$params['customer[addresses][][completion]'] = 'A';
	$params['customer[addresses][][neighborhood]'] =$bairro;
	$params['customer[addresses][][city]'] = $cidade;
	$params['customer[addresses][][state]'] =$estado;
	/* Dados da Transação */
	$params['transaction[available_payment_methods]'] = '2,3,4,5,6,7,14,15,16,18,19,21,22,23';
	$params['transaction[order_number]'] =$id_transacao;
	$params['transaction[customer_ip]'] = $_SERVER["REMOTE_ADDR"];
	$params['transaction[url_notification]'] = 'http://cursosapp.com.br/gestao/retornos/RetornoIdmcursos';
	
	/* Dados do Produto 1 - Notebook Preto */
	$params['transaction_product[][description]'] = $transacaoAluno['descricao'];
	$params['transaction_product[][quantity]'] = '1';
	$params['transaction_product[][price_unit]'] = $transacaoAluno['valor'];
	$params['transaction_product[][code]'] = '1';
	// $params['transaction_product[][sku_code]'] = '0001';
	$params['transaction_product[][extra]'] = '';
	
	/* Dados para Pagamento */
	$params['payment[payment_method_id]'] = $request->getKey('Forma');
	$params['payment[split]'] = '01';//quantidade de parcelas
	$params['payment[card_name]'] = $NomePortador;//Nome Impresso no Cartão
	$params['payment[card_number]'] =$numerocartao;//Numero Impresso no Cartão
	$params['payment[card_expdate_month]'] =$mesvalidade;//Mês validade
	$params['payment[card_expdate_year]'] =$anovalidade;//Ano Validade
	$params['payment[card_cvv]'] =$CodigoSeguranca;//Código de Segurança Impresso no Cartão
	
	$urlPost = "https://api.sandbox.traycheckout.com.br/v2/transactions/pay_complete";
//	$urlPost = "https://api.traycheckout.com.br/v2/transactions/pay_complete";
	
	ob_start();
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $urlPost);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt ($ch, CURLOPT_SSLVERSION, 6);
	//curl_setopt ($ch, CURLOPT_SSLVERSION,'CURL_SSLVERSION_TLSv1_2');
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_exec($ch);
	
	/* XML de retorno */
	$resposta = simplexml_load_string(ob_get_contents());
	
	ob_end_clean();
	curl_close($ch);
	
	if($resposta->message_response->message == "success"){
		//Tratamento dos dados de resposta da consulta.
		$link=$resposta->data_response->transaction->payment->url_payment;
		$token=$resposta->data_response->transaction->token_transaction;
                
                $intermediadorPagamento = 'traycheckout';
		$transacaoController->saveLogTransacao($id_transacao, $id_aluno, $token, $conexao, $intermediadorPagamento);
		
		header("location: $link"); 
	}
	else {
		$message_complete = (array)$resposta->error_response->validation_errors->validation_error->message_complete;
		$message_complete = utf8_decode("Atenção: Houve um erro ao processar sua transação de pagamento,".$message_complete[0]);
		echo "<script>alert('$message_complete!')</script>";
		echo '<meta http-equiv="refresh" content="0; url=./" />';
	}
	//print_r($resposta);
}
//echo "<script>window.close();</script>";
?>