<?php
$senha = ( isset($_COOKIE["senha"]) ) ? $_COOKIE["senha"] : '';

echo $html='<div class="col-md-5" id="tela">
		
					<div class="signup-header wow fadeInUp">
						<h3 class="form-title text-center">Login</h3>
						<div class="alert alert-success">Por favor, digite a sua senha de acesso.</div>
						<form action="" class="form-header" id="formLogin"  method="POST">
						 <input type="hidden" name="flagLogin" value="0">
							<div class="form-group">
								<input value="'.$_GET['email'].'" class="form-control input-lg" name="email" id="login" type="email" placeholder="Seu E-mail" required>
							</div>
							<div class="form-group">
								<input class="form-control input-lg" value="'.$senha.'" name="senha" id="password" type="password" placeholder="Senha" pattern=".{6,}" title="Mínimo 6 dígitos" required>
							</div>
							<div class="checkbox">
							<label>
							<input name="check" type="checkbox"><p>Lembrar-me</p></label>
							</div>
							<div class="form-group last">
								<input id="submitLogin" name="enviar" type="submit" class="btn-secondary btn-block btn-lg" value="Entrar">
								<div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
							</div>
							<div id="recuperar" class="btn-group">
							<a class="wow fadeInUp animated" href="#" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-envelope"></i> Recuperar Senha</a>
							</div>
							<p></p>
						</form>
					</div>
										
				</div>

			<div id="myModalRecuperar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			        <h3 id="myModalLabel">Recuperação de senha</h3>
			      </div>
			      <div class="modal-body">
			       <p style="color:black">Verifique sua caixa de E-mail, senha enviada para o E-mail: <strong>'.$_GET['email'].'</strong> </p>
			      </div>
			      <div class="modal-footer">
			        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
			      </div>
			    </div>
			  </div>
			</div>';
?>
<script>
$(document).ready(function() {

	$("#recuperar").click(function() {

		var login = $("#login").val();

		$.ajax(
					{
					  type: "POST",
					  url: 'recuperarSenha.php',
					  data: { login:login},
					 success: function (data) {
						 $('#myModalRecuperar').modal('show');
						 console.log('success');
					        },
				        error: function() {
				        	alert('Problemas ao Enviar sua senha!')
				           console.log('error');
				        }
					}
				 );

	});
		
	/* function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

$("input[type='submit']").click(function() {

		var email = $("#login").val();

		function validate(email){
			  if (validateEmail(email)) {
				    return true;
			  } else {
				  return false;
			  }
			}

		if(email){
			
			if( validate(email)){

			$.getJSON('fragment_is_email_valid.php?login='+email,function(data){

				if( (data.toString()==="false") )
				{
					$.ajax(
		 					{
		 					  type: "POST",
		 					  url: 'fragmentFormPassword.php',
		 					  data: { email:email},
		 					 success: function (data) {
		 							$("#enviarLogin").hide();
									$("#form").html(data);
								        },
							        error: function() {
							           console.log('error');
							        }
		 					}
		 				   );
				}
				else if(data.toString()==="null")
				{
					$.ajax(
		 					{
		 					  type: "GET",
		 					  url: 'fragmentFormCadastro.php',
		 					  data: { email:email},
		 					 success: function (data) {
		 							$("#enviarLogin").hide();
									$("#form").html(data);
								        },
							        error: function() {
							           console.log('error');
							        }
		 					}
		 				   );
					   
				}
				else if(data.toString()==="true")
				{
					$.ajax(
		 					{
		 					  type: "GET",
		 					  url: 'fragmentFormLogin.php',
		 					  data: { email:email},
		 					 success: function (data) {
		 							$("#enviarLogin").hide();
									$("#form").html(data);
								        },
							        error: function() {
							           console.log('error');
							        }
		 					}
		 				   );
				}

	 			});

			}
			else
			{
				alert("Atenção: e-mail "+email+ " não é valido");
			} 
		}
		else if(!email)	{
			alert('Atenção: O campo E-mail não pode ser vazio');
		}
	}); */
	
});
</script>