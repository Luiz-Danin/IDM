<?php
class ServiceDBTransacao
{
	private $db;
	private $entity;
	
	public function __construct(\PDO $db, EntidadeInterfaceTransacao $entity)
	{
		$this->db = $db;
		$this->entity = $entity;
	}
	
	public function inserirTransacao()
	{
		try 
		{
			
			$query = "INSERT INTO {$this->entity->getTable()}(id_transacao, id_aluno, data_transacao, descricao, situacao,	tipopagamento, valor, data_vencimento, id_instituicao_unidade) 
																VALUES (:id_transacao, :id_aluno, :data_transacao, :descricao, :situacao, :tipopagamento, :valor, :data_vencimento, :id_instituicao_unidade)";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':id_transacao', $this->entity->getIdTransacao() );
			$stmt->bindValue(':id_aluno', $this->entity->getIdAluno() );
			$stmt->bindValue(':data_transacao', date('Y-m-d H:i:s') );
			$stmt->bindValue(':descricao', $this->entity->getDescricao() );
			$stmt->bindValue(':situacao', $this->entity->getSituacao() );
			$stmt->bindValue(':tipopagamento', $this->entity->getTipoPagamento() );
			$stmt->bindValue(':valor', $this->entity->getValor() );
			$stmt->bindValue(':data_vencimento', date('Y-m-d H:i:s') );
			$stmt->bindValue(':id_instituicao_unidade', $this->entity->getIdInstituicaoUnidade() );
			
			if ( $stmt->execute() )
			{
				$valor = $this->entity->getValor();
				$isItemTransacao = $this->inserirItemTransacao($this->entity->getIdTransacao(), $valor);
				
				if ($isItemTransacao)
					return true;
			}
		
		}
		catch(Exception $e)
		{
			echo 'Erro ao inserir a Transação: '.$e->getMessage();
		}
	}
	
	public function inserirLogTransacao($id_transacao, $id_aluno, $token, $intermediadorPagamento)
	{
		try
		{
			$query = 'INSERT INTO log_transacao (transacao, id_aluno, data, situacao, pago, cod_interme, responsavel) 
					 VALUES (:transacao, :id_aluno, :data, :situacao, :pago, :cod_interme, :responsavel)';
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':transacao', $id_transacao);
			$stmt->bindValue(':id_aluno', $id_aluno);
			$stmt->bindValue(':data', date('Y-m-d H:i:s') );
			$stmt->bindValue(':situacao', 0 );
			$stmt->bindValue(':pago', 0 );
			$stmt->bindValue(':cod_interme', $token);
                        
                        if($intermediadorPagamento=='traycheckout')
                        {
                            $stmt->bindValue(':responsavel', 'traycheckout');
                            $stmt->bindValue(':situacao', 0 );
                        }
                        else if($intermediadorPagamento=='cielo')
                        {
                            $stmt->bindValue(':responsavel', 'cielo');
                            $stmt->bindValue(':situacao', 3 );
                        }
                        else
                        {
                            $stmt->bindValue(':responsavel', 'Intermediador Indefinido');
                        }
			
			if ( $stmt->execute() )
				return true;
			else 
				return false;
			
			
		}
		catch(Exception $e)
		{
			
		}
	}
        
        public function inserirLogTransacaoCredito($id_transacao, $id_aluno, $token, $intermediadorPagamento, $situacao)
        {
            $query = 'INSERT INTO log_transacao (transacao, id_aluno, data, situacao, pago, cod_interme, responsavel) 
									    VALUES (:transacao, :id_aluno, :data, :situacao, :pago, :cod_interme, :responsavel)';
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':transacao', $id_transacao);
			$stmt->bindValue(':id_aluno', $id_aluno);
			$stmt->bindValue(':data', date('Y-m-d H:i:s') );
			
			$stmt->bindValue(':pago', 0 );
			$stmt->bindValue(':cod_interme', $token);
                        
                        
                        if($intermediadorPagamento=='cielo')
                        {
                            $stmt->bindValue(':responsavel', 'cielo');
                            $stmt->bindValue(':situacao', $situacao );
                        }
                        else
                        {
                            $stmt->bindValue(':responsavel', 'Intermediador Indefinido');
                        }
			
			if ( $stmt->execute() )
				return true;
			else 
				return false;
			
        }
        
        public function updateTransacao()
        {
            try
            {
                $query = "UPDATE {$this->entity->getTable()} SET situacao=:situacao, tipopagamento=:tipopagamento Where id_transacao=:id_transacao";

                $stmt = $this->db->prepare($query);
                $stmt->bindValue(':id_transacao',$this->entity->getIdTransacao());
                $stmt->bindValue(':situacao', $this->entity->getSituacao());
                $stmt->bindValue(':tipopagamento', $this->entity->getTipoPagamento());

                if($stmt->execute())
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception $ex)
            {
                echo 'Erro ao atualizar a Transação para pagamento com cartão: '.$e->getMessage();
            }
        }
	
	public function findTransacaoByIdTransacao()
	{
		try
		{
			$query = "SELECT * FROM {$this->entity->getTable()} WHERE id_transacao=:id_transacao";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id_transacao", $this->entity->getIdTransacao() );
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao buscar dados  da Transação pelo id: '.$e->getMessage();
		}
	}
	
	public function isVerifyTransacao()
	{
		try 
		{
			$query = "SELECT id_transacao FROM {$this->entity->getTable()} WHERE id_transacao=:id_transacao";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id_transacao", $this->entity->getIdTransacao() );
			
			if ( $stmt->execute() )
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			else 
				return false;
			
		}
		catch(Exception $e)
		{
			echo 'Exceção ao verificar a existencia de uma transação: '.$e->getMessage();
		}
	}
	
	private function inserirItemTransacao($idTransacao, $valor)
	{
		try 
		{
			$query = "INSERT INTO item_transacao (transacao,id_produto,valor,data_registro) VALUES  (:transacao, :id_produto, :valor, :data_registro)";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':transacao', $idTransacao);
			$stmt->bindValue(':id_produto', 8);
			$stmt->bindValue(':valor', $valor);
			$stmt->bindValue(':data_registro', date('Y-m-d H:i:s') );
			
			return $stmt->execute();
		}
		catch(Exception $e)
		{
			echo 'Exceção ao inserir o Item da Transação: '.$e->getMessage();
		}
	}
}