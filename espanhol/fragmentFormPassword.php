<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
echo $html='<div class="col-md-5" id="tela">
					<div class="signup-header wow fadeInUp">
						<h3 class="form-title text-center">Cadastrar Senha</h3>
						<div class="alert alert-warning">Por favor, crie uma Senha e confirme a Senha para realizar seu Cadastro.</div>
						<form action="" class="form-header"  method="POST">
						<input type="hidden" name="flagUpdatePassword" value="2">
						<input type="hidden" name="id_curso" value="636">
							<div class="form-group">
								<input value="'.$_POST['email'].'" class="form-control input-lg" name="email" id="login" type="email" placeholder="Seu E-mail" required>
							</div>
							<div class="form-group">
								<input  class="form-control input-lg" name="senha" id="password" type="password" placeholder="Crie uma Senha" pattern=".{6,}" title="Mínimo 6 dígitos" required>
							</div>
							<div class="form-group">
								<input class="form-control input-lg" name="senhaNova" id="confirm_password" type="password" placeholder="Repita a Senha" pattern=".{6,}" title="Mínimo 6 dígitos" required>
							</div>
							<div class="checkbox">
							<label>
							<input name="check" type="checkbox"><p>Lembrar-me</p></label>
							</div>
							<div class="form-group last">
								<input name="enviar" type="submit" class="btn-secondary btn-block btn-lg" value="Entrar">
								<div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
							</div>
							<p></p>
						</form>
					</div>
				</div>';
?>
<script>
$( document ).ready(function() {
	var password = document.getElementById("password")
	, confirm_password = document.getElementById("confirm_password");
	
	function validatePassword(){
	if(password.value != confirm_password.value) {
	  confirm_password.setCustomValidity("Atenção: senhas não coincidem!");
	} else {
	  confirm_password.setCustomValidity('');
	}
	}
	
	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;
});
</script>
<script>
$( document ).ready(function() {

	/* function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

$("input[type='submit']").click(function() {

		var email = $("#login").val();
		var senha = $("#password").val();
		var confirmSenha = $("#confirm_password").val();

		function validate(email){
			  if (validateEmail(email)) {
				    return true;
			  } else {
				  return false;
			  }
			}

		if(email){
			
			if( validate(email) ){

				if( (senha.length==0) && (confirmSenha.length==0) )
				{

			$.getJSON('fragment_is_email_valid.php?login='+email,function(data){

				if( (data.toString()==="false") )
				{
					$.ajax(
		 					{
		 					  type: "POST",
		 					  url: 'fragmentFormPassword.php',
		 					  data: { email:email},
		 					 success: function (data) {
		 							$("#enviarLogin").hide();
									$("#form").html(data);
								        },
							        error: function() {
							           console.log('error');
							        }
		 					}
		 				   );
				}
				else if(data.toString()==="null")
				{
					$.ajax(
		 					{
		 					  type: "GET",
		 					  url: 'fragmentFormCadastro.php',
		 					  data: { email:email},
		 					 success: function (data) {
		 							$("#enviarLogin").hide();
									$("#form").html(data);
								        },
							        error: function() {
							           console.log('error');
							        }
		 					}
		 				   );
					   
				}
				else if(data.toString()==="true")
				{
					$.ajax(
		 					{
		 					  type: "GET",
		 					  url: 'fragmentFormLogin.php',
		 					  data: { email:email},
		 					 success: function (data) {
		 							$("#enviarLogin").hide();
									$("#form").html(data);
								        },
							        error: function() {
							           console.log('error');
							        }
		 					}
		 				   );
				}

	 			});
 			
			  }

			}
			else
			{
				alert("Atenção: e-mail "+email+ " não é valido");
			} 
		}
		else if(!email)	{
			alert('Atenção: O campo E-mail não pode ser vazio');
		}
	}); */
	
});
</script>