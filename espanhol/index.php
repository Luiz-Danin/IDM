<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
session_start();
require_once '../ingles/config.php';
require_once 'AlunoController.php';
require_once 'Request.php';

$alunoController = new AlunoController();
$alunoController->indexIDMCursos(new Request(), $conexao);

if (isset($_SESSION['id_aluno']))
{
	
	$dados = $alunoController->estudarAlunoAVA($conexao, $_SESSION['id_aluno']);
	
	$IDIOMAS = array(
			'nome' => 'IDM CURSOS',
			'chave' => 'WVZkU2RGa3pWbmxqTWpsNg',
			'token' => 'e5ed5459954a9a725034d4936d99b76a'
	);
	
	
	
	$send = base64_encode(json_encode(array('instituicao' => $IDIOMAS,'dados'=> $dados)));	
	$estudar = '<a id="idEstudar" href="#"	class="btn-primary">Estudar </a>';
	
	$situacao5 = ' <form id="formEstudar" target="_blank" action="https://www.onlist.com.br/ava/aluno/idiomas/estudar/" method="post" >'
			. '<input type="hidden" name="request" value="'.$send.'">'.'<input id="idEstudar" type="submit" value="Estudar" class="btn-primary"></form>';
}
?>
<!DOCTYPE html>
<html>
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102414743-1', 'auto');
  ga('send', 'pageview');

</script>


<!-- /.website title -->
<title>IDM Cursos - Cursos de Espanhol Básico</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- CSS Files -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/font-awesome.min.min.css" rel="stylesheet">
<link href="fonts/icon-7-stroke/css/pe-icon-7-stroke.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.min.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">

<!-- Colors -->
<link href="css/css-index.min.css" rel="stylesheet" media="screen">
<link href="css/css-app-red.min.css" rel="stylesheet" media="screen">

<!-- Google Fonts -->
<link href="css/fonts.googleapis.lato.min.css" rel="stylesheet">
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1024701292;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "F6CLCO2nj3MQ7ObO6AM";
var google_remarketing_only = false;
/* ]]> */
</script>


<!-- /.preloader -->
<div id="preloader"></div>
<div id="top"></div>
<div class="navbar">
    <div class="container">
        <div class="login-header">
<?php 
if ( isset( $_SESSION['nome']) )
{
	$nomeUsuario = explode(' ', $_SESSION['nome']);
	$nomeAluno = "<strong>".$nomeUsuario[0]."</strong>".', você está no IDM Curso'.'<a href="logout.php">, sair </a>';
}
else 
	$nomeAluno = "Olá,<strong> Você está no IDM Curso</strong>";
			
echo $nomeAluno;
?>
        </div>
    </div>
</div>
<div class="login-header"></div>
<!-- /.parallax full screen background image -->
<div class="fullscreen landing parallax" style="background-image:url('images/bg.jpg');" data-img-width="2000" data-img-height="1333" data-diff="100">
	
	<div class="overlay">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-7">
				
					<!-- /.logo -->
					<div class="logo wow fadeInDown"> <a href="../"><img src="images/logo.png" alt="logo"></a></div>

					<!-- /.main title -->
						<h1 class="wow fadeInLeft">
                                                    <strong>Espanhol para o dia a dia</strong>
						</h2>

					<!-- /.header paragraph -->
					<div class="landing-text wow fadeInUp">
						<p>Acesso 24 horas por dia ao conteúdo com temas voltados para o cotidiano. <br>
						Você acessa pelo computador, tablet ou celular. Apenas R$14,90 por mês!</p>
					</div>				  

					<!-- /.header button -->
					<div id="btn-logado" class="head-btn wow fadeInLeft">
						<!-- <a href="#" class="btn-primary">Matricular</a>
						<a href="#" class="btn-primary">Estudar	</a> -->
						<a id="idMatricular" href="#" class="btn-primary" value="<?php echo ( isset($_SESSION['id_aluno_hash']) ) ? $_SESSION['id_aluno_hash'] : '';?>" >Gerar Fatura</a>
						<?php echo $situacao5;?>
					</div>
				</div> 
				
				<!-- /.signup form -->
				<div class="col-md-5">
						<div id="enviarLogin">
							<div class="signup-header wow fadeInUp">
								<h3 class="form-title text-center">Login/Matricula</h3>
								<div class="form-header" id="login-form">
									<input type="hidden" name="u" value="503bdae81fde8612ff4944435">
									<input type="hidden" name="id" value="bfdba52708">
									<div class="form-group">
										<input class="form-control input-lg" name="email" id="email"
											type="email" placeholder="Seu E-mail" required>
									</div>

									<div class="form-group last">
									<span class="erro" id="divMayus" style="visibility:hidden; margin-bottom: -23px;">
					  				Capslock esta ativo
			        		</span>
										<input id="loginCadastro" type="submit" class="btn-secondary btn-block btn-lg"
											value="Continuar">
											<p></p>
										<!-- <div class="fb-login-button" data-max-rows="1"
											data-size="small" data-button-type="login_with"
											data-show-faces="false" data-auto-logout-link="false"
											data-use-continue-as="false"></div> -->
									</div>
									<!-- <p class="privacy text-center">Não compartilharemos o seu
										e-mail.</p> -->
								</div>
							</div>
						</div>

						</div>

						<div id="form"></div>
						<div id="formLogin"></div>
					</div>
				</div>
			</div>
		</div>
 
<!-- NAVIGATION -->
<div id="menu">
	<nav class="navbar-wrapper navbar-default" role="navigation">
		<div class="container">
			  <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-backyard">
				  <span class="sr-only">Toggle navigation</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand site-name" href="#top">IDM Curso de Idiomas</a>
			  </div>
	 
			  <div id="navbar-scroll" class="collapse navbar-collapse navbar-backyard navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#sobre">Sobre</a></li>
					<li><a href="#beneficios">Benefícios</a></li>
					<li><a href="#motivo">Por que fazer Espanhol?</a></li>
					<li><a href="#modulo">O que vou aprender?</a></li>
					<li><a href="#contato">Contato</a></li>
				</ul>
			  </div>
		</div>
	</nav>
</div>

<!-- /.Sobre -->
<div id="sobre">
	<div class="container">
		<div class="row">

		<!-- /.intro image -->
			<div class="col-md-6 sobre-pic wow slideInLeft">
				<img src="images/intro-image.jpg" alt="image" class="img-responsive">
			</div>	
			
			<!-- /.Conte�do Sobre -->
			<div class="col-md-6 wow slideInRight">
				<h2>Aprenda Espanhol com o curso básico da IDM Cursos!</h2>
				<p>Falar um segundo idioma é fundamental, seja para ajudá-lo em sua carreira profissional, construir relacionamentos com novas pessoas ou para entrar em cursos de pós graduação.</p>
		
			</div>
		</div>			  
	</div>
</div>

<!-- /.Benef�cios -->
<div id="beneficios">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-12 text-center beneficios-title">

			<!-- /.Benef�cios T�tulo -->
				<h2>Estude Sem Sair de Casa!</h2>
				<p>Tire dúvidas por áudio, texto e exercícios de acompanhamento. E
					o melhor: TUDO PELA INTERNET!</p>
			</div>
		</div>
		<div class="row row-feat">
			<div class="col-md-4 text-center">
			
			<!-- /.Benef�cios Imagem-->
				<div class="beneficios-img">
					<img src="images/feature-image.jpg" alt="image" class="img-responsive wow fadeInLeft">
				</div>
			</div>
		
			<div class="col-md-8">
			
				<!-- /.Benef�cio 1 -->
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-notebook pe-5x pe-va wow fadeInUp"></i>
					<div class="inner">
						<h4>Turmas Iniciadas Toda Semana</h4>
						<p>Tire suas dúvidas por áudio, texto. Dispomos de
									turmas semanais!</p>
					</div>
				</div>
			
				<!-- /.Benef�cio 2 -->
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-cash pe-5x pe-va wow fadeInUp" data-wow-delay="0.2s"></i>
					<div class="inner">
						<h4>Pagamento Facilitado</h4>
						<p>Pagamento facilitado por cartão de crédito, débito em conta ou débito bancário.</p>
					</div>
				</div>
			
				<!-- /.Benef�cio 3 -->
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-cart pe-5x pe-va wow fadeInUp" data-wow-delay="0.4s"></i>
					<div class="inner">
						<h4>Tudo Pela Internet</h4>
						<p>Acesso 24 horas por dia ao conteúdo. 
						Entre no ambiente de
						aprendizagem diretamente pelo computador, tablet ou celular.</p>
					</div>
				</div>
			
				<!-- /.Benef�cio 4 -->
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-users pe-5x pe-va wow fadeInUp" data-wow-delay="0.6s"></i>
					<div class="inner">
						<h4>Acesse Fácil e Rápido</h4>
						<p>Você informará suas informações de login e senha no cadastro
						 e poderá acessar o seu curso imediatamente.</p>
						<br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- /.Sess�o Benef�cio 2 -->
<div id="beneficios-2">
	<div class="container">
		<div class="row">
	
			<!-- /.Benef�cio Conte�do -->
			<div class="col-md-6 wow fadeInLeft">
				<h2>Professores acompanhando seu desenvolvimento!</h2>
				<p>Com foco em temas do dia a dia, os professores acompanharão seu
							desempenho através de metodologias <!-- span class="highlight"> metodologias</span> -->
							diferenciadas. Serão utilizados também os recursos de áudio,
							texto e exercícios para fixar o conteúdo de forma rápida e
							prática.
				</p>
			</div>
			  
			<!-- /.Benef�cio Imagem -->
			<div class="col-md-6 beneficios-2-pic wow fadeInRight">
				<img src="images/feature2-image.jpg" alt="macbook" class="img-responsive">
			</div>				  
		</div>			  
  
	</div>
</div>


<!-- /.Motivo -->
<div id="motivo">
	<div class="action fullscreen parallax" style="background-image:url('images/bg.jpg');" data-img-width="2000" data-img-height="1333" data-diff="100">
		<div class="overlay">
			<div class="container">
				<div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
				
					<!-- /.Motivo T�tulo -->
					<h2 class="wow fadeInRight">Por que fazer este curso?</h2>
					<p class="motivo-text wow fadeInLeft">Aprender espanhol já deixou de ser um diferencial curricular. Aprender espanhol é obrigatório para alcançar o sucesso em um mercado de trabalho cada vez mais competitivo.</p>
					
					<!-- /.Motivo Bot�o -->
						<div class="motivo-cta wow fadeInLeft">
							<a href="#contato" class="btn-secondary">Quero Saber Mais!</a>
						</div>
				</div>	
			</div>	
		</div>
	</div>
</div>

<!-- /.pricing section -->
<div id="modulo">
	<div class="container">
		<div class="text-center">
		
			<!-- /.pricing title -->
			<h2 class="wow fadeInLeft">Aproveite, o Certificado é Grátis!</h2>
			<p>*Após realizar o pagamento você já ganha automaticamente o certificado.</p>
					<p>24 Módulos, 5 aulas por semana, 40 minutos cada aula.</p>
			<div class="title-line wow fadeInRight"></div>
		</div>
		<div class="row modulo-option">
			
			<!-- /.m�dulos -->
			<div class="col-sm-3">
			  <div class="price-box wow fadeInUp" data-wow-delay="0.2s">
			   <div class="price-heading text-center">
			   </div>
			
				<!-- /.Informações sobre os m�dulos -->
			   <ul class="price-feature text-center">
				  <li><strong>1ª Módulo - </strong>¡Hola! ¿Qué tal?</li>
				  <li><strong>2ª Módulo - </strong> La familia </li>
				  <li><strong>3ª Módulo - </strong> La casa</li>
				  <li><strong>4ª Módulo - </strong> El trabajo</li>
				  <li><strong>5ª Módulo - </strong> La comida</li>
				  <li><strong>6ª Módulo - </strong> Mi rutina diaria </li>
				  <li><strong>7ª Módulo - </strong> ¿Cual es su número de telefono?</li>
				  <li><strong>8ª Módulo - </strong> El tiempo </li>				  
			   </ul>

			  </div>
			</div>	
			
			<!-- /.M�dulos -->
			<div class="col-sm-3">
			  <div class="price-box wow fadeInUp" data-wow-delay="0.4s">
			   <div class="price-heading text-center">

			   </div>
			
				<!-- /.Informa��es sobre os m�dulos-->
			   <ul class="price-feature text-center">
				 <li><strong>9ª Módulo - </strong>Ir de Compras</li>
				  <li><strong>10ª Módulo - </strong> El supermercado </li>
				  <li><strong>11ª Módulo - </strong> Un paseo por mi ciudad</li>
				  <li><strong>12ª Módulo - </strong> Vacaciones</li>
				  <li><strong>13ª Módulo - </strong> El tiempo ocio</li>
				  <li><strong>14ª Módulo - </strong> ¡Describe!  </li>
				  <li><strong>15ª Módulo - </strong> Mi receta favorita</li>
				  <li><strong>16ª Módulo - </strong> El barrio que vivo  </li>							  
			   </ul>

			  </div>
			</div>
			
			<!-- /.M�dulos -->
			<div class="col-sm-3">
			  <div class="price-box wow fadeInUp" data-wow-delay="0.6s">
			   <div class="price-heading text-center">

			   </div>
			
				<!-- /.Informa��es sobre os m�dulos -->
			   <ul class="price-feature text-center">
				  <li><strong>17ª Módulo - </strong> El centro comercial</li>
				  <li><strong>18ª Módulo - </strong> Salud y enfermedad </li>
				  <li><strong>19ª Módulo - </strong> Antes y ahora</li>
				  <li><strong>20ª Módulo - </strong> Yo no gano tanto como tú</li>
				  <li><strong>21ª Módulo - </strong> ¿Qué hiciste en el fin de semana?</li>
				  <li><strong>22ª Módulo - </strong> Moverse por La ciudad  </li>
				  <li><strong>23ª Módulo - </strong> Segunda mano </li>
				  <li><strong>24ª Módulo - </strong> Consejos </li>			  
			   </ul>
			  </div>
			</div>

		</div>
	</div>
</div>


<!-- /.Parceiros -->
<div id="parceiros"> 
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="http://www.abed.org.br/site/pt/associados/consulta_associados_abed/?busca_rapida=cidade+aprendizagem" target="_blank"><img alt="client" src="images/abed.png" class="wow fadeInUp"></a>
					<a href="https://www.alphassl.com/" target="_blank"><img alt="client" src="images/alpha.png" class="wow fadeInUp" data-wow-delay="0.2s"></a>
					<a target="_blank" href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=pt-BR#url=idmcursos.com.br"><img alt="client" src="images/google.jpg" class="wow fadeInUp" data-wow-delay="0.4s"></a>
					<a href="https://www.tray.com.br/"  target="_blank"><img alt="client" src="images/tray.png" class="wow fadeInUp" data-wow-delay="0.6s"></a>
					<a href="http://lattes.cnpq.br/" target="_blank"><img alt="client" src="images/lattes.png" class="wow fadeInUp" data-wow-delay="0.6s"></a>
					<a href="https://www.cidadeaprendizagem.com.br/newsite/index.php" target="_blank"><img alt="client" src="images/cidade-aprendizagem.png" class="wow fadeInUp" data-wow-delay="0.6s"></a>
				</div>
			</div>
		</div>	
</div>


<!-- /.Contato -->
<div id="contato">
	<div class="contato fullscreen parallax" style="background-image:url('images/bg.jpg');" data-img-width="2000" data-img-height="1334" data-diff="100">
		<div class="overlay">
			<div class="container">
				<div class="row contato-row">
				
					<!-- /.Endere�o e Contato -->
					<div class="col-sm-5 contato-left wow fadeInUp">
						<h2>Dúvidas?</h2>
							<ul class="ul-address">
							<!--<li><i class="pe-7s-phone"></i>4004-0435 Ramal:8229</br>-->
							<!--(whatsapp) 91 983490030-->
							</li>
							<li><i class="pe-7s-mail"></i><a href="mailto:info@yoursite.com">atendimento@idmcursos.com.br</a></li>
							<li><i class="pe-7s-look"></i><a href="../">www.idmcursos.com.br</a></li>
							</ul>	
								
					</div>
					
					<!-- /.Formul�rio de Contato -->
					<div class="col-sm-7 contato-right">
						<h2 class="form-title text-center">Contato</h2>
								<form method="POST" action="contactengine.php" id="contato-form" class="form-horizontal">
									<div class="form-group">
										<input type="text" name="nameContato" id="nameContato"  pattern="[A-zÀ-ž ]+"
											class="form-control wow fadeInUp" placeholder="Nome" required />
									</div>
									<div class="form-group">
										<input type="email" name="emailContato" id="emailContato"
											class="form-control wow fadeInUp" placeholder="E-mail"
											required />
									</div>
									<div class="form-group row">
										<div class="form-group col-xs-6 col-sm-4 col-md-3 col-lg-3">
											<input class="form-control input-lg phone wow fadeInUp" name="dddContato"
											id="dddContato" type="text" placeholder="DDD" maxlength="2" pattern="\d{2}" required title="Somente números, apenas 2 digitos">
										</div>
										<div id="tel-fixo" class="form-group col-xs-6 col-sm-8 col-md-9 col-lg-9">
											<input class="form-control input-lg phone wow fadeInUp" name="celularContato"
											id="celularContato" type="text" placeholder="Celular ou Telefone Fixo " maxlength="9" pattern="\d{8}|\d{9}" required title="Somente números, 8 ou 9 dígitos">
										</div>
									</div>
									<div class="form-group">
										<textarea name="messageContato" rows="20" cols="20" id="messageContato"
											class="form-control input-message wow fadeInUp"
											placeholder="Mensagem" required></textarea>
									</div>
									<div class="form-group">
										<input id="submitContato" type="submit" name="submit" value="Enviar"
											class="btn btn-success wow fadeInUp" />
									</div>
								</form>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
  
<!-- /.Rodap� -->
<footer id="footer">
	<div class="container">
		<div class="col-sm-4 col-sm-offset-4">
			<!-- /.social links -->
				<div class="social text-center">
					<ul>
						<!-- <li><a class="wow fadeInUp" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li> -->
						<li><a class="wow fadeInUp" href="https://www.facebook.com/idmcursos" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
					</ul>
				</div>	
			<div class="text-center wow fadeInUp" style="font-size: 14px;">Copyright IDM Cursos 2017</div>
			<a href="#" class="scrollToTop"><i class="pe-7s-up-arrow pe-va"></i></a>
		</div>	
	</div>	
</footer>
	
	<!-- /.Arquivos JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/jquery.sticky.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<script>
	$( document ).ready(function() {

		$("#btn-logado").hide();

	function validateEmail(email) { 
		    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    return re.test(email);
		}
	
	$("#loginCadastro").click(function() {

			var email = $("#email").val();

			function validate(email){
				  if (validateEmail(email)) {
					    return true;
				  } else {
					  return false;
				  }
				}

			if(email){
				
				if( validate(email)){

				$.getJSON('fragment_is_email_valid.php?login='+email,function(data){

					if( (data.toString()==="false") )
					{
						$.ajax(
			 					{
			 					  type: "POST",
			 					  url: 'fragmentFormPassword.php',
			 					  data: { email:email},
			 					 success: function (data) {
			 							$("#enviarLogin").hide();
										$("#form").html(data);
									        },
								        error: function() {
								           console.log('error');
								        }
			 					}
			 				   );
					}
					else if(data.toString()==="null")
					{
						$.ajax(
			 					{
			 					  type: "GET",
			 					  url: 'fragmentFormCadastro.php',
			 					  data: { email:email},
			 					 success: function (data) {
			 							$("#enviarLogin").hide();
										$("#form").html(data);
									        },
								        error: function() {
								           console.log('error');
								        }
			 					}
			 				   );
						   
					}
					else if(data.toString()==="true")
					{
						$.ajax(
			 					{
			 					  type: "GET",
			 					  url: 'fragmentFormLogin.php',
			 					  data: { email:email},
			 					 success: function (data) {
			 							$("#enviarLogin").hide();
										$("#form").html(data);
									        },
								        error: function() {
								           console.log('error');
								        }
			 					}
			 				   );
					}

		 			});

				}
				else
				{
					alert("Atenção: e-mail "+email+ " não é valido");
				} 
			}
			else if(!email)	{
				alert('Atenção: O campo E-mail não pode ser vazio');
			}
		});
	});
</script>
<script type="text/javascript">
$(document).ready(function() {

	//$('#largeModal').modal('show');
	
	$("#idMatricular").click(function() {

		var hash = $("#idMatricular").attr("value");

		$.ajax(
				{
				  type: "POST",
				  url: 'fragmentFormMatricular.php',
				  data: {hash:hash},
				  dataType: "html",
				  success: function (data) {
					$("#form").html(data);
				        },
			        error: function() {
			           console.log('error');
			        }
				});
		
	});
});
</script>
<script type="text/javascript">
$( document ).ready(function() {

		document.getElementById("email")
	    .addEventListener("keyup", function(event) {
	    event.preventDefault();
	    if (event.keyCode == 13) {
	        document.getElementById("loginCadastro").click();
	    }
	});
	
});
</script>
</body>
</html>
<?php 
	if( isset($_SESSION['id_aluno']))
	{
		$id_curso = 636;
		$isAlunoEstudar = $alunoController->checkEstudar($conexao, $_SESSION['id_aluno'], $id_curso);
		
		if ( $isAlunoEstudar==false )
		{
			echo '<script>$( document ).ready(function() { $("#idEstudar").hide(); });</script>';
		}
		else 
		{
			echo '<script>$( document ).ready(function() { $("#idEstudar").show(); });</script>';
		}
		
	}

	if ( isset($_SESSION['nome']) )
	{
		echo '<script>$( document ).ready(function() {  $(".col-md-5").hide(); $("#btn-logado").show(); });</script>';
	}
	else 
	{
		
	}
?>
