<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
require_once '../ingles/config.php';

function buildFormAdd($email)
{
	echo $html = '<div class="col-md-5" id="tela">
							<div class="signup-header wow fadeInUp">
								<h3 class="form-title text-center">Cadastro</h3>
								<form action="" class="form-header" id="" method="POST">
									<input type="hidden" name="flagCadastro" value="1">
									<div class="form-group">
										<input class="form-control input-lg" name="nome" id="name"
											type="text" placeholder="Nome e Sobrenome (com no mínimo 5 letras)" pattern="[A-zÀ-ž]+[ ][A-zÀ-ž]+" required title="Digite Nome e Sobrenome, mínimo 5 letras">
									</div>
									<div class="form-group">
										<input value="'.$email.'" class="form-control input-lg" name="email" id="login"
											type="email" placeholder="Seu E-mail" required>
									</div>
									<div class="form-group row">
										<div class="form-group col-xs-6 col-sm-4 col-md-3 col-lg-3">
											<input class="form-control input-lg phone" name="ddd"
											id="ddd" type="text" placeholder="DDD" maxlength="2" pattern="\d{2}" required title="Somente números, apenas 2 digitos">
										</div>
										<div class="form-group col-xs-6 col-sm-8 col-md-9 col-lg-9">
											<input class="form-control input-lg phone" name="telefone"
											id="telefone" type="text" placeholder="Telefone" maxlength="9" pattern="\d{8}|\d{9}" required title="Somente números, 8 ou 9 dígitos">
										</div>
									</div>
									<div class="form-group">
										<input class="form-control input-lg" name="cep" id="cep"
											type="text" placeholder="CEP"  maxlength="8" pattern="\d{8}" required title="Somente números, apenas 8 digitos">
									</div>
									<div class="form-group">
										<input class="form-control input-lg" name="senha"
											id="password" type="password" placeholder="Senha" pattern=".{6,}" required title="Mínimo 6 dígitos">
									</div>
									<div class="checkbox">
										<label> <input type="checkbox">
										<p>Lembrar-me</p></label>
									</div>
									<div class="form-group last">
										<input name="enviar" id="submitForm" type="submit" class="btn-secondary btn-block btn-lg" value="Enviar">
										<div class="fb-login-button" data-max-rows="1"
											data-size="small" data-button-type="login_with"
											data-show-faces="false" data-auto-logout-link="false"
											data-use-continue-as="false"></div>
									</form>
									<p class="privacy text-center">Não compartilharemos o seu
										e-mail.</p>
								</div>
							</div>';
}

function buildFormLogin($email)
{
	$senha = ( isset($_COOKIE["senha"]) ) ? $_COOKIE["senha"] : '';
	
	echo $html='<div class="col-md-5" id="tela">
			
					<div class="signup-header wow fadeInUp">
						<h3 class="form-title text-center">Login</h3>
						<form action="" class="form-header" id="formLogin"  method="POST">
						 <input type="hidden" name="flagLogin" value="0">
							<div class="form-group">
								<input value="'.$email.'" class="form-control input-lg" name="email" id="login" type="email" placeholder="Seu E-mail" required>
							</div>
							<div class="form-group">
								<input value="'.$senha.'" class="form-control input-lg" name="senha" id="password" type="password" placeholder="Senha" pattern=".{6,}" title="Mínimo 6 dígitos" required>
							</div>
							<div class="checkbox">
							<label>
							<input name="check" type="checkbox"><p>Lembrar-me</p></label>
							</div>
							<div class="form-group last">
								<input id="submitLogin" name="enviar" type="submit" class="btn-secondary btn-block btn-lg" value="Enviar">
								<div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
							</div>
							<p class="privacy text-center">Não compartilharemos o seu e-mail.</p>
						</form>
					</div>
										
				</div>';
}

function getForm($email, $conexao)
{
	$query = "SELECT nome, email FROM aluno WHERE email= :email";
	
	$stmt = $conexao->prepare($query);
	$stmt->bindValue(":email", $email, PDO::PARAM_STR);
	$stmt->execute();
	
	$emailAluno= $stmt->fetch(PDO::FETCH_ASSOC);
	
	if (!$emailAluno)
		buildFormAdd($email);
	else
		buildFormLogin($email);
}

getForm($_POST['email'],$conexao);
?>
<script type="text/javascript">
$( document ).ready(function() {
 	$("#submitLogin").click(function() {
	
 		var login = $("#login").val();
 		var password =  $("#password").val();

 		$.getJSON('fragment_is_email_valid.php?login='+login,function(data){

			if( (data.toString()==="false") )
			{
				$.ajax(
	 					{
	 					  type: "POST",
	 					  url: 'fragmentFormPassword.php',
	 					  data: { login:login},
	 					 success: function (data) {
								$("#form").html(data);
							        },
						        error: function() {
						           console.log('error');
						        }
	 					}
	 				   );
			}
			else if(data.toString()==="null")
			{
				$.ajax(
	 					{
	 					  type: "GET",
	 					  url: 'fragmentFormCadastro.php',
	 					  data: { login:login},
	 					 success: function (data) {
								$("#form").html(data);
							        },
						        error: function() {
						           console.log('error');
						        }
	 					}
	 				   );
				   
			}
			else if(data.toString()==="true")
			{
				$.ajax(
	 					{
	 					  type: "GET",
	 					  url: 'fragmentFormLogin.php',
	 					  data: { login:login},
	 					 success: function (data) {
								$("#form").html(data);
							        },
						        error: function() {
						           console.log('error');
						        }
	 					}
	 				   );
			}

 			});
 	});
 });
</script>