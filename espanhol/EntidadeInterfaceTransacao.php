<?php
interface EntidadeInterfaceTransacao
{
	public function getTable();
	public function setTable($table);
	
	public function getIdTransacao();
	public function setIdTransacao($id_transacao);
	
	public function getIdAluno();
	public function setIdAluno($id_aluno);
	
	public function getDataTransacao();
	public function setDataTransacao($data_transacao);
	
	public function getDescricao();
	public function setDescricao($descricao);
	
	public function getSituacao();
	public function setSituacao($situacao);
	
	public function getTipoPagamento();
	public function setTipoPagamento($tipo_pagamento);
	
	public function getValor();
	public function setValor($valor);
	
	public function getDataVencimento();
	public function setDataVencimento($data_vencimento);
	
	public function getIdInstituicaoUnidade();
	public function setIdInstituicaoUnidade($id_instituicao_unidade);
}