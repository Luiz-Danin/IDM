<?php 

class Phpmailer_library {
    
    private $objMail;

    
    public function __construct ()
    {
        require_once 'class.phpmailer.php';
        require_once 'class.smtp.php';

        set_time_limit(0);
        
    }
    
    
    public function send($smtp_server,$para,$assunto,$corpo,$de)
    {

        $this->objMail = new PHPMailer;
        $this->objMail->CharSet = 'iso-8859-1';
        
        //setando para autorizar
        $this->objMail->SMTPAuth = true;

        //setando a porta
        $this->objMail->Port = 587;

        //ligando o SMTP debug
        $this->objMail->SMTPDebug = 0;
        
        //setando o phpmailer para usar SMTP
        $this->objMail->isSMTP();

        //setando tipo de segurança
        $this->objMail->SMTPSecure = "tls";

       //configurando o dominio
       $this->objMail->Host = $smtp_server->getServidor_smtp_dominio();
       
       //configura o nome de usuário
       $this->objMail->Username = $smtp_server->getServidor_smtp_usuario();
       
       //configurando a senha
       $this->objMail->Password = $smtp_server->getServidor_smtp_senha();
       
       //email de origem
       $this->objMail->setFrom($smtp_server->getServidor_smtp_email(),$de);
       //$this->objMail->setFrom($de);
       
       //configurando o destinatário
       $para_name = explode('@',$para);
       
       
       $this->objMail->addAddress($para,$para_name[0]);
            
       //configura o asunto do e-mail
       $this->objMail->Subject = $assunto;
       //$this->objMail->Body = $corpo;
       //configura o corpo do e-mail     
       $this->objMail->msgHTML($corpo);
        
       $status = $this->objMail->send();
       $this->objMail->SmtpClose();

       if (!$status) {
          return false;
       } else {
          return true;
       }
          
    }
    
}
