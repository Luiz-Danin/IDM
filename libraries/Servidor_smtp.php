<?php


class Servidor_smtp
{
    private $servidor_smtp_id;
    private $servidor_smtp_dominio;
    private $servidor_smtp_ip;
    private $servidor_smtp_email;
    private $servidor_smtp_usuario;
    private $servidor_smtp_senha;
    private $servidor_smtp_descricao;
    
    
 
    public function setData($id,$dominio,$ip,$email,$usuario,$senha,$descricao)
    {
        
        $this->servidor_smtp_id = $id;
        $this->servidor_smtp_dominio = $dominio;
        $this->servidor_smtp_ip = $ip;
        $this->servidor_smtp_email = $email;
        $this->servidor_smtp_usuario = $usuario;
        $this->servidor_smtp_senha = $senha;
        $this->servidor_smtp_descricao = $descricao;
    }
    
    public function getServidor_smtp_id() { return $this->servidor_smtp_id; }
    public function getServidor_smtp_dominio() { return $this->servidor_smtp_dominio; }
    public function getServidor_smtp_ip() { return $this->servidor_smtp_ip; }
    public function getServidor_smtp_email() { return $this->servidor_smtp_email; }
    public function getServidor_smtp_usuario() { return $this->servidor_smtp_usuario; }
    public function getServidor_smtp_senha() { return $this->servidor_smtp_senha; }
    public function getServidor_smtp_descricao() { return $this->servidor_smtp_descricao; }
    
    public function setServidor_smtp_dominio($dominio) { $this->servidor_smtp_dominio = $dominio; }
    public function setServidor_smtp_ip($ip) { $this->servidor_smtp_ip = $ip; }
    public function setServidor_smtp_email($email) { $this->servidor_smtp_email = $email; }
    public function setServidor_smtp_usuario($usuario) { $this->servidor_smtp_usuario = $usuario; }
    public function setServidor_smtp_senha($senha) { $this->servidor_smtp_senha = $senha; }
    public function setServidor_smtp_descricao($descricao) { $this->servidor_smtp_descricao = $descricao; }
    
}