<!DOCTYPE html>
<html>
<head>
<title> IDM Cursos - Cursos de Inglês Básico </title>
<meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link rel=stylesheet href=ingles/css/bootstrap.min.css rel=stylesheet media=screen>
<link rel=stylesheet href=ingles/css/font-awesome.min.min.css rel=stylesheet>
<link rel=stylesheet href=ingles/fonts/icon-7-stroke/css/pe-icon-7-stroke.min.css rel=stylesheet>
<link rel=stylesheet href=ingles/css/animate.min.css rel=stylesheet media=screen>
<link rel=stylesheet href=ingles/css/owl.theme.min.css rel=stylesheet>
<link rel=stylesheet href=ingles/css/owl.carousel.min.css rel=stylesheet>
<link href=ingles/css/css-index.min.css rel=stylesheet media=screen>

<?php header('Content-type: text/html; charset=utf-8'); ?> 

<style type=text/css>#paragraphIDM{text-align:justify}</style>
<div id=preloader></div>
<div id=top></div>
<div class="fullscreen landing parallax" style="background-image:url('ingles/images/bgindex.jpg')" data-img-width=2000 data-img-height=1333 data-diff=100>
<div class=overlay>
<div class=container-fluid>
<center><div class="logo wow fadeInDown"> <a href><img src=ingles/images/logo.png alt=logo></a></div></center>
<div class=col-md-12>
<center>
<h2 class="wow fadeInLeft animated" style=visibility:visible;animation-name:fadeInLeft>
Escolha o idioma
</h2>
</center>
<br>
<br>
</div>
<div class=row>
<div class=col-md-5>
<div class="landing-text wow fadeInUp animated" style=visibility:visible;animation-name:fadeInUp>
<p id=paragraphIDM>O inglês é a língua mais utilizada no mundo, além de ser uma ótima ferramenta para impulsionar sua carreira.
Caso queira conhecer o conteúdo programático do curso e iniciar seus estudos. Escolha aqui!</p>
<center><a href=./ingles><img src=ingles/images/btn_ingles.png alt></a></center>
</div>
</div>
<div class=col-md-2></div>
<div class=col-md-5>
<div class="landing-text wow fadeInUp animated" style=visibility:visible;animation-name:fadeInUp>
<p id=paragraphIDM>O espanhol é o segundo idioma mais falado no mundo, além de ser a língua dos nossos vizinhos. Comunicar-se com cerca de 550 milhões de pessoas pelas Américas, Europa, África, Ásia e Oceania.</p>
<center><a href=./espanhol><img src=ingles/images/btn_espanhol.png alt></a></center>
<br><br><br><br>
</div>
</div>
</div>
<footer>
<center>
<small>
<p>
Copyright © IDM CURSOS 2017/<?php echo date('Y')?><br>
E-mail: atendimento@idmcursos.com.br 
</p>
</small>
</center>
</footer>
</div>
<script>(function(d,e,j,h,f,c,b){d.GoogleAnalyticsObject=f;d[f]=d[f]||function(){(d[f].q=d[f].q||[]).push(arguments)},d[f].l=1*new Date();c=e.createElement(j),b=e.getElementsByTagName(j)[0];c.async=1;c.src=h;b.parentNode.insertBefore(c,b)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create","UA-102414743-1","auto");ga("send","pageview");</script>
<script type=text/javascript src=ingles/js/jquery.min.js></script>
<script type=text/javascript src=ingles/js/bootstrap.min.js></script>
<script type=text/javascript src=ingles/js/custom.min.js></script>
<script type=text/javascript src=ingles/js/jquery.sticky.min.js></script>
<script type=text/javascript src=ingles/js/wow.min.min.js></script>
<script type=text/javascript src=ingles/js/owl.carousel.min.min.js></script>
<script>new WOW().init();</script>
</body>
</html>