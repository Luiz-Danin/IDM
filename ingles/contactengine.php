<?php
require_once 'Request.php';
require_once 'mail/TemplateEmail.php';
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Belem');

$request = new Request();

	function get_client_ip_server() {
		$ipaddress = '';
		if (getenv ( 'HTTP_CLIENT_IP' ))
			$ipaddress = getenv ( 'HTTP_CLIENT_IP' );
		else if (getenv ( 'HTTP_X_FORWARDED_FOR' ))
			$ipaddress = getenv ( 'HTTP_X_FORWARDED_FOR' );
		else if (getenv ( 'HTTP_X_FORWARDED' ))
			$ipaddress = getenv ( 'HTTP_X_FORWARDED' );
		else if (getenv ( 'HTTP_FORWARDED_FOR' ))
			$ipaddress = getenv ( 'HTTP_FORWARDED_FOR' );
		else if (getenv ( 'HTTP_FORWARDED' ))
			$ipaddress = getenv ( 'HTTP_FORWARDED' );
		else if (getenv ( 'REMOTE_ADDR' ))
			$ipaddress = getenv ( 'REMOTE_ADDR' );
		else
			$ipaddress = 'UNKNOWN';
		
		return $ipaddress;
	}
	
if ($request->getKey('submit')=='Enviar')
{
	$ip = get_client_ip_server();
	
	$Subject = "Mensagem de Contato do IDM Cursos";
	$Name = Trim( stripslashes($request->getKey('nameContato') ) ); 
	$Email = Trim( 	($request->getKey('emailContato') ) );
	$EmailTo= Trim( stripslashes( 'atendimento@idmcursos.com.br' ) ); 
	$ddd = Trim( stripslashes( $request->getKey('dddContato') ) );
	$Celular = Trim( stripslashes( $request->getKey('celularContato') ) );
	$Message = Trim( stripslashes( $request->getKey('messageContato') ) ); 
	
	$dataContato = strftime('%A, %d de %B de %Y', strtotime('today') ).'. Hora: '.date('H:i:s');
	$dataContato = utf8_encode($dataContato );
	
	$Body = TemplateEmail::contato($Name, $Email, $ddd, $Celular, $Message, $dataContato, $ip);
	
	$data = array('name'=>'IDM Cursos','email'=>$EmailTo,'subject'=>$Subject,'body'=>$Body);
	 
	$ch = curl_init('http://curso.me/mail/LibMail.php');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	
	$response = curl_exec($ch);
	curl_close($ch);
	echo '<script>alert("Mensagem enviada com Sucesso!")</script>';
	echo '<meta http-equiv="refresh" content="0; url=./" />';
}
?>