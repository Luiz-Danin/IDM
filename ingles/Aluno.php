<?php
require_once 'EntidadeInterface.php';

class Aluno implements EntidadeInterface
{
	private $table = "aluno";
	private $id;
	private $nome;
	private $email;
	private $ddd;
	private $telefone;
	private $cep;
	private $bairro;
	private $estado;
	private $cidade;
	private $endereco;
	private $numero;
	private $senha;
	
	/**
	 * @param string $table
	 */
	public function setTable($table)
	{
		$this->table = $table;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTable()
	{
		return $this->table;
	}
	
	/**
	 * @param mixed $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}
	
	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @param mixed $nome
	 */
	public function setNome($nome)
	{
		$this->nome = $nome;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getNome()
	{
		return $this->nome;
	}
	
	/**
	 * @param mixed $ddd
	 */
	public function setDDD($ddd)
	{
		$this->ddd = $ddd;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getDDD()
	{
		return $this->ddd;
	}
	
	/**
	 * @param mixed $telefone
	 */
	public function setTelefone( $telefone)
	{
		$this->telefone = $telefone;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getTelefone()
	{
		return $this->telefone;
	}
	
	/**
	 * @param mixed $cep
	 */
	public function setCep($cep)
	{
		$this->cep = $cep;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getCep()
	{
		return $this->cep;
	}
	
	
	/**
	 * @param mixed $bairro
	 */
	public function setBairro($bairro)
	{
		$this->bairro= $bairro;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getBairro()
	{
		return $this->bairro;
	}
	
	/**
	 * @param mixed $estado
	 */
	public function setEstado($estado)
	{
		$this->estado= $estado;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getEstado()
	{
		return $this->estado;
	}
	
	/**
	 * @param mixed $estado
	 */
	public function setCidade($cidade)
	{
		$this->cidade= $cidade;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getCidade()
	{
		return $this->cidade;
	}
	
	/**
	 * @param mixed $estado
	 */
	public function setEndereco($endereco)
	{
		$this->endereco= $endereco;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getEndereco()
	{
		return $this->endereco;
	}
	
	/**
	 * @param mixed $estado
	 */
	public function setNumero($numero)
	{
		$this->numero= $numero;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getNumero()
	{
		return $this->numero;
	}
	
	/**
	 * @param mixed $senha
	 */
	public function setSenha($senha)
	{
		$this->senha = $senha;
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getSenha()
	{
		return $this->senha;
	}
	
	private function checkNome($nome)
	{
		$strNome = strlen($nome);
		$isNumber = (int)$nome;
		
		if ( isset($nome) && ( ($strNome>=5) && ($isNumber==0) ) )
			return true;
	}
	
	private function checkEmail($email)
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false)
			return true;
		else
			return false;
	}
	
	private function checkSenha($senha)
	{
		$strSenha = strlen($senha);
		if ( isset($senha) && $strSenha>=6 )
			return true;
	}
	
	private function checkDDD($ddd)
	{
		$strDDD = strlen($ddd);
		$isNumber = ctype_digit($ddd);
		
		if ( ($isNumber==true) && ($strDDD==2) )
				return true;
	}
	
	private function checkTelefone($telefone)
	{
		$strTel = strlen($telefone);
		$isNumber = ctype_digit($telefone);
		
		if ( ($isNumber==true) && ($strTel==8 || $strTel==9) )
			return true;
		else return false;
	}
	
	private function checkCep($cep)
	{
		$strCep = strlen($cep);
		$isNumber = ctype_digit($cep);
		
		if ( ($isNumber==true) && ($strCep==8) )
			return true;
	}
	
	private function checkBairro($bairro)
	{
		if ( isset($bairro) && !(is_null($bairro)) )
			return true;
	}
	
	private function checkEstado($estado)
	{
		if ( isset($estado) && !(is_null($estado)) )
			return true;
	}
	
	private function checkCidade($cidade)
	{
		if ( isset($cidade) && !(is_null($cidade)) )
			return true;
	}
	
	private function checkEndereco($endereco)
	{
		if ( isset($endereco) && !(is_null($endereco)) )
			return true;
	}
	
	public function checkAluno()
	{
		$nome = $this->getNome();
		$email = $this->getEmail();
		$telefone = $this->getTelefone();
		$ddd = $this->getDDD();
		$cep = $this->getCep();
		$bairro = $this->getBairro();
		$estado = $this->getEstado();
		$cidade = $this->getCidade();
		$endereco = $this->getEndereco();
		$senha = $this->getSenha();
		
		if ( $this->checkNome($nome) && $this->checkEmail($email) && $this->checkTelefone($telefone) && $this->checkDDD($ddd) && $this->checkCep($cep) && $this->checkBairro($bairro) && $this->checkEstado($estado) && $this->checkCidade($cidade) && $this->checkEndereco($endereco) && $this->checkSenha($senha) )
			return true;
		else return false;
	}
}