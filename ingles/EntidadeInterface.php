<?php
interface EntidadeInterface
{
	public function getId();
	public function setId($id);
	
	public function getTable();
	public function setTable($table);
	
	public function getNome();
	public function setNome($nome);
	
	public function getEmail();
	public function setEmail($email);
	
	public function getDDD();
	public function setDDD($ddd);
	
	public function getTelefone();
	public function setTelefone($telefone);
	
	public function getCep();
	public function setCep($cep);
	
	public function getBairro();
	public function setBairro($bairro);
	
	public function getEstado();
	public function setEstado($estado);
	
	public function getCidade();
	public function setCidade($cidade);
	
	public function getEndereco();
	public function setEndereco($endereco);
	
	public function getNumero();
	public function setNumero($numero);
	
	public function getSenha();
	public function setSenha($senha);
}