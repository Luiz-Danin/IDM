<?php
// include_once '../library/Import.php';
// Import::library('/lib/class.phpmailer');

include_once 'lib/class.phpmailer.php';

class LibMail extends PHPMailer
{
	
	const HOST = 'smtp.gmail.com';
	const SMTP_AUTH = true;
	const PORT = '465';
	const SMTP_SECURE = 'ssl';
	const CHATSET = 'utf8';
	const USERNAME  = 'cadastro@imazon.com.br';
	const NAME = 'IDM Cursos';
	const PASSWORD = '657@beta';
	
	function LibMail()
	{
		$this->Host = $this::HOST;
		$this->SMTPAuth = $this::SMTP_AUTH;
		$this->Port = $this::PORT;
		$this->SMTPSecure = $this::SMTP_SECURE;
		$this->CharSet = $this::CHATSET;
		$this->Username = $this::USERNAME;
		$this->From = $this::USERNAME;
		$this->FromName = $this::NAME;
		$this->Password = $this::PASSWORD;	
		$this->Mailer = 'smtp';	
		$this->SMTPDebug = 2;
		$mail->Debugoutput = 'html';
	}
	
	public function sendTo($address,$name=NULL)
	{
		$this->AddAddress($address,$name);
	}
	
	public function setBodyMail($message)
	{
		$this->Body = $message;
	}
	
	public function setSubject($subject)
	{
		$this->Subject = $subject;
	}
	
	public function setAltInform($alt)
	{
		$this->AltBody = $alt;
	}
	
	public function execute()
	{
		return $this->Send();
	}
	
	public function debugSmtpOff()
	{
		$this->SMTPDebug = 0;
	}
	
	
}