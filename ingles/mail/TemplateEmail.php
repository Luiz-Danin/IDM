<?php
class TemplateEmail
{
	public static function cadastro($nome, $email, $senha)
	{
		return '<!DOCTYPE html>
					<meta charset="UTF-8">
					<html lang="pt-br">
					<head>
					<meta charset="UTF-8">
					</head>
					<title></title>
					<style type="text/css">
					  body {
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    margin:0 !important;
					    width: 100% !important;
					    -webkit-text-size-adjust: 100% !important;
					    -ms-text-size-adjust: 100% !important;
					    -webkit-font-smoothing: antialiased !important;
					  }
					  .tableContent img {
					    border: 0 !important;
					    display: block !important;
					    outline: none !important;
					  }
					  a{
					    color:#382F2E;
					  }
					  table.social {
					    /*  padding:15px; */
					    background-color: #fff;
					
					  }
					  p.callout {
					    padding:15px;
					    background-color:#f6f6f6;
					    margin-bottom: 15px;
					  }
					  .callout a {
					    font-weight:bold;
					    color: #2BA6CB;
					  }
					  .social .soc-btn {
					    width: 100px;
					    padding: 1px 7px;
					    font-size:12px;
					    margin-bottom:5px;
					    text-decoration:none;
					    color: #FFF;font-weight:bold;
					    display:block;
					    text-align:center;
					  }
					  a.fb { background-color: #3B5998!important; }
					  a.tw { background-color: #1daced!important; }
					
					  p{
					    text-align:left;
					    color:#4f4f4f;
					    font-size:14px;
					    font-weight:normal;
					    line-height:19px;
					  }
					
					  a.link1{
					    color:#382F2E;
					  }
					  a.link2{
					    font-size:16px;
					    text-decoration:none;
					    color:#ffffff;
					  }
					
					  div,p,ul,h1{
					    margin:0;
					  }
					
					  .bgBody{
					    background: #ffffff;
					  }
					  .bgItem{
					    background: #ffffff;
					  }
					
					  @media only screen and (max-width:480px)
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					    td[class="left_pad"] 
					    {
					      padding-left:15px !important;
					      padding-right:15px !important;
					    }
					
					  }
					
					  @media only screen and (max-width:540px) 
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					  }
					
					</style>
					
					<body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
					  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center"  style="font-family:Helvetica, Arial,serif;">
					    <tbody>
					      <tr>
					        <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
					          <tbody>
					            <tr>
					              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tbody>
					                  <tr>
					                    <td valign="top" width="40">&nbsp;</td>
					                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tbody>
					                        <tr>
					                          <td height="-12" class="spechide"></td>
					
					                        </tr>
					                        <tr>
					                          <td class="movableContentContainer" valign="top">
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="35"></td>
					                                  </tr>
					                                  <tr>
					                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                      <tbody>
					                                        <tr>
					                                          <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                               <p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">IDM Cursos</span></strong> </p>
																	<p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">Mensagem de Boas Vindas</span></strong> </p>
					                                              </div>
					                                            </div></td>
					                                          </tr>
					                                        </tbody>
					                                      </table>
					                                    </td>
					                                  </tr>
					                                </tbody>
					                              </table>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					                                <tr>
					                                  <td align="left">
					                                  <br><br>
					                                    <div class="contentEditableContainer contentTextEditable">
					                                      <div class="contentEditable" align="center">
					                                        <p>
					                                          <p class="callout">
					                                           Olá <strong>'.$nome.'</strong>, Seja bem vindo(a) ao IDM Cursos. Utilize as seguintes informações para seu acesso:
					                                            <br>
					                                            <br>
					                                            <strong>E-mail: </strong>'.$email.'<br>
					                                            <strong>Senha:</strong>'.base64_decode($senha).'<br>
					                                          </p>
					                                        </p>
					                                      </div>
					                                    </div>
					                                  </td>
					                                </tr>
					
					                                <tr><td height="15"></td></tr>
					
					                                <tr>
					                                  <td align="center" >
					                                    <table>
					                                      <tr>
					                                        <td align="center" width="200px" bgcolor="#1A54BA" style="background:#005b95; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;">
					                                          <div class="contentEditableContainer contentTextEditable">
					                                            <div class="contentEditable" align="center">
					                                              <a target="_blank" href="https://www.idmcursos.com.br/ingles/" class="link2" style="color:#ffffff;"><font color="#fff">Acessar Agora</font></a>
					                                            </div>
					                                          </div>
					                                        </td>
					                                      </tr>
					                                    </table>
					                                  </td>
					                                </tr>
					                                <tr><td height="10"></td></tr>
					                              </table>
					                              <br>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="15">
					                                    </tr>
					                                    <tr>
					                                      <td  style="border-bottom:1px solid #DDDDDD;"></td>
					                                    </tr>
					                                    <tr><td height="15"></td></tr>
					                                    <tr>
					                                      <td>
					                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                        <tbody>
					                                          <tr>
					                                            <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                                <p  style="text-align:left;color:#4f4f4f;font-size:12px;font-weight:normal;line-height:20px;">
					                                                  <span style="font-weight:bold;">Copyright @ IDM Cursos '.date("Y").'</span>
					                                                   atendimento@cidadeaprendizagem.com.br
					                                                  <br>
					                                                </div>
					                                              </div></td>
					                                              <td valign="top" width="30" class="specbundle">&nbsp;</td>
					                                              <td valign="top" class="specbundle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					
					                                              </td>
					                                            </tr>
					                                          </tbody>
					                                        </table>
					                                      </td>
					                                    </tr>
					                                    <tr><td height="48"></td></tr>
					                                  </tbody>
					                                </table>
					
					                              </div>
					
					                              <!-- =============================== Rodapé ====================================== -->
					
					                            </td>
					                          </tr>
					                        </tbody>
					                      </table>
					                    </td>
					                    <td valign="top" width="40">&nbsp;</td>
					                  </tr>
					                </tbody>
					              </table>
					            </td>
					          </tr>
					        </tbody>
					      </table>
					    </td>
					  </tr>
					</tbody>
					</table>
					</body>
					</html>';
	}
	
	public static function contato($nome, $email, $ddd, $Celular, $Message, $dataContato, $ip)
	{
		setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
		date_default_timezone_set('America/Belem');
		
		return '<!DOCTYPE html>
					<meta charset="UTF-8">
					<html lang="pt-br">
					<head>
					<meta charset="UTF-8">
					</head>
					<title></title>
					<style type="text/css">
					  body {
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    margin:0 !important;
					    width: 100% !important;
					    -webkit-text-size-adjust: 100% !important;
					    -ms-text-size-adjust: 100% !important;
					    -webkit-font-smoothing: antialiased !important;
					  }
					  .tableContent img {
					    border: 0 !important;
					    display: block !important;
					    outline: none !important;
					  }
					  a{
					    color:#382F2E;
					  }
					  table.social {
					    /*  padding:15px; */
					    background-color: #fff;
					
					  }
					  p.callout {
					    padding:15px;
					    background-color:#f6f6f6;
					    margin-bottom: 15px;
					  }
					  .callout a {
					    font-weight:bold;
					    color: #2BA6CB;
					  }
					  .social .soc-btn {
					    width: 100px;
					    padding: 1px 7px;
					    font-size:12px;
					    margin-bottom:5px;
					    text-decoration:none;
					    color: #FFF;font-weight:bold;
					    display:block;
					    text-align:center;
					  }
					  a.fb { background-color: #3B5998!important; }
					  a.tw { background-color: #1daced!important; }
					
					  p{
					    text-align:left;
					    color:#4f4f4f;
					    font-size:14px;
					    font-weight:normal;
					    line-height:19px;
					  }
					
					  a.link1{
					    color:#382F2E;
					  }
					  a.link2{
					    font-size:16px;
					    text-decoration:none;
					    color:#ffffff;
					  }
					
					  div,p,ul,h1{
					    margin:0;
					  }
					
					  .bgBody{
					    background: #ffffff;
					  }
					  .bgItem{
					    background: #ffffff;
					  }
					
					  @media only screen and (max-width:480px)
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					    td[class="left_pad"] 
					    {
					      padding-left:15px !important;
					      padding-right:15px !important;
					    }
					
					  }
					
					  @media only screen and (max-width:540px) 
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					  }
					
					</style>
					
					<body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
					  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center"  style="font-family:Helvetica, Arial,serif;">
					    <tbody>
					      <tr>
					        <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
					          <tbody>
					            <tr>
					              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tbody>
					                  <tr>
					                    <td valign="top" width="40">&nbsp;</td>
					                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tbody>
					                        <tr>
					                          <td height="-12" class="spechide"></td>
					
					                        </tr>
					                        <tr>
					                          <td class="movableContentContainer" valign="top">
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="35"></td>
					                                  </tr>
					                                  <tr>
					                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                      <tbody>
					                                        <tr>
					                                          <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                               <p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">IDM Cursos</span></strong> </p>
																	<p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">Mensagem de Contato</span></strong> </p>
					                                              </div>
					                                            </div></td>
					                                          </tr>
					                                        </tbody>
					                                      </table>
					                                    </td>
					                                  </tr>
					                                </tbody>
					                              </table>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					                                <tr>
					                                  <td align="left">
					                                  <br><br>
					                                    <div class="contentEditableContainer contentTextEditable">
					                                      <div class="contentEditable" align="center">
					                                        <p>
					                                          <p class="callout">
					                                            <strong>Nome: </strong>'.$nome.'<br>
					                                            <strong>E-mail: </strong>'.$email.'<br>
																<strong>Celular/Telefone: </strong>0'.$ddd.$Celular.'<br>
																<strong>Mensagem: </strong>'.$Message.'<br>
																<strong>Data: </strong>'.$dataContato.'<br>
																<strong>IP: </strong>'.$ip.'
					                                          </p>
					                                        </p>
					                                      </div>
					                                    </div>
					                                  </td>
					                                </tr>
					                              </table>
					                              <br>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="15">
					                                    </tr>
					                                    <tr>
					                                      <td  style="border-bottom:1px solid #DDDDDD;"></td>
					                                    </tr>
					                                    <tr><td height="15"></td></tr>
					                                    <tr>
					                                      <td>
					                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                        <tbody>
					                                          <tr>
					                                            <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                                <p  style="text-align:left;color:#4f4f4f;font-size:12px;font-weight:normal;line-height:20px;">
					                                                  <span style="font-weight:bold;">Copyright @ IDM Cursos '.date("Y").'</span>
					                                                   atendimento@cidadeaprendizagem.com.br
					                                                  <br>
					                                                </div>
					                                              </div></td>
					                                              <td valign="top" width="30" class="specbundle">&nbsp;</td>
					                                              <td valign="top" class="specbundle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					
					                                              </td>
					                                            </tr>
					                                          </tbody>
					                                        </table>
					                                      </td>
					                                    </tr>
					                                    <tr><td height="48"></td></tr>
					                                  </tbody>
					                                </table>
					
					                              </div>
					
					                              <!-- =============================== Rodapé ====================================== -->
					
					                            </td>
					                          </tr>
					                        </tbody>
					                      </table>
					                    </td>
					                    <td valign="top" width="40">&nbsp;</td>
					                  </tr>
					                </tbody>
					              </table>
					            </td>
					          </tr>
					        </tbody>
					      </table>
					    </td>
					  </tr>
					</tbody>
					</table>
					</body>
					</html>';
	}
	
	public static function recuperaSenha($nome, $email, $senha)
	{
		return '
				<!DOCTYPE html>
					<meta charset="UTF-8">
					<html lang="pt-br">
					<head>
					<meta charset="UTF-8">
					</head>
					<title></title>
					<style type="text/css">
					  body {
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    margin:0 !important;
					    width: 100% !important;
					    -webkit-text-size-adjust: 100% !important;
					    -ms-text-size-adjust: 100% !important;
					    -webkit-font-smoothing: antialiased !important;
					  }
					  .tableContent img {
					    border: 0 !important;
					    display: block !important;
					    outline: none !important;
					  }
					  a{
					    color:#382F2E;
					  }
					  table.social {
					    /*  padding:15px; */
					    background-color: #fff;
					
					  }
					  p.callout {
					    padding:15px;
					    background-color:#f6f6f6;
					    margin-bottom: 15px;
					  }
					  .callout a {
					    font-weight:bold;
					    color: #2BA6CB;
					  }
					  .social .soc-btn {
					    width: 100px;
					    padding: 1px 7px;
					    font-size:12px;
					    margin-bottom:5px;
					    text-decoration:none;
					    color: #FFF;font-weight:bold;
					    display:block;
					    text-align:center;
					  }
					  a.fb { background-color: #3B5998!important; }
					  a.tw { background-color: #1daced!important; }
					
					  p{
					    text-align:left;
					    color:#4f4f4f;
					    font-size:14px;
					    font-weight:normal;
					    line-height:19px;
					  }
					
					  a.link1{
					    color:#382F2E;
					  }
					  a.link2{
					    font-size:16px;
					    text-decoration:none;
					    color:#ffffff;
					  }
					
					  div,p,ul,h1{
					    margin:0;
					  }
					
					  .bgBody{
					    background: #ffffff;
					  }
					  .bgItem{
					    background: #ffffff;
					  }
					
					  @media only screen and (max-width:480px)
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					    td[class="left_pad"] 
					    {
					      padding-left:15px !important;
					      padding-right:15px !important;
					    }
					
					  }
					
					  @media only screen and (max-width:540px) 
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					  }
					
					</style>
					
					<body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
					  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center"  style="font-family:Helvetica, Arial,serif;">
					    <tbody>
					      <tr>
					        <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
					          <tbody>
					            <tr>
					              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tbody>
					                  <tr>
					                    <td valign="top" width="40">&nbsp;</td>
					                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tbody>
					                        <tr>
					                          <td height="-12" class="spechide"></td>
					
					                        </tr>
					                        <tr>
					                          <td class="movableContentContainer" valign="top">
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="35"></td>
					                                  </tr>
					                                  <tr>
					                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                      <tbody>
					                                        <tr>
					                                          <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                               <p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">IDM Cursos</span></strong> </p>
																	<p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">Recuperação de Senha</span></strong> </p>
					                                              </div>
					                                            </div></td>
					                                          </tr>
					                                        </tbody>
					                                      </table>
					                                    </td>
					                                  </tr>
					                                </tbody>
					                              </table>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					                                <tr>
					                                  <td align="left">
					                                  <br><br>
					                                    <div class="contentEditableContainer contentTextEditable">
					                                      <div class="contentEditable" align="center">
					                                        <p>
					                                          <p class="callout">
					                                           Olá <strong>'.$nome.'</strong>, você solicitou recuperação de senha utilize as seguintes informações para seu acesso:
					                                            <br>
					                                            <br>
					                                            <strong>E-mail: </strong>'.$email.'<br>
					                                            <strong>Senha: </strong>'.base64_decode($senha).'<br>
					                                          </p>
					                                        </p>
					                                      </div>
					                                    </div>
					                                  </td>
					                                </tr>
					
					                                <tr><td height="15"></td></tr>
					
					                                <tr>
					                                  <td align="center" >
					                                    <table>
					                                      <tr>
					                                        <td align="center" width="200px" bgcolor="#1A54BA" style="background:#005b95; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;">
					                                          <div class="contentEditableContainer contentTextEditable">
					                                            <div class="contentEditable" align="center">
					                                              <a target="_blank" href="https://www.idmcursos.com.br/ingles/" class="link2" style="color:#ffffff;"><font color="#fff">Acessar Agora</font></a>
					                                            </div>
					                                          </div>
					                                        </td>
					                                      </tr>
					                                    </table>
					                                  </td>
					                                </tr>
					                                <tr><td height="10"></td></tr>
					                              </table>
					                              <br>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="15">
					                                    </tr>
					                                    <tr>
					                                      <td  style="border-bottom:1px solid #DDDDDD;"></td>
					                                    </tr>
					                                    <tr><td height="15"></td></tr>
					                                    <tr>
					                                      <td>
					                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                        <tbody>
					                                          <tr>
					                                            <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                                <p  style="text-align:left;color:#4f4f4f;font-size:12px;font-weight:normal;line-height:20px;">
					                                                  <span style="font-weight:bold;">Copyright @ IDM Cursos '.date("Y").'</span>
					                                                   atendimento@cidadeaprendizagem.com.br
					                                                  <br>
					                                                </div>
					                                              </div></td>
					                                              <td valign="top" width="30" class="specbundle">&nbsp;</td>
					                                              <td valign="top" class="specbundle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					
					                                              </td>
					                                            </tr>
					                                          </tbody>
					                                        </table>
					                                      </td>
					                                    </tr>
					                                    <tr><td height="48"></td></tr>
					                                  </tbody>
					                                </table>
					
					                              </div>
					
					                              <!-- =============================== Rodapé ====================================== -->
					
					                            </td>
					                          </tr>
					                        </tbody>
					                      </table>
					                    </td>
					                    <td valign="top" width="40">&nbsp;</td>
					                  </tr>
					                </tbody>
					              </table>
					            </td>
					          </tr>
					        </tbody>
					      </table>
					    </td>
					  </tr>
					</tbody>
					</table>
					</body>
					</html>';
	}
}
?>