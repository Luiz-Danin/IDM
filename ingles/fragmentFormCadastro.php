<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
echo $html = '<div class="col-md-5" id="tela">
							<div class="signup-header wow fadeInUp">
								<h3 class="form-title text-center">Cadastro</h3>
								<div class="alert alert-info">Cadastro não encontrado. Digite os seus dados.</div>
								<form action="" class="form-header" id="" method="POST">
									<input type="hidden" name="flagCadastro" value="1">
									<input type="hidden" name="id_curso" value="635">
									<div class="form-group">
										<input class="form-control input-lg" name="nome" id="nameCadastro" 
											type="text" placeholder="Nome e Sobrenome (com no mínimo 5 letras)" pattern="[A-zÀ-ž]+[ ][A-zÀ-ž\s]+" required title="Digite pelo menos seu Nome e Sobrenome, mínimo 5 letras">
									</div>
									<div class="form-group">
										<input value="'.$_GET['email'].'" class="form-control input-lg" name="email" id="login"
											type="email" placeholder="Seu E-mail" required>
									</div>
									<div class="form-group row">
										<div class="form-group col-xs-6 col-sm-4 col-md-3 col-lg-3">
											<input class="form-control input-lg phone" name="ddd_celular"
											id="ddd" type="text" placeholder="DDD" maxlength="2" pattern="\d{2}" required title="Somente números, apenas 2 digitos" onkeydown="return ( event.ctrlKey || event.altKey 
																							                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
																							                    || (95<event.keyCode && event.keyCode<106)
																							                    || (event.keyCode==8) || (event.keyCode==9) 
																							                    || (event.keyCode>34 && event.keyCode<40) 
																							                    || (event.keyCode==46) )">
										</div>
										<div class="form-group col-xs-6 col-sm-8 col-md-9 col-lg-9">
											<input class="form-control input-lg phone" name="celular"
											id="telefone" type="text" placeholder="Celular" maxlength="9" pattern="\d{8}|\d{9}" required title="Somente números, 8 ou 9 dígitos" onkeydown="return ( event.ctrlKey || event.altKey 
																							                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
																							                    || (95<event.keyCode && event.keyCode<106)
																							                    || (event.keyCode==8) || (event.keyCode==9) 
																							                    || (event.keyCode>34 && event.keyCode<40) 
																							                    || (event.keyCode==46) )">
										</div>
									</div>
									<div class="form-group">
										<input class="form-control input-lg" name="cep" id="cep"
											type="text" placeholder="CEP"  maxlength="8" pattern="\d{8}" required title="Somente números, apenas 8 digitos" onkeydown="return ( event.ctrlKey || event.altKey 
																							                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
																							                    || (95<event.keyCode && event.keyCode<106)
																							                    || (event.keyCode==8) || (event.keyCode==9) 
																							                    || (event.keyCode>34 && event.keyCode<40) 
																							                    || (event.keyCode==46) )">
									</div>
									<div class="form-group">
										<input class="form-control input-lg" name="senha"
											id="password" type="password" placeholder="Senha" pattern=".{6,}" required title="Mínimo 6 dígitos">
									</div>
									<div class="checkbox">
										<label> <input type="checkbox">
										<p>Lembrar-me</p></label>
									</div>
									<div class="form-group last">
										<input name="enviar" id="submitForm" type="submit" class="btn-secondary btn-block btn-lg" value="Entrar">
										<div class="fb-login-button" data-max-rows="1"
											data-size="small" data-button-type="login_with"
											data-show-faces="false" data-auto-logout-link="false"
											data-use-continue-as="false"></div>
									</form>
									<p></p>
								</div>
							</div>';
?>
<script>
/* $(document).on('keypress', '#nameCadastro', function (event) {
    var regex = new RegExp("^[A-zÀ-ž ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}); */
</script>