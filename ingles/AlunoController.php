<?php
require_once 'EntidadeInterface.php';
require_once 'Aluno.php';
require_once 'ServiceDB.php';

class AlunoController
{
	public function indexIDMCursos(Request $request, $conexao)
	{
		if ( $request->isElement('enviar') )
		{
			$aluno = new Aluno();
		
			$aluno->setEmail( $request->getKey('email') );
			$aluno->setSenha($request->getKey('senha') );
		
			$serviceDB = new ServiceDb($conexao, $aluno);
			$email = $serviceDB->findEmail( $request->getKey('email') );
			$email = $email['email'];
			
			if ( ( $email==$request->getKey('email') ) && ($request->getKey('senha')!='') && ($request->getKey('flagLogin')=='0') )
			{
				$this->login($request, $email, $serviceDB);
			}
			else if ( !( $email==$request->getKey('email') ) && ($request->getKey('flagCadastro')=='1') )
			{
				$this->saveAluno($request, $aluno, $serviceDB);
			}
			elseif ( ( $email==$request->getKey('email') ) && ( $request->getKey('senha')!='' ) && ( $request->getKey('senhaNova')!='' ) && ($request->getKey('flagUpdatePassword')=='2'))
			{
				$this->editPasswordLogin($request, $aluno, $serviceDB);
			}
			else 
				echo '<meta http-equiv="refresh" content="0; url=./" />';
		}
		
	}
	
	public function generateIdTransacaoAluno($conexao, $id_aluno)
	{
		$aluno = new Aluno();
		
		$serviceDB = new ServiceDb($conexao, $aluno);
		$email = $serviceDB->findEmailByIdAluno($id_aluno);
		
		$salt = 'K6tfBMaPlrfm1eJFAC110J';
		$salt= str_shuffle($salt);
		$email = $email['email'];
		$custo = '08';
		
		$hash = crypt($email, '$2a$' . $custo . '$' . $salt . '$');
		
		$id_transacao = substr($hash, -8);
		$id_transacao = strtoupper($id_transacao);
		
		return 'IDMC'.$id_transacao;
	}
	
	public function getAlunoByIdAluno($conexao, $id_aluno)
	{
		if ($id_aluno)
		{
			$aluno = new Aluno();
			
			$serviceDB = new ServiceDb($conexao, $aluno);
			$dataAluno = $serviceDB->findId($id_aluno);
			
			return $dataAluno;
		}
		else 
			return false;
	}
	
	public function checkPagamento($conexao, $id_aluno)
	{
		$aluno = new Aluno();
		
		$serviceDB = new ServiceDb($conexao, $aluno);
		$isPagamento = $serviceDB->findPagamentoAluno($id_aluno);
		
		if (!$isPagamento)
		{
			return true;
		}
		else 
			return false;
	}
	
	public function estudarAlunoAVA($conexao, $id_aluno)
	{
		$aluno = new Aluno();
		$serviceDB = new ServiceDb($conexao, $aluno);
		$alunoAVA = $serviceDB->getDataAlunoAVA($id_aluno);
		
		$arrayAlunoAVA = array();
		$arrayAlunoAVA['turma'] = $alunoAVA['turma'];
		$arrayAlunoAVA['email'] = $alunoAVA['email'];
		$arrayAlunoAVA['nome'] = $alunoAVA['nome'];
		$arrayAlunoAVA['aluno'] = $alunoAVA['id_aluno'];
		$arrayAlunoAVA['curso'] = 1;
		$arrayAlunoAVA['tipo'] = 4;
		
		return $arrayAlunoAVA;
	}
	
	public function checkEstudar($conexao, $id_aluno, $id_curso)
	{
		$aluno = new Aluno();
		
		$serviceDB = new ServiceDb($conexao, $aluno);
		$isEstudar = $serviceDB->isEstudarAluno($id_aluno, $id_curso);
		
		if (!$isEstudar)
			return false;
		else 
			return true;
	}
	
	public function saveMatriculaAluno($conexao, $id_aluno, Request $request)
	{
		$aluno = new Aluno();
		
		$serviceDb = new ServiceDb($conexao, $aluno);
		$isAlunoMatriculado = $serviceDb->insereMatriculaAluno($id_aluno, 635, $request);
		
		if ($isAlunoMatriculado)
			return true;
		else 
			return false;
	}
	
	private function getDataWebServiceCorreio($cep)
	{
		$url = 'http://viacep.com.br/ws/'.$cep.'/json/';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$dataAlunoWebService = curl_exec($ch);
		curl_close($ch);
		
		$dataAlunoWebService = json_decode($dataAlunoWebService, true);
		
		if ( isset($dataAlunoWebService["erro"]) )
		{
			$dataAlunoWebService['bairro'] = 'Não Informado';
			$dataAlunoWebService['uf'] = 'Não Informado';
			$dataAlunoWebService['localidade'] = 'Não Informado';
			$dataAlunoWebService['logradouro'] = 'Não Informado';
		}
		
		return $dataAlunoWebService;
	}
	
	private function saveAluno(Request $request, Aluno $aluno, ServiceDb $serviceDB)
	{
	    
		$dataAlunoWebService = $this->getDataWebServiceCorreio( $request->getKey('cep') );
		
		$aluno->setNome( $request->getKey('nome') );
		$aluno->setEmail( $request->getKey('email') );
		$aluno->setDDD($request->getKey('ddd_celular'));
		$aluno->setTelefone( $request->getKey('celular') );
		$aluno->setCep( $request->getKey('cep') );
		$aluno->setBairro($dataAlunoWebService['bairro']);
		$aluno->setEstado($dataAlunoWebService['uf']);
		$aluno->setCidade($dataAlunoWebService['localidade']);
		$aluno->setEndereco($dataAlunoWebService['logradouro']);
		$aluno->setSenha($request->getKey('senha'));
		
		$email = $serviceDB->findEmail( $request->getKey('email') );
		
		if (!$email)
		{
			if ( $aluno->checkAluno() )
			{
				$id_aluno = $serviceDB->inserirAluno( $request->getKey('id_curso'), $request);
				if ( $id_aluno)
				{
				    include 'cod_conversion.php';
					$_SESSION['nome'] = $aluno->getNome();
					$_SESSION['id_aluno'] = $id_aluno;
					$_SESSION['id_aluno_hash'] = $this->generateHashBcrypt($id_aluno);
					
					if ( $request->getKey('check')=='on' )
						setcookie("senha", $senha, time()+ (10 * 365 * 24 * 60 * 60));
				}
				else 
				{
					echo '<script>alert("Atenção: Falha ao Cadastrar os dados do Aluno!")</script>';
					echo '<meta http-equiv="refresh" content="0; url=./" />';
				}
			}
			else
			{
				echo '<script>alert("Atenção: Não foi possível realizar o Cadastro, problemas na validação dos dados do Aluno!")</script>';
				echo '<meta http-equiv="refresh" content="0; url=./" />';
			}
		}
		else 
		{
			echo '<script>alert("Atenção: E-mail já Cadastrado!")</script>';
			echo '<meta http-equiv="refresh" content="0; url=./" />';
		}
	}
	
	private function editPasswordLogin(Request $request, Aluno $aluno ,ServiceDb $serviceDB)
	{
		$email = $request->getKey('email');
		$senha = $request->getKey('senha');
		$senhaNova =  $request->getKey('senhaNova');
		
		$dadosAluno = $serviceDB->findAlunoSenhaVazia($email);
		
		if ($dadosAluno==false)
		{
			echo '<script>alert("Atenção: Este E-mail já possui uma senha cadastrada!")</script>';
			echo '<meta http-equiv="refresh" content="0; url=./" />';
		}
		else
		{
			if ( $senha!=$senhaNova ||($senha== "" || $senha== null) || ($senhaNova== "" || $senhaNova== null) )
			{
				echo '<script>alert("Atenção: A senha precisa ser igual a senha de confirmação!")</script>';
				echo '<meta http-equiv="refresh" content="0; url=./" />';
			}
			else 
			{
				$aluno->setEmail($email);
				$aluno->setSenha($senha);
				$nomeAluno = $serviceDB->findNomeByEmail($email);
				
				if ( $serviceDB->UpdateSenhaByEmail($dadosAluno['id_aluno'], $request->getKey('id_curso'), $request ) )
				{
					$_SESSION['id_aluno'] = $dadosAluno['id_aluno'];
					$_SESSION['nome'] = $nomeAluno['nome'];
					$_SESSION['id_aluno_hash'] = $this->generateHashBcrypt($dadosAluno['id_aluno']);
					
					if ( $request->getKey('check')=='on' )
						setcookie("senha", $senha, time()+ (10 * 365 * 24 * 60 * 60));
				}
			}
			
		}
			
	}
	
	private function generateHashBcrypt($id_aluno)
	{
		$custo = '12';
		$salt = 'p3fdix834wjkcz39f7sn9f';
		$hash = crypt($id_aluno, '$2a$' . $custo . '$' . $salt . '$');
		
		return $hash;
	}
	
	private function login(Request $request, $email, ServiceDb $serviceDB)
	{
		$aluno = $serviceDB->findAlunoByEmailSenha( $email, $request->getKey('senha') );
		
		$id = $aluno['id_aluno'];
		$nome = $aluno['nome'];
		$senha = base64_decode( $aluno['senha'] );
		
		if( $aluno==false )
		{
			echo '<script>alert("Atenção: Falha no Login!")</script>';
			echo '<meta http-equiv="refresh" content="0; url=./" />';
		}
		else
			{
				$_SESSION['nome'] = $nome;
				$_SESSION['id_aluno'] = $id;
				$_SESSION['id_aluno_hash'] = $this->generateHashBcrypt($id);
				if ( $request->getKey('check')=='on' )
					setcookie("senha", $senha, time()+ (10 * 365 * 24 * 60 * 60));
			}
	}
	
}