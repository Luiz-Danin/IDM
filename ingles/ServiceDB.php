<?php
require_once 'Request.php';
require_once 'mail/TemplateEmail.php';

class ServiceDb
{
	private $db;
	private $entity;
	
	public function __construct(\PDO $db, EntidadeInterface $entity)
	{
		$this->db = $db;
		$this->entity = $entity;
	}
	
	public function findId($id_aluno)
	{
		try 
		{
			$query = "SELECT * FROM {$this->entity->getTable()} WHERE id_aluno=:id_aluno";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id_aluno", $id_aluno);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar os dados do aluno pelo id do aluno: '.$e->getMessage();
		}
	}
	
	public function findNomeByEmail($email)
	{
		try
		{
			$query = "SELECT nome FROM {$this->entity->getTable()} WHERE email=:email";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar nome por email: '.$e->getMessage();
		}
	}
	
	public function findEmail($email)
	{
		try
		{
			$query = "SELECT id_aluno, email FROM {$this->entity->getTable()} WHERE email=:email";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar email'.$e->getMessage();
		}
	}
	
	public function findEmailEmptyPassword($email)
	{
		try
		{
			$query = "SELECT senha FROM {$this->entity->getTable()} WHERE email=:email";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar email com senha vazia: '.$e->getMessage();
		}
	}
	
	public function findSenha($senha)
	{
		try
		{
			$query = "SELECT senha FROM {$this->entity->getTable()} WHERE senha=:senha";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":senha", $senha);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar senha: '.$e->getMessage();
		}
	}
	
	public function findEmailByIdAluno($id_aluno)
	{
		try
		{
			$query = "SELECT email FROM {$this->entity->getTable()} WHERE id_aluno=:id_aluno";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id_aluno", $id_aluno);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar senha pelo id_aluno: '.$e->getMessage();
		}
	}
	
	public function findSenhaByEmail($email)
	{
		try
		{
			$query = "SELECT senha FROM {$this->entity->getTable()} WHERE email=:email";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar senha por email: '.$e->getMessage();
		}
	}
	
	public function findAlunoByEmailSenha($email,$senha)
	{
		try
		{
			$query = "SELECT * FROM {$this->entity->getTable()} WHERE email=:email AND senha=:senha";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->bindValue(":senha", base64_encode($senha) );
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar aluno pelo email e senha: '.$e->getMessage();
		}
	}
	
	public function findAlunoSenhaVazia($email)
	{
		try
		{
			$query = "SELECT * FROM {$this->entity->getTable()} WHERE email=:email AND senha='' ";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar aluno sem senha: '.$e->getMessage();
		}
	}
	
	public function findDadosMatricula($id_aluno, $id_aluno_hash)
	{
		try
		{
			if (crypt($id_aluno, $id_aluno_hash) === $id_aluno_hash)
			{
				$query = "SELECT * FROM {$this->entity->getTable()} WHERE id_aluno=:id_aluno ";
				$stmt = $this->db->prepare($query);
				$stmt->bindValue(":id_aluno", $id_aluno);
				$stmt->execute();
				
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar os dados da matricula do aluno: '.$e->getMessage();
		}
	}
	
	public function findPagamentoAluno($id_aluno)
	{
		try
		{
			$query = "SELECT * FROM transacao WHERE id_aluno=:id_aluno AND situacao=3";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id_aluno", $id_aluno);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar os dados da matricula do aluno: '.$e->getMessage();
		}
	}
	
	public function getDataAlunoAVA($id_aluno)
	{
		try
		{
			$id_curso = 635;
			
			$queryIdTurma = "SELECT at.id_turma FROM aluno_turma at JOIN turma t ON (t.id_turma = at.id_turma) WHERE at.id_aluno=:id_aluno";
			$stmtIdTurma= $this->db->prepare($queryIdTurma);
			$stmtIdTurma->bindValue(":id_aluno", $id_aluno);
			$stmtIdTurma->execute();
			
			$id_turma = $stmtIdTurma->fetch(\PDO::FETCH_ASSOC);
			$turma = $id_turma['id_turma'];
			
			try
			{
				$query = "SELECT ta.id_ava as turma, a.email as email, a.nome, alc.id_aluno as id_aluno FROM aluno a  join aluno_turma al on(a.id_aluno=al.id_aluno) 
                                            join turma_ava ta on (ta.id_turma=al.id_turma) 
                                            join aluno_curso alc on(alc.id_aluno=a.id_aluno)   
                                            where ta.id_turma=:id_turma and a.id_aluno=:id_aluno and alc.id_curso=635 and date(alc.ultimo_dia)>now()";
				$stmt = $this->db->prepare($query);
				$stmt->bindValue(":id_aluno", $id_aluno);
				$stmt->bindValue(":id_turma", $turma);
				
				$stmt->execute();
				
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(Exception $e)
			{
				echo 'Erro ao retornar o aluno para estudar no AVA: '.$e->getMessage();
			}
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar o id do aluno: '.$e->getMessage();
		}
	}
	
	public function isEstudarAluno($id_aluno, $id_curso)
	{
		
			try
			{
				$query = "SELECT * FROM `aluno` a  join aluno_turma al on(a.id_aluno=al.id_aluno) "
                                        . "join turma_ava ta on (ta.id_turma=al.id_turma) "
                                        . "join aluno_curso alc on(alc.id_aluno=a.id_aluno)   "
                                        . "where a.id_aluno=:id_aluno and alc.id_curso=:id_curso and date(alc.ultimo_dia)>now()";
				
				$stmt = $this->db->prepare($query);
				$stmt->bindValue(":id_aluno", $id_aluno);
				$stmt->bindValue(":id_curso", $id_curso);
				$stmt->execute();
				
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			}
			catch(Exception $e)
			{
				echo 'Erro ao retornar o aluno com permissão para estudar: '.$e->getMessage();
			}
		
	}
	
	public function listar($ordem = null)
	{
		if($ordem) {
			$query = "Select * from {$this->entity->getTable()} order by {$ordem}";
		} else {
			$query = "Select * from {$this->entity->getTable()}";
		}
		
		
		$stmt = $this->db->query($query);
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
	
	public function inserirAluno($id_curso, Request $request)
	{
				try
				{
					$query = "INSERT INTO {$this->entity->getTable()}(nome,email,ddd_celular,celular,data_cadastro,cep,bairro,estado,cidade,endereco,numero,senha) VALUES (:nome,:email,:ddd_celular,:celular,:data_cadastro, :cep, :bairro, :estado, :cidade, :endereco,:numero,:senha)";
					
					$stmt = $this->db->prepare($query);
					$stmt->bindValue(':nome', $this->entity->getNome());
					$stmt->bindValue(':email', $this->entity->getEmail());
					$stmt->bindValue(':ddd_celular', $this->entity->getDDD());
					$stmt->bindValue(':celular', $this->entity->getTelefone());
					$stmt->bindValue(':data_cadastro', date('Y-m-d H:i:s') );
					$stmt->bindValue(':cep', $this->entity->getCep());
					$stmt->bindValue( ':bairro', $this->entity->getBairro() );
					$stmt->bindValue( ':estado', $this->entity->getEstado() );
					$stmt->bindValue( ':cidade', $this->entity->getCidade() );
					$stmt->bindValue( ':endereco', $this->entity->getEndereco() );
					$stmt->bindValue( ':numero', 999 );
					$stmt->bindValue(':senha', base64_encode( $this->entity->getSenha() ) );
					
					if ( $stmt->execute() )
					{
						$lastIdAluno = $this->db->lastInsertId();
						$this->sendEmailCadastro($lastIdAluno);
                                                $celular = $this->entity->getDDD().$this->entity->getTelefone();
                                                $mensagem = "Ola ".$this->entity->getNome().", seja bem vindo(a) ao IDM Cursos. Acesse o site: www.idmcursos.com.br Login: ".$this->entity->getEmail().' Senha: '.$this->entity->getSenha();
                                                $this->enviar_sms($celular, $mensagem);
						
						$alunoUnidade = $this->inserirAlunoUnidade($lastIdAluno);
						$alunoInstituicao = $this->inserirAlunoInstituicao($lastIdAluno);
						
						//$alunoMatriculado = $this->insereMatriculaAluno($lastIdAluno, $id_curso, $request);
						
						if ($alunoUnidade && $alunoInstituicao)
							return $lastIdAluno;
					}
				}
				catch(Exception $e)
				{
					echo 'Erro ao inserir aluno: '.$e->getMessage();
				}
	}
        
        private function enviar_sms($celular, $mensagem)
        {
            $ch = curl_init();

            $post = array(
                'token' => 'XHJKORWT0L37TQ83XAONTR5US2LPJEL3',
                'chave' => '8VG8LLRBKN2JPVUQDGSSX8LT05YABSTR',
                'instituicao' => 'IDMCURSOS',
                'celular' => $celular,
                'mensagem' => $mensagem
            );

            curl_setopt($ch, CURLOPT_URL, 'https://www.ovum.com.br/api_sms/');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

            $requisicao = curl_exec($ch);
            return json_decode($requisicao);
            curl_close ($ch); 
        }
	
	private function sendEmailCadastro($id_aluno)
	{
		try
		{
			$query = "SELECT nome, email, senha FROM {$this->entity->getTable()} WHERE id_aluno=:id_aluno";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id_aluno", $id_aluno);
			$stmt->execute();
			
			$dataAluno = $stmt->fetch(\PDO::FETCH_ASSOC);
			
			$subject = 'Bem-vindo ao IDM Cursos, '.$dataAluno['nome'];
			$EmailTo = $dataAluno['email'];
			
			$Body  = TemplateEmail::cadastro($dataAluno['nome'], $dataAluno['email'], $dataAluno['senha']);
			
			$data = array('email'=>$EmailTo,'subject'=>$subject,'body'=>$Body);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://curso.me/mail/LibMail.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			curl_exec($ch);
			curl_close($ch);
		}
		catch(Exception $e)
		{
			echo 'Erro ao enviar e-mail ao aluno: '.$e->getMessage();
		}
	}
	
	public function sendEmailRecuperaSenha($email)
	{
		try
		{
			$query = "SELECT nome, email, senha, ddd_celular, celular FROM {$this->entity->getTable()} WHERE email=:email";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":email", $email);
			$stmt->execute();
			
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			echo 'Erro ao enviar e-mail de recuperação  ed senha: '.$e->getMessage();
		}
		
	}
	
	public function insereMatriculaAluno($id_aluno, $id_curso, Request $request)
	{
		try
		{
			$alunoCurso = $this->inserirAlunoCurso($id_aluno, $id_curso, $request);
			$alunoTurma = $this->inserirAlunoTurma($id_aluno, $id_curso);
			$alunoCursoTurma = $this->inserirAlunoCursoTurma($id_aluno, $id_curso);
			
			if ($alunoCurso && $alunoTurma && $alunoCursoTurma)
				return true;
			else 
				return false;
		}
		catch(Exception $e)
		{
			echo 'Erro ao inserir matricula'.$e->getMessage();
		}
	}
	
	private function inserirAlunoCursoTurma($id_aluno, $id_curso)
	{
		try
		{
			$queryIdTurma = "SELECT id_turma FROM turma WHERE id_curso = :id_curso AND  situacao = :situacao";
			$stmtIdTurma= $this->db->prepare($queryIdTurma);
			$stmtIdTurma->bindValue(':id_curso', $id_curso);
			$stmtIdTurma->bindValue(':situacao', 1);
			$stmtIdTurma->execute();
			
			$id_turma =  $stmtIdTurma->fetch(\PDO::FETCH_ASSOC);
			$turma = $id_turma['id_turma'];
			
			try
			{
				$queryAlunoCurso = "SELECT id_historico FROM aluno_curso WHERE id_aluno=:id_aluno";
				$stmtAlunoCurso = $this->db->prepare($queryAlunoCurso);
				$stmtAlunoCurso->bindValue(":id_aluno", $id_aluno);
				$stmtAlunoCurso->execute();
				
				$idHistoricoAluno = $stmtAlunoCurso->fetch(\PDO::FETCH_ASSOC);
			}
			catch(Exception $error)
			{
				echo 'Erro: '.$error->getMessage();
			}
			
			$query = "INSERT INTO aluno_curso_turma (id_turma, id_historico) VALUES  (:id_turma,:id_historico)";
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(":id_turma", $turma);
			$stmt->bindValue(":id_historico", $idHistoricoAluno['id_historico']);
			
			return $stmt->execute();
		}
		catch(Exception $e)
		{
			echo 'Erro ao inserir Aluno em Curso Turma: '.$e->getMessage();
		}
	}
	
	private function inserirAlunoTurma($id_aluno, $id_curso)
	{
		try
		{
			$queryIdTurma = "SELECT id_turma FROM turma WHERE id_curso = :id_curso AND  situacao = :situacao";
			$stmtIdTurma= $this->db->prepare($queryIdTurma);
			$stmtIdTurma->bindValue(':id_curso', $id_curso);
			$stmtIdTurma->bindValue(':situacao', 1);
			$stmtIdTurma->execute();
			
			$id_turma =  $stmtIdTurma->fetch(\PDO::FETCH_ASSOC);
			$turma = $id_turma['id_turma'];
			
			try
			{
				$query = "INSERT INTO aluno_turma (id_aluno,id_turma) VALUES  (:id_aluno,:id_turma)";
				
				$stmt = $this->db->prepare($query);
				$stmt->bindValue( ':id_aluno', $id_aluno);
				$stmt->bindValue( ':id_turma', $turma);
				
				if ( $stmt->execute() )
					return true;
			}
			catch(Exception $e)
			{
				echo 'Erro ao inserir aluno na turma: '.$e->getMessage();
			}
		}
		catch(Exception $e)
		{
			echo 'Erro ao retornar id da Turma: '.$e->getMessage();
		}
	}
        
        
	
	private function getDataEncerramento(Request $request, $id_turma)
	{
			$data_encerramento = '';
			
			if ( $request->getKey('flagOpcaPagamento')=='CartaoCredito' )
			{
//				$queryTurma = 'SELECT data_encerramento FROM turma WHERE  id_turma = :id_turma';
//				
//				$stmtTurma= $this->db->prepare($queryTurma);
//				$stmtTurma->bindValue(':id_turma', $id_turma);
//				$stmtTurma->execute();
//				
//				$data_encerramento = $stmtTurma->fetch(\PDO::FETCH_ASSOC);
//				$data_encerramento = $data_encerramento['data_encerramento'];
                            $data_encerramento = date('Y-m-d');
                            $data_encerramento = date('Y-m-d', strtotime($data_encerramento. ' + 30 days') );
			}
			else 
			{
				$data_encerramento = date('Y-m-d');
				$data_encerramento = date('Y-m-d', strtotime($data_encerramento. ' + 30 days') );
			}
			
			return $data_encerramento;
	}
        public function inserirAlunoCursoPOST($id_aluno, $id_curso)
	{
            die("hgdajfhdg");
            $id_aluno = $_POST['id_aluno'];
            $id_curso = $_POST['id_curso'];
            
		try
		{
			$queryTurma = "SELECT id_turma FROM turma WHERE id_curso=:id_curso AND situacao=:situacao";
			
			$stmtTurma = $this->db->prepare($queryTurma);
			$stmtTurma->bindValue(":id_curso", $id_curso);
			$stmtTurma->bindValue(":situacao", 1 );
			$stmtTurma->execute();
			
			$id_turma =  $stmtTurma->fetch(\PDO::FETCH_ASSOC);
			$turma = $id_turma['id_turma'];
                        
                        $data_encerramento = date('Y-m-d');
                        $data_encerramento = date('Y-m-d', strtotime($data_encerramento. ' + 30 days') );
			
			//$data_encerramento = $this->getDataEncerramento($request, $turma);
			
			try
			{
				$query = "INSERT INTO aluno_curso (id_aluno, id_curso, codigo_autenticacao, matricula, carga_horaria, nota, primeira_hora, primeiro_dia, ultima_hora, ultimo_dia, situacao,
														base_legal, ordem, id_instituicao_unidade)
										 VALUES  (:id_aluno, :id_curso, :codigo_autenticacao, :matricula, :carga_horaria, :nota, :primeira_hora, :primeiro_dia, :ultima_hora, :ultimo_dia, :situacao, :base_legal, :ordem, :id_instituicao_unidade)";
				
				$hash = date('Y') . '-' . strtoupper(substr(md5($id_aluno . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($id_aluno . ':' . microtime()), 0, 4));
				
				$autenticacao = strtoupper(substr(md5($hash), 0, 8));
				$autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
				
				
				$stmt = $this->db->prepare($query);
				$stmt->bindValue(':id_aluno', $id_aluno);
				$stmt->bindValue(':id_curso', 635);
				$stmt->bindValue(':codigo_autenticacao', $hash);
				$stmt->bindValue(':matricula', $autenticacao);
				$stmt->bindValue(':carga_horaria', 120);
				$stmt->bindValue(':nota', (float)0);
				$stmt->bindValue(':primeira_hora', date('H:i:s') );
				$stmt->bindValue(':primeiro_dia', date('Y-m-d') );
				$stmt->bindValue(':ultima_hora', date('H:i:s') );
				$stmt->bindValue(':ultimo_dia', $data_encerramento );
				$stmt->bindValue(':situacao', 0 );
				$stmt->bindValue(':base_legal', 1 );
				$stmt->bindValue(':ordem', 1 );
				$stmt->bindValue(':id_instituicao_unidade', 2 );
				
				if ( $stmt->execute() )
					return true;
			}
			catch(Exception $e)
			{
				echo 'Erro ao inserir aluno no curso: '.$e->getMessage();
			}
		
		}
		catch(Exception $e)
		{
			
		}
	}
	
	private function inserirAlunoCurso($id_aluno, $id_curso, Request $request)
	{
		try
		{
			$queryTurma = "SELECT id_turma FROM turma WHERE id_curso=:id_curso AND situacao=:situacao";
			
			$stmtTurma = $this->db->prepare($queryTurma);
			$stmtTurma->bindValue(":id_curso", $id_curso);
			$stmtTurma->bindValue(":situacao", 1 );
			$stmtTurma->execute();
			
			$id_turma =  $stmtTurma->fetch(\PDO::FETCH_ASSOC);
			$turma = $id_turma['id_turma'];
			
			$data_encerramento = $this->getDataEncerramento($request, $turma);
			
			try
			{
				$query = "INSERT INTO aluno_curso (id_aluno, id_curso, codigo_autenticacao, matricula, carga_horaria, nota, primeira_hora, primeiro_dia, ultima_hora, ultimo_dia, situacao,
														base_legal, ordem, id_instituicao_unidade)
										 VALUES  (:id_aluno, :id_curso, :codigo_autenticacao, :matricula, :carga_horaria, :nota, :primeira_hora, :primeiro_dia, :ultima_hora, :ultimo_dia, :situacao, :base_legal, :ordem, :id_instituicao_unidade)";
				
				$hash = date('Y') . '-' . strtoupper(substr(md5($id_aluno . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($id_aluno . ':' . microtime()), 0, 4));
				
				$autenticacao = strtoupper(substr(md5($hash), 0, 8));
				$autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
				
				
				$stmt = $this->db->prepare($query);
				$stmt->bindValue(':id_aluno', $id_aluno);
				$stmt->bindValue(':id_curso', 635);
				$stmt->bindValue(':codigo_autenticacao', $hash);
				$stmt->bindValue(':matricula', $autenticacao);
				$stmt->bindValue(':carga_horaria', 120);
				$stmt->bindValue(':nota', (float)0);
				$stmt->bindValue(':primeira_hora', date('H:i:s') );
				$stmt->bindValue(':primeiro_dia', date('Y-m-d') );
				$stmt->bindValue(':ultima_hora', date('H:i:s') );
				$stmt->bindValue(':ultimo_dia', $data_encerramento );
				$stmt->bindValue(':situacao', 0 );
				$stmt->bindValue(':base_legal', 1 );
				$stmt->bindValue(':ordem', 1 );
				$stmt->bindValue(':id_instituicao_unidade', 2 );
				
				if ( $stmt->execute() )
					return true;
			}
			catch(Exception $e)
			{
				echo 'Erro ao inserir aluno no curso: '.$e->getMessage();
			}
		
		}
		catch(Exception $e)
		{
			
		}
	}
	
	private function inserirAlunoUnidade($id_aluno)
	{
		try
		{
			$query = "INSERT INTO aluno_unidade (id_aluno,id_unidade) VALUES  (:id_aluno,:id_unidade)";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':id_aluno', $id_aluno);
			$stmt->bindValue(':id_unidade', 2);
			
			return $stmt->execute();
		}
		catch(Exception $e)
		{
			echo 'Erro ao inserir aluno na Unidade: '.$e->getMessage();
		}
	}
	
	private function inserirAlunoInstituicao($id_aluno)
	{
		try
		{
			$query = "INSERT INTO aluno_instituicao (id_aluno,id_instituicao) VALUES  (:id_aluno,:id_instituicao)";
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':id_aluno', $id_aluno);
			$stmt->bindValue(':id_instituicao', 2);
			
			return $stmt->execute();
		}
		catch(Exception $e)
		{
			echo 'Erro ao inserir aluno na InstituiÃ§Ã£o: '.$e->getMessage();
		}
	}
	
	public function alterar()
	{
		$query = "Update {$this->entity->getTable()} set nome=:nome, email=:email Where id=:id";
		
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':id', $this->entity->getId());
		$stmt->bindValue(':nome', $this->entity->getNome());
		$stmt->bindValue(':email', $this->entity->getEmail());
		
		if($stmt->execute()) {
			return true;
		}
	}
	
	public function UpdateSenhaByEmail($id_aluno, $id_curso, Request $request)
	{
		try
		{
			$query = "UPDATE {$this->entity->getTable()} SET senha=:senha WHERE email=:email";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':email', $this->entity->getEmail());
			$stmt->bindValue(':senha',  base64_encode($this->entity->getSenha()) );
			
			if($stmt->execute())
			{
				$alunoUnidade = $this->inserirAlunoUnidade($id_aluno);
				$alunoInstituicao = $this->inserirAlunoInstituicao($id_aluno);
				$alunoMatricualdo = $this->insereMatriculaAluno($id_aluno, $id_curso, $request);
				
				if ($alunoUnidade && $alunoInstituicao && $alunoMatricualdo)
					return true;
			}
		}
		catch(Exception $e)
		{
			echo 'Erro ao atualizar senha do aluno: '.$e->getMessage();
		}
	}
	
	public function updateDadosAluno(Request $request)
	{
		try
		{
			$query = "UPDATE {$this->entity->getTable()} SET cep=:cep, endereco=:endereco, numero=:numero, bairro=:bairro, cidade=:cidade, estado=:estado, ddd_celular=:ddd_celular, celular=:celular WHERE id_aluno=:id_aluno";
			
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':cep', $request->getKey('cep') );
			$stmt->bindValue(':endereco', $request->getKey('endereco') );
			$stmt->bindValue(':numero', $request->getKey('numero') );
			$stmt->bindValue(':bairro', $request->getKey('bairro') );
			$stmt->bindValue(':cidade', $request->getKey('cidade') );
			$stmt->bindValue(':estado', $request->getKey('estado') );
			$stmt->bindValue(':ddd_celular', $request->getKey('ddd') );
			$stmt->bindValue(':celular', $request->getKey('celular') );
			$stmt->bindValue(':id_aluno', $request->getKey('id_aluno') );
			
			if( $stmt->execute() )
				return true;
		}
		catch(Exception $e)
		{
			echo 'Erro ao atualizar os dados do aluno: '.$e->getMessage();
		}
	}
	
	public function deletar($id)
	{
		$query = "delete from {$this->entity->getTable()} where id=:id";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':id', $id);
		
		if($stmt->execute()) {
			return true;
		}
	}
	
}