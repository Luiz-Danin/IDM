<?php 
session_start();
require_once 'config.php';
require_once 'AlunoController.php';
require_once 'Request.php';

$alunoController = new AlunoController();
$alunoController->indexIDMCursos(new Request(), $conexao);

if (isset($_SESSION['id_aluno']))
{
	
	$dados = $alunoController->estudarAlunoAVA($conexao, $_SESSION['id_aluno']);
	
	$IDIOMAS = array(
			'nome' => 'IDM CURSOS',
			'chave' => 'WVZkU2RGa3pWbmxqTWpsNg',
			'token' => 'e5ed5459954a9a725034d4936d99b76a'
	);



$send = base64_encode(json_encode(array('instituicao' => $IDIOMAS,'dados'=> $dados)));

$estudar = '<a id="idEstudar" href="#"	class="btn-primary">Estudar </a>';

$situacao5 = ' <form id="formEstudar" target="_blank" action="https://www.onlist.com.br/ava/aluno/idiomas/estudar/" method="post" >'
		. '<input type="hidden" name="request" value="'.$send.'">'.'<input id="idEstudar" type="submit" value="Estudar" class="btn-primary"></form>';
}
?>
<!DOCTYPE html>
<html>
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102414743-1', 'auto');
  ga('send', 'pageview');

</script>



<!-- /.website title -->
<title>IDM Cursos - Cursos de Inglês Básico</title>
<meta name="viewport"	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- CSS Files -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/font-awesome.min.min.css" rel="stylesheet">
<link href="fonts/icon-7-stroke/css/pe-icon-7-stroke.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.min.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">

<!-- Colors -->
<link href="css/css-index.min.css" rel="stylesheet" media="screen">

<?php header('Content-type: text/html; charset=utf-8'); ?>

<!-- Google Fonts -->
<link href="css/fonts.googleapis.lato.min.css" rel="stylesheet">
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1024701292;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "F6CLCO2nj3MQ7ObO6AM";
var google_remarketing_only = false;
/* ]]> */
</script>



</head>
<body data-spy="scroll" data-target="#navbar-scroll">

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Olá Fulano da Silva</h4>
      </div>
      <div class="modal-body">
        <h3>Parabéns, ao se cadastrar você ganhou uma matricula grátis válida por um mês</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        
      </div>
    </div>
  </div>
</div>

	<div id="fb-root"></div>
	<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];	
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9&appId=1550297434987933";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
	<!-- /.preloader -->
	<div id="preloader"></div>
	<div id="top"></div>
	<div class="navbar">
		<div class="container">
			<div class="login-header">
<?php 
if ( isset( $_SESSION['nome']) )
{
	$nomeUsuario = explode(' ', $_SESSION['nome']);
	$nomeAluno = "<strong>".$nomeUsuario[0]."</strong>".', você está no IDM Curso'.'<a href="logout.php">, sair </a>';
}
else 
{
	$nomeAluno = "Olá,<strong> Você está no IDM Curso</strong>";
}
echo $nomeAluno;
?>
			</div>
		</div>
	</div>
	<!-- /.parallax full screen background image -->
	<div class="fullscreen landing parallax"
		style="background-image: url('images/bg.jpg');" data-img-width="2000"
		data-img-height="1333" data-diff="100">

		<div class="overlay">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-7">

						<!-- /.logo -->
						<div class="logo wow fadeInDown">
							<a href="../"><img src="images/logo.png" alt="logo"></a>
						</div>

						<!-- /.main title -->
						<h1 class="wow fadeInLeft">Inglês para o dia a dia</h1>

						<!-- /.header paragraph -->
						<div class="landing-text wow fadeInUp">
							<p>
								Acesso 24 horas por dia ao conteúdo com temas voltados para o
								cotidiano. <br> Você acessa pelo computador, tablet ou celular. Apenas R$14,90 por mês!
							</p>
						</div>

						<!-- /.header button -->
						<div id="btn-logado" class="head-btn wow fadeInLeft">
							<a id="idMatricular" href="#" class="btn-primary" value="<?php echo ( isset($_SESSION['id_aluno_hash']) ) ? $_SESSION['id_aluno_hash'] : '';?>" >Gerar Fatura</a> 
							<!-- <a id="idEstudar" href="#"	class="btn-primary" value="" >Estudar </a> --> 
							<?php echo $situacao5;?>
							<!-- <a id="idMatricula" href="#" class="btn-primary">Matricular</a> -->
						</div>
					</div>

					<!-- /.signup form -->
					<div class="col-md-5">
						<div id="enviarLogin">
							<div class="signup-header wow fadeInUp">
								<h3 class="form-title text-center">Login/Matricula</h3>
								<div class="form-header" id="login-form">
									<input type="hidden" name="u" value="503bdae81fde8612ff4944435">
									<input type="hidden" name="id" value="bfdba52708">
									<div class="form-group">
										<input class="form-control input-lg" name="email" id="email"
											type="email" placeholder="Seu E-mail" required>
									</div>

									<div class="form-group last">
									<span class="erro" id="divMayus" style="visibility:hidden; margin-bottom: -23px;">
					  				Capslock esta ativo
			        		</span>
										<input id="loginCadastro" type="submit" class="btn-secondary btn-block btn-lg"
											value="Continuar">
											<p></p>
										<!-- <div class="fb-login-button" data-max-rows="1"
											data-size="small" data-button-type="login_with"
											data-show-faces="false" data-auto-logout-link="false"
											data-use-continue-as="false"></div> -->
									</div>
									<!-- <p class="privacy text-center">Não compartilharemos o seu
										e-mail.</p> -->
								</div>
							</div>
						</div>

						</div>

						<div id="form"></div>
						<div id="formLogin"></div>
					</div>
				</div>
			</div>
		</div>
		

		<!-- NAVIGATION -->
		<div id="menu">
			<nav class="navbar-wrapper navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-backyard">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand site-name" href="#top">IDM Curso de Idiomas</a>
					</div>

					<div id="navbar-scroll"
						class="collapse navbar-collapse navbar-backyard navbar-right">
						<ul class="nav navbar-nav">
							<li><a href="#sobre">Sobre</a></li>
							<li><a href="#beneficios">Benefícios</a></li>
							<li><a href="#motivo">Por que fazer inglês?</a></li>
							<li><a href="#modulo">O que vou aprender?</a></li>
							<li><a href="#contato">Contato</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>

		<!-- /.Sobre -->
		<div id="sobre">
			<div class="container">
				<div class="row">

					<!-- /.intro image -->
					<div class="col-md-6 sobre-pic wow slideInLeft">
						<img src="images/intro-image.jpg" alt="image"
							class="img-responsive">
					</div>

					<!-- /.Conteúdo Sobre -->
					<div class="col-md-6 wow slideInRight">
						<h2>Aprenda inglês com o curso básico da IDM Cursos!</h2>
						<p>Falar um segundo idioma é fundamental, seja para ajudá-lo em
							sua carreira profissional, construir relacionamentos com novas
							pessoas ou para entrar em cursos de pós graduação.</p>
					</div>
				</div>
			</div>
		</div>

		<!-- /.Benefícios -->
		<div id="beneficios">
			<div class="container">
				<div class="row">
					<div
						class="col-md-10 col-md-offset-1 col-sm-12 text-center beneficios-title">

						<!-- /.Benefícios Título -->
						<h2>Estude Sem Sair de Casa!</h2>
						<p>Tire dúvidas por áudio, texto e exercícios de acompanhamento. E
							o melhor: TUDO PELA INTERNET!</p>
					</div>
				</div>
				<div class="row row-feat">
					<div class="col-md-4 text-center">

						<!-- /.Benefícios Imagem-->
						<div class="beneficios-img">
							<img src="images/feature-image.jpg" alt="image"
								class="img-responsive wow fadeInLeft">
						</div>
					</div>

					<div class="col-md-8">

						<!-- /.Benefício 1 -->
						<div class="col-sm-6 feat-list">
							<i class="pe-7s-notebook pe-5x pe-va wow fadeInUp"></i>
							<div class="inner">
								<h4>Turmas Iniciadas Toda Semana</h4>
								<p>Tire suas dúvidas por áudio, texto. Dispomos de
									turmas semanais!</p>
							</div>
						</div>

						<!-- /.Benefício 2 -->
						<div class="col-sm-6 feat-list">
							<i class="pe-7s-cash pe-5x pe-va wow fadeInUp"
								data-wow-delay="0.2s"></i>
							<div class="inner">
								<h4>Pagamento Facilitado</h4>
								<p>Pagamento facilitado por cartão de crédito, débito em conta
									ou débito bancário.</p>
							</div>
						</div>

						<!-- /.Benefício 3 -->
						<div class="col-sm-6 feat-list">
							<i class="pe-7s-cart pe-5x pe-va wow fadeInUp"
								data-wow-delay="0.4s"></i>
							<div class="inner">
								<h4>Tudo Pela Internet</h4>
								<p>Acesso 24 horas por dia ao conteúdo. Entre no ambiente de
									aprendizagem diretamente pelo computador, tablet ou celular.</p>
							</div>
						</div>

						<!-- /.Benefício 4 -->
						<div class="col-sm-6 feat-list">
							<i class="pe-7s-users pe-5x pe-va wow fadeInUp"
								data-wow-delay="0.6s"></i>
							<div class="inner">
								<h4>Acesse Fácil e Rápido</h4>
								<p>Você informará suas informações de login e senha no cadastro e poderá acessar o seu curso imediatamente.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- /.Sessão Benefício 2 -->
		<div id="beneficios-2">
			<div class="container">
				<div class="row">

					<!-- /.Benefício Conteúdo -->
					<div class="col-md-6 wow fadeInLeft">
						<h2>Professores acompanhando seu desenvolvimento!</h2>
						<p>
							Com foco em temas do dia a dia, os professores acompanharão seu
							desempenho através de metodologias <!-- span class="highlight"> metodologias</span> -->
							diferenciadas. Serão utilizados também os recursos de áudio,
							texto e exercícios para fixar o conteúdo de forma rápida e
							prática.
						</p>
					</div>

					<!-- /.Benefício Imagem -->
					<div class="col-md-6 beneficios-2-pic wow fadeInRight">
						<img src="images/feature2-image.jpg" alt="macbook"
							class="img-responsive">
					</div>
				</div>

			</div>
		</div>

		<!-- /.Motivo -->
		<div id="motivo">
			<div class="action fullscreen parallax"
				style="background-image: url('images/bg.jpg');"
				data-img-width="2000" data-img-height="1333" data-diff="100">
				<div class="overlay">
					<div class="container">
						<div class="col-md-8 col-md-offset-2 col-sm-12 text-center">

							<!-- /.Motivo Título -->
							<h2 class="wow fadeInRight">Por que fazer este curso?</h2>
							<p class="motivo-text wow fadeInLeft">Aprender inglês já deixou
								de ser um diferencial curricular. Aprender inglês é obrigatório
								para alcançar o sucesso em um mercado de trabalho cada vez mais
								competitivo.</p>

							<!-- /.Motivo Botão -->
							<div class="motivo-cta wow fadeInLeft">
								<a href="#contato" class="btn-secondary">Quero Saber Mais!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- /.pricing section -->
		<div id="modulo">
			<div class="container">
				<div class="text-center">

					<!-- /.pricing title -->
					<h2 class="wow fadeInLeft">Aproveite, o Certificado é Grátis!</h2>
                                            <p>*Após realizar o pagamento você já ganha automaticamente o certificado.</p>
					<p>24 Módulos, 5 aulas por semana, 40 minutos cada aula.</p>
					<div class="title-line wow fadeInRight"></div>
				</div>
				<div class="row modulo-option">

					<!-- /.módulos -->
					<div class="col-sm-3">
						<div class="price-box wow fadeInUp" data-wow-delay="0.2s">
							<div class="price-heading text-center"></div>

							<!-- /.Informações sobre os módulos -->
							<ul class="price-feature text-center">
								<li><strong>1ª Módulo - </strong> Conhecendo um novo amigo no
									ambiente escolar</li>
								<li><strong>2ª Módulo - </strong> Começando em um novo trabalho</li>
								<li><strong>3ª Módulo - </strong> Iniciando o primeiro dia na
									escola</li>
								<li><strong>4ª Módulo - </strong> Visitando parentes distantes</li>
								<li><strong>5ª Módulo - </strong> Escrevendo um e-mail para um
									amigo distante</li>
								<li><strong>6ª Módulo - </strong> Informando um novo endereço de
									residência</li>
								<li><strong>7ª Módulo - </strong> American Foods  -  Comida Americana </li>
								<li><strong>8ª Módulo - </strong> Health – Saúde</li>
							</ul>

						</div>
					</div>

					<!-- /.Módulos -->
					<div class="col-sm-3">
						<div class="price-box wow fadeInUp" data-wow-delay="0.4s">
							<div class="price-heading text-center"></div>

							<!-- /.Informações sobre os módulos-->
							<ul class="price-feature text-center">
								<li><strong>9ª Módulo - </strong> Nature – Natureza</li>
								<li><strong>10ª Módulo - </strong> Safety  -  segurança</li>
								<li><strong>11ª Módulo - </strong> Voting  -  votação</li>
								<li><strong>12ª Módulo - </strong>Dating  -  Namoro</li>
								<li><strong>13ª Módulo - </strong>Bank  -  Banco</li>
								<li><strong>14ª Módulo - </strong>Taking the bus  -  Pegando o ônibus</li>
								<li><strong>15ª Módulo - </strong>House  -  Casa</li>
								<li><strong>16ª Módulo - </strong>Driving  -  Dirigindo</li>
							</ul>

						</div>
					</div>

					<!-- /.Módulos -->
					<div class="col-sm-3">
						<div class="price-box wow fadeInUp" data-wow-delay="0.6s">
							<div class="price-heading text-center"></div>

							<!-- /.Informações sobre os módulos -->
							<ul class="price-feature text-center">
								<li><strong>17ª Módulo - </strong>Shopping  -  Comprando</li>
								<li><strong>18ª Módulo - </strong>Sports  -  Esportes</li>
								<li><strong>19ª Módulo - </strong>Social Happenings  -  Acontecimentos sociais</li>
								<li><strong>20ª Módulo - </strong>Daily Life  -  vida cotidiana</li>
								<li><strong>21ª Módulo - </strong>Daily Life Part II  -  vida cotidiana</li>
								<li><strong>22ª Módulo - </strong>Entertainment  -  Entretenimento</li>
								<li><strong>23ª Módulo - </strong>Transportation  -  Transporte</li>
								<li><strong>24ª Módulo - </strong>Eating Out   -  Comer fora</li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- /.Parceiros -->
		<div id="parceiros">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
					<a href="http://www.abed.org.br/site/pt/associados/consulta_associados_abed/?busca_rapida=cidade+aprendizagem" target="_blank"><img alt="client" src="images/abed.png" class="wow fadeInUp"></a>
					<a href="https://www.alphassl.com/" target="_blank"><img alt="client" src="images/alpha.png" class="wow fadeInUp" data-wow-delay="0.2s"></a>
					<a target="_blank" href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=pt-BR#url=idmcursos.com.br"><img alt="client" src="images/google.jpg" class="wow fadeInUp" data-wow-delay="0.4s"></a>
					<a href="https://www.tray.com.br/"  target="_blank"><img alt="client" src="images/tray.png" class="wow fadeInUp" data-wow-delay="0.6s"></a>
					<a href="http://lattes.cnpq.br/" target="_blank"><img alt="client" src="images/lattes.png" class="wow fadeInUp" data-wow-delay="0.6s"></a>
					<a href="https://www.cidadeaprendizagem.com.br/newsite/index.php" target="_blank"><img alt="client" src="images/cidade-aprendizagem.png" class="wow fadeInUp" data-wow-delay="0.6s"></a>
				</div>
				</div>
			</div>
		</div>

		<!-- /.Contato -->
		<div id="contato">
			<div class="contato fullscreen parallax"
				style="background-image: url('images/bg.jpg');"
				data-img-width="2000" data-img-height="1334" data-diff="100">
				<div class="overlay">
					<div class="container">
						<div class="row contato-row">

							<!-- /.Endereço e Contato -->
							<div class="col-sm-5 contato-left wow fadeInUp">
								<h2>
									<span class="highlight">Dúvidas?</span>
								</h2>
								<ul class="ul-address">
									<!--<li><i class="pe-7s-phone"></i>4004-0435 Ramal:8229</br>-->
										<!--(whatsapp) 91 983490030</li>-->
									<li><i class="pe-7s-mail"></i><a
										href="mailto:info@yoursite.com">atendimento@idmcursos.com.br</a></li>
									<li><i class="pe-7s-look"></i><a href="../">www.idmcursos.com.br</a></li>
								</ul>

							</div>

							<!-- /.Formulário de Contato -->
							<div class="col-sm-7 contato-right">
								<h2 class="form-title text-center">Contato</h2>
								<form method="POST" action="contactengine.php" id="contato-form" class="form-horizontal">
									<div class="form-group">
										<input type="text" name="nameContato" id="nameContato"  pattern="[A-zÀ-ž ]+"
											class="form-control wow fadeInUp" placeholder="Nome" required />
									</div>
									<div class="form-group">
										<input type="email" name="emailContato" id="emailContato"
											class="form-control wow fadeInUp" placeholder="E-mail"
											required />
									</div>
									<div class="form-group row">
										<div class="form-group col-xs-6 col-sm-4 col-md-3 col-lg-3">
											<input class="form-control input-lg phone wow fadeInUp" name="dddContato"
											id="dddContato" type="text" placeholder="DDD" maxlength="2" pattern="\d{2}" required title="Somente números, apenas 2 digitos">
										</div>
										<div id="tel-fixo" class="form-group col-xs-6 col-sm-8 col-md-9 col-lg-9">
											<input class="form-control input-lg phone wow fadeInUp" name="celularContato"
											id="celularContato" type="text" placeholder="Celular ou Telefone Fixo " maxlength="9" pattern="\d{8}|\d{9}" required title="Somente números, 8 ou 9 dígitos">
										</div>
									</div>
									<div class="form-group">
										<textarea name="messageContato" rows="20" cols="20" id="messageContato"
											class="form-control input-message wow fadeInUp"
											placeholder="Mensagem" required></textarea>
									</div>
									<div class="form-group">
										<input id="submitContato" type="submit" name="submit" value="Enviar"
											class="btn btn-success wow fadeInUp" />
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- /.Rodapé -->
		<footer id="footer">
							<div id="spin"></div>
			<div class="container">
				<div class="col-sm-4 col-sm-offset-4">
					<!-- /.social links -->
					<div class="social text-center">
						<ul>
							<!-- <li><a class="wow fadeInUp" href="https://twitter.com/"><i
									class="fa fa-twitter"></i></a></li> -->
							<li><a class="wow fadeInUp" href="https://www.facebook.com/idmcursos"
								data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					<div class="text-center wow fadeInUp" style="font-size: 14px;">Copyright
						IDM Cursos 2017</div>
					<a href="#" class="scrollToTop"><i class="pe-7s-up-arrow pe-va"></i></a>
				</div>
			</div>
		</footer>

		<!-- /.Arquivos JavaScript -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/custom.min.js"></script>
		<script src="js/jquery.sticky.min.js"></script>
		<script src="js/wow.min.min.js"></script>
		<script src="js/owl.carousel.min.min.js"></script>
		<script src="js/bootstrap-select.min.js"></script>
		<script>
		new WOW().init();
	</script>
		<script>
	$( document ).ready(function() {

		$("#btn-logado").hide();

	function validateEmail(email) { 
		    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    return re.test(email);
		}
	
	$("#loginCadastro").click(function() {

			var email = $("#email").val();

			function validate(email){
				  if (validateEmail(email)) {
					    return true;
				  } else {
					  return false;
				  }
				}

			if(email){
				
				if( validate(email)){

				$.getJSON('fragment_is_email_valid.php?login='+email,function(data){

					if( (data.toString()==="false") )
					{
						$.ajax(
			 					{
			 					  type: "POST",
			 					  url: 'fragmentFormPassword.php',
			 					  data: { email:email},
			 					 success: function (data) {
			 							$("#enviarLogin").hide();
										$("#form").html(data);
									        },
								        error: function() {
								           console.log('error');
								        }
			 					}
			 				   );
					}
					else if(data.toString()==="null")
					{
						$.ajax(
			 					{
			 					  type: "GET",
			 					  url: 'fragmentFormCadastro.php',
			 					  data: { email:email},
			 					 success: function (data) {
			 							$("#enviarLogin").hide();
										$("#form").html(data);
									        },
								        error: function() {
								           console.log('error');
								        }
			 					}
			 				   );
						   
					}
					else if(data.toString()==="true")
					{
						$.ajax(
			 					{
			 					  type: "GET",
			 					  url: 'fragmentFormLogin.php',
			 					  data: { email:email},
			 					 success: function (data) {
			 							$("#enviarLogin").hide();
										$("#form").html(data);
									        },
								        error: function() {
								           console.log('error');
								        }
			 					}
			 				   );
					}

		 			});

				}
				else
				{
					alert("Atenção: e-mail "+email+ " não é valido");
				} 
			}
			else if(!email)	{
				alert('Atenção: O campo E-mail não pode ser vazio');
			}
		});
	});
</script>

<script type="text/javascript">
$( document ).ready(function() {

	//$('#largeModal').modal('show');
	
	$("#idMatricular").click(function() {

		var hash = $("#idMatricular").attr("value");

		$.ajax(
				{
				  type: "POST",
				  url: 'fragmentFormMatricular.php',
				  data: {hash:hash},
				  dataType: "html",
				  success: function (data) {
					$("#form").html(data);
				        },
			        error: function() {
			           console.log('error');
			        }
				});
		
	});
});
</script>
<script type="text/javascript">
$( document ).ready(function() {
	$("#submitContatox").click(function() {

		 var nameContato = $('#nameContato').val();
		 var emailContato = $('#emailContato').val();
		 var emailDestino = $('#emailDestino').val();
		 var dddContato = $('#dddContato').val();
		 var celularContato = $('#celularContato').val();
		 var messageContato = $('#messageContato').val();

		 

		function validateEmailContato(email)
		 { 
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		 }

		function validaNomeContato(nameContato)
		{				    
			if(nameContato.length<3 || !nameContato.match(/\w/) || !( isNaN(nameContato) ) )
			{
				alert("Campo Nome Inválido!");
				return false;
			}
		}

		function validaEmailContato(emailContato)
		{
			if( !validateEmailContato(emailContato) || !emailContato.match(/\w/) )
			{
				alert("Campo E-mail Inválido!");
				return false;
			}
		}

		function validaDDDContato(dddContato)
		{
			if(!dddContato.match(/\w/) ||  dddContato.length!=2 || isNaN(dddContato) )
			{
				alert("Campo DDD Inválido!");
				return false;
			}
		}

		function validaCelularContato(celularContato)
		{
			if( !celularContato.match(/\w/) || isNaN(celularContato) )
			{
				alert("Campo Celular Inválido!");
				return false;
			}
		}

		function validaMSG(messageContato)
		{
			if( !messageContato.match(/\w/) )
			{
				alert("Campo Mensagem Inválido!");
				return false;
			}
		}

		if( (validaNomeContato(nameContato)!=false) && (validaEmailContato(emailContato)!=false) && (validaDDDContato(dddContato)!=false) && (validaCelularContato(celularContato)!=false) && (validaMSG(messageContato)!=false) )
		{

				var opts = {
						  lines: 12,
						  length: 56 
						, width: 12 
						, radius: 50 
						, scale: 0.5 
						, corners: 1 
						, color: '#000' 
						, opacity: 0 
						, rotate: 56
						, direction: 1 
						, speed: 1.1 
						, trail: 100 
						, fps: 20 
						, zIndex: 2e9 
						, className: 'spinner' 
						, top: '50%' 
						, left: '50%' 
						, shadow: false,
						  hwaccel: false, 
						  position: 'absolute'
						};
		
				  var el = $('<div>#spin').appendTo('#contato-form').spin(opts);
		
				$.ajax(
							{
							  type: "POST",
							  url: 'contactengine.php',
							  data: { nameContato:nameContato, emailContato:emailContato ,emailDestino:emailDestino, dddContato:dddContato ,celularContato:celularContato, messageContato:messageContato},
							  dataType: "html",
							  success: function (data) {
		
								  window.stop();
								  alert("Mensagem Enviada com Sucesso!");
								  $('#nameContato').val("");
								  $('#emailContato').val("");
								  $('#dddContato').val("");
								  $('#celularContato').val("");
								  $('#messageContato').val("");
								  new WOW().init();
							        },
						        error: function() {
						           console.log('error');
						           
						        },
						        complete: function() {
						        	setTimeout(function() { 
								        el.spin(false).remove()
								    }, 500);
						        }
						});
		}
	});
});
</script>
<script type="text/javascript">
$( document ).ready(function() {

		document.getElementById("email")
	    .addEventListener("keyup", function(event) {
	    event.preventDefault();
	    if (event.keyCode == 13) {
	        document.getElementById("loginCadastro").click();
	    }
	});
	
});
</script>
</body>
</html>
<?php 
	if( isset($_SESSION['id_aluno']))
	{
		//$notPagamentoAluno = $alunoController->checkPagamento($conexao, $_SESSION['id_aluno']);
		
		$id_curso = 635;
		$isAlunoEstudar = $alunoController->checkEstudar($conexao, $_SESSION['id_aluno'], $id_curso);
		
		if ( $isAlunoEstudar==false )
		{
			echo '<script>$( document ).ready(function() { $("#idEstudar").hide(); });</script>';
		}
		else 
		{
			echo '<script>$( document ).ready(function() { $("#idEstudar").show(); });</script>';
		}
		
	}

	if ( isset($_SESSION['nome']) )
	{
		echo '<script>$( document ).ready(function() {  $(".col-md-5").hide(); $("#btn-logado").show(); });</script>';
	}
	else 
	{
		
	}
?>