<?php header('Content-type: text/html; charset=utf-8'); ?>
<?php
require_once 'Request.php';
require_once 'config.php';
require_once 'EntidadeInterface.php';
require_once 'Aluno.php';
require_once 'ServiceDB.php';

$aluno = new Aluno();
$serviceDB = new ServiceDb($conexao, $aluno);

$confirmacao = $serviceDB->updateDadosAluno(new Request());
if ($confirmacao==true)
{
	echo $html='
		<div class="col-md-5" id="tela">
			<div class="signup-header wow fadeInUp">	
				<div class="col-md-12" id="pedido">
					<br><br>
						<h2 class="wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">Investimento</h2>
						<hr>
						<h4><p id="quantidadeParcela" class="motivo-text wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;"><strong>É hora de decolar sua carreira, seja aluno do IDM Cursos. Faça um investimento mensal e estude 24h/dia onde e quando quiser.</strong></p></h4>
						<h4><p class="motivo-text wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;"><strong>Aproveite a nova oferta com o valor da Mensalidade a R$14,90.</strong></p></h4>
					    <div class="form-group">
							<h3 class="form-title text-center">Escolha a Forma de pagamento</h3>
					        <div class="form-title text-center">
					            <span data-toggle="collapse" data-parent="#accordion" href="#boleto" aria-expanded="true" aria-controls="collapseOne" class="btn btn-default btn-sm">
					                <i class="glyphicon glyphicon-barcode"></i>Boleto
					            </span>
					            <span class="btn btn-warning btn-sm" role="button" data-toggle="collapse" data-parent="#accordion" href="#cartao" aria-expanded="false" aria-controls="collapseTwo">
					                <i class="glyphicon glyphicon-credit-card"></i>Cartão
					            </span>
					            <span class="btn btn-info btn-sm" role="button" data-toggle="collapse" data-parent="#accordion" href="#debito" aria-expanded="false" aria-controls="collapseTwo">
					                <i class="glyphicon glyphicon-credit-card"></i>Débito
					            </span>
					        </div>
					    </div>
					</div>
					<div id="respostaFormaPagamento"></div>
				</div>
			</div>';
}
?>
<script>
</script>
<script>
$( document ).ready(function(){
	$(".btn-default").click(function() {

		
		$.ajax(
					{
					  type: "POST",
					  url: 'fragmentBoleto.php',
					 success: function (data) {
						$("#respostaFormaPagamento").html(data);
		 				$("p#quantidadeParcela").text("É hora de decolar sua carreira, seja aluno do IDM Cursos. Faça um investimento mensal e estude 24h/dia onde e quando quiser.");
					        },
				        error: function() {
				           console.log('error');
				        }
					}
			   );
	});

	$(".btn-warning").click(function() {
		
		$.ajax(
				{
				  type: "POST",
				  url: 'fragmentCartao.php',
				 success: function (data) {
					$("#respostaFormaPagamento").html(data);
				        },
			        error: function() {
			           console.log('error');
			        }
				}
		   );
		
	});

	$(".btn-info").click(function() {
		$.ajax(
				{
				  type: "POST",
				  url: 'fragmentDebito.php',
				 success: function (data) {
					$("#respostaFormaPagamento").html(data);
					$("p#quantidadeParcela").text("É hora de decolar sua carreira, seja aluno do IDM Cursos. Faça um investimento mensal e estude 24h/dia onde e quando quiser.");
				        },
			        error: function() {
			           console.log('error');
			        }
				}
		   );
		
		
	});
});
</script>
<script>
$( document ).ready(function(){
	
});
</script>