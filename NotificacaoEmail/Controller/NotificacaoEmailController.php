<?php
require_once dirname(__DIR__).'/Model/NotificacaoEmail.php';
require_once '../../libraries/Phpmailer_library.php';
require_once '../../libraries/Servidor_smtp.php';

class NotificacaoEmailController
{
    
    public function notifica_credito($conexao)
    {
        set_time_limit(0);
        $notifica_alunos = new NotificacaoEmail($conexao);
        $alunos = $notifica_alunos->notifica_credito();
        var_dump($alunos);
        die('credito');
        if(count($alunos)>0 && !empty($alunos))
        {
            
            $array_novo = array();
            $email=' ';
            $curso='';
            
            foreach ($alunos as $aluno)
            {
                if($email!=$aluno['email'])
                {
                    $array_novo[] = $aluno; 
                }
                else
                {
                    if($curso != $aluno['id_produto'])
                    {
                        $array_novo[] = $aluno;
                    }
                }
                
                $email = $aluno['email'];
                $curso = $aluno['id_produto'];
            }
            
            $c = 0;
            foreach ($array_novo as $aluno)
            {
                $assunto = $this->retirar_acentos('Seu pagamento encontra-se pendente!');
                $mensagem = $this->alterar_caracteres("A sua fatura gerada, com cartão de crédito, para a ".$aluno['descricao']." encontra-se pendente há 1 dia. Realize o pagamento, aprenda uma novo idioma estudando onde quando quiser.<br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
                $trim_nome = trim($aluno['nome']);
                if( !empty( $trim_nome ) )
                {
                    $mensagem = $this->alterar_caracteres($aluno['nome'].", sua fatura gerada, com cartão de crédito, para a ".$aluno['descricao']." encontra-se pendente há 1 dia. Realize o pagamento, aprenda uma novo idioma estudando onde quando quiser.<br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
                }
                
                $html = $this->template_html($assunto, utf8_encode($mensagem) , $aluno['email']);
                
                $aluno_email = trim($aluno['email']);
                
                if( !empty( $aluno_email ) )
                {
                    $enviar = $this->send_mail($aluno['email'], $assunto, $html);
                }
                
                if ($enviar == true)
                {
                    $array['Enviado'][$c] = $aluno['email'];
                } 

                $c++;
            }
            
            $assunto = $this->retirar_acentos("Relatório: Notificações IDM Cursos - Faturas Pendentes por Cartão de Crédito");
            $texto = "<b>Enviados:</b> ".count($array['Enviado'])."<br><b>Data e Hora:</b> ". date("d/m/Y H:i:s");
            $this->relatorio($assunto, $texto);
        }
        else
        {
            echo 'Não há dados para Faturas pendentes por Cartão de Credito';
        }
    }

    public function notifica_debito($conexao)
    {
        set_time_limit(0);
        $notifica_alunos = new NotificacaoEmail($conexao);
        $alunos = $notifica_alunos->notifica_debito();
        var_dump($alunos);
        die('debito');
        
        if(count($alunos)>0 && !empty($alunos))
        {
            $array_novo = array();
            $email=' ';
            $curso='';
            
            foreach ($alunos as $aluno)
            {
                if($email!=$aluno['email'])
                {
                    $array_novo[] = $aluno; 
                }
                else
                {
                    if($curso != $aluno['id_produto'])
                    {
                        $array_novo[] = $aluno;
                    }
                }
                
                $email = $aluno['email'];
                $curso = $aluno['id_produto'];
            }
            
            $c = 0;
            foreach ($array_novo as $aluno)
            {
                $assunto = $this->retirar_acentos('Seu pagamento encontra-se pendente!');
                $mensagem = $this->alterar_caracteres("A sua fatura gerada, com opção de pagamento débito, para a ".$aluno['descricao']." encontra-se pendente há 1 dia. Realize o pagamento acessando novamente o IDM Cursos.<br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
            
                $aluno_nome = trim($aluno['nome']);
                if( !empty( $aluno_nome ) )
                {
                    $mensagem = $this->alterar_caracteres($aluno['nome'].", sua fatura gerada, com opção de pagamento débito, para a ".$aluno['descricao']." encontra-se pendente há 1 dia. Realize o pagamento acessando novamente o IDM Cursos.<br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
                }
                
                $html = $this->template_html($assunto, utf8_encode($mensagem) , $aluno['email']);
                
                $aluno_email = trim($aluno['email']);
                if( !empty( $aluno_email ) )
                {
                    $enviar = $this->send_mail($aluno['email'], $assunto, $html);
                }
                
                if ($enviar == true)
                {
                    $array['Enviado'][$c] = $aluno['email'];
                }

                $c++;
            }
            
            $assunto = $this->retirar_acentos("Relatório: Notificações IDM Cursos - Faturas Pendentes por Débito");
            $texto = "<b>Enviados:</b> ".count($array['Enviado'])."<br><b>Data e Hora:</b> ". date("d/m/Y H:i:s");
            $this->relatorio($assunto, $texto);
        }
        else
        {
            echo 'Não há dados para Faturas pedentes por Débito';
        }
    }


    public function notifica_boleto($conexao)
    {
        set_time_limit(0);
        $notifica_alunos = new NotificacaoEmail($conexao);
        $alunos = $notifica_alunos->notifica_boleto();
        var_dump($alunos);
        die('boleto');
        
        if(count($alunos)>0 && !empty($alunos))
        {
            $array_novo = array();
            $email=' ';
            $curso='';
            
            foreach ($alunos as $aluno)
            {
                if($email!=$aluno['email'])
                {
                    $array_novo[] = $aluno; 
                }
                else
                {
                    if($curso != $aluno['id_produto'])
                    {
                        $array_novo[] = $aluno;
                    }
                }
                
                $email = $aluno['email'];
                $curso = $aluno['id_produto'];
            }
            
            $c = 0;
            foreach ($array_novo as $aluno)
            {
                $assunto = $this->retirar_acentos('Seu pagamento encontra-se pendente!');
                $mensagem = $this->alterar_caracteres("A sua fatura gerada para a ".$aluno['descricao']." encontra-se pendente há 3 dias. Realize o pagamento clicando no botão abaixo.<br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
                //não descomentar em produção$aluno['cod_interme'] = '44c5d9e824170c2d18cca9554c2059e2';
                $response = $this->get_transacao_tray($aluno['cod_interme']);
                
                $response_url = (array)$response->data_response->transaction->payment->url_payment;
                $response_url = $response_url[0];
                
                $aluno_nome = trim($aluno['nome']);
                if( !empty( $aluno_nome ) )
                {
                    $mensagem = $this->alterar_caracteres($aluno['nome'].", sua fatura gerada para a ".$aluno['descricao']." encontra-se pendente há 3 dias. Realize o pagamento clicando no botão abaixo.<br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
                }
                
                $html = $this->template_html_pagamento($assunto, utf8_encode($mensagem) , $aluno['email'], $response_url);
                
                $aluno_email = trim($aluno['email']);
                if( !empty( $aluno_email ) )
                {
                    $enviar = $this->send_mail($aluno['email'], $assunto, $html);
                }
                
                if ($enviar == true)
                {
                    $array['Enviado'][$c] = $aluno['email'];
                } 

                $c++;
            }
            
            $assunto = $this->retirar_acentos("Relatório: Notificações IDM Cursos - Faturas Pendentes por Boleto");
            $texto = "<b>Enviados:</b> ".count($array['Enviado'])."<br><b>Data e Hora:</b> ". date("d/m/Y H:i:s");
            $this->relatorio($assunto, $texto);
        }
        else
        {
            echo 'Não ha dados para Faturas pendentes por Boleto';
        }
        
    }
    
    public function matricula_sem_geracao_fatura($conexao)
    {
        set_time_limit(0);
        $matriculas_alunos = new NotificacaoEmail($conexao);
        $alunos =  $matriculas_alunos->matricula_sem_geracao_fatura();
        var_dump($alunos);
        die('geracao fatura');
        
        if(count($alunos)>0 && !empty($alunos))
        {
            $c = 0;
            foreach ($alunos as $aluno)
            {
                $mensagem = $this->alterar_caracteres("Sua matricula no IDM Cursos  foi realizada com sucesso. Acesse o site e gere sua fatura. <br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
                
                $aluno_nome =  trim($aluno['nome']);
                if( !empty( $aluno_nome ))
                {
                    $mensagem = $this->alterar_caracteres($aluno['nome'].", a sua matricula no IDM Cursos  foi realizada com sucesso. Acesse o site e gere sua fatura. <br> ATENÇÃO! Caso já tenha realizado o pagamento, desconsidere este e-mail.");
                }
                
                $assunto = $this->retirar_acentos('Inscrição aprovada, gere sua fatura!');
                
                $html = $this->template_html($assunto, utf8_encode($mensagem) , $aluno['email']);
                
                $aluno_email = trim($aluno['email']);
                if( !empty( $aluno_email ) )
                {
                    $enviar = $this->send_mail($aluno['email'], $assunto, $html);
                }
                
                if ($enviar == true)
                {
                    $array['Enviado'][$c] = $aluno['email'];
                } 

                $c++;
            }
            
            $assunto = $this->retirar_acentos("Relatório: Notificações IDM Cursos - Matricula sem Geração de Fatura");
            $texto = "<b>Enviados:</b> ".count($array['Enviado'])."<br><b>Data e Hora:</b> ". date("d/m/Y H:i:s");
            $this->relatorio($assunto, $texto);
        }
        else
        {
            echo 'Não há dados para matriculas sem faturas';
        }
        
    }

    public function curso_liberado($conexao)
    {
        set_time_limit(0);
        $cursos_alunos = new NotificacaoEmail($conexao);
        $alunos =  $cursos_alunos->curso_liberado();
        var_dump($alunos);
        die('curso liberado');
        
        if(count($alunos)>0 && !empty($alunos))
        {
            
            $c = 0;
            foreach ($alunos as $aluno)
            {
                $curso = ($aluno['id_curso']==635)?'Inglês': ($aluno['id_curso']==636)? 'Espanhol':'';
                $mensagem = $this->alterar_caracteres("Informamos que está disponível para seu estudo o curso de ".$curso.". Fortaleça ainda mais seus estudos realizando mais cursos.");
                
                $aluno_nome = trim($aluno['nome']);
                if( !empty( $aluno_nome ) )
                {
                    $mensagem = $this->alterar_caracteres($aluno['nome'].", informamos que está disponível para seu estudo o curso de ".$curso.". Fortaleça ainda mais seus estudos realizando mais cursos.");
                }
                
                $assunto = $this->retirar_acentos('Acesso liberado!');
                
                $html = $this->template_html($assunto, utf8_encode($mensagem) , $aluno['email']);
                
                $aluno_email = trim($aluno['email']);
                if( !empty( $aluno_email ) )
                {
                    $enviar = $this->send_mail($aluno['email'], $assunto, $html);
                }
                
                if ($enviar == true)
                {
                    $array['Enviado'][$c] = $aluno['email'];
                } 

                $c++;
            }
            
            $assunto = $this->retirar_acentos("Relatório: Notificações IDM Cursos - Curso Liberado");
            $texto = "<b>Enviados:</b> ".count($array['Enviado'])."<br><b>Data e Hora:</b> ". date("d/m/Y H:i:s");
            $this->relatorio($assunto, $texto);
        }
        else
        {
            echo 'Não há dados para curso liberado';
        }
        
    }
    
    private function get_transacao_tray($codigo)
    {
        $params['token_transaction'] = $codigo;

        $params['token_account'] = 'a44677dac7cba94';   
        
        $urlPost = "https://api.sandbox.traycheckout.com.br/v2/transactions/get_by_token";
        
        ob_start();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlPost);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt ($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_exec($ch);

        /* XML de retorno */
        $resposta = simplexml_load_string(ob_get_contents());
        ob_end_clean();
        curl_close($ch);
        return $resposta;
    }

    private function send_mail($email, $assunto, $html)
    {
        set_time_limit(0);
        $smtp = new Servidor_smtp;
        $phpmailer = new Phpmailer_library();

        $new_smtp = $smtp->setData("1", "smtp.gmail.com", $this->get_client_ip_server(), "contato@notificacoes.online", "contato@notificacoes.online", "##C0n-m4il##", "IDM Cursos");
        return $phpmailer->send($smtp, $email, $assunto, $html, "IDM Cursos");
    }
    
    private function relatorio($assunto, $texto)
    {
        set_time_limit(0);
        $smtp = new Servidor_smtp;
        $phpmailer = new Phpmailer_library();
        
        $new_smtp = $smtp->setData("1", "smtp.gmail.com", $this->get_client_ip_server(), "contato@notificacoes.online", "contato@notificacoes.online", "##C0n-m4il##", "Cidade Aprendizagem");
        $enviar = $phpmailer->send($smtp, 'emarketing.cidade@hotmail.com', $assunto, $texto, "IDM Cursos");
    }


    private function get_client_ip_server()
    {
		$ipaddress = '';
		if (getenv ( 'HTTP_CLIENT_IP' ))
			$ipaddress = getenv ( 'HTTP_CLIENT_IP' );
		else if (getenv ( 'HTTP_X_FORWARDED_FOR' ))
			$ipaddress = getenv ( 'HTTP_X_FORWARDED_FOR' );
		else if (getenv ( 'HTTP_X_FORWARDED' ))
			$ipaddress = getenv ( 'HTTP_X_FORWARDED' );
		else if (getenv ( 'HTTP_FORWARDED_FOR' ))
			$ipaddress = getenv ( 'HTTP_FORWARDED_FOR' );
		else if (getenv ( 'HTTP_FORWARDED' ))
			$ipaddress = getenv ( 'HTTP_FORWARDED' );
		else if (getenv ( 'REMOTE_ADDR' ))
			$ipaddress = getenv ( 'REMOTE_ADDR' );
		else
			$ipaddress = 'UNKNOWN';
		
		return $ipaddress;
	}
        
    private function template_html_pagamento($assunto, $mensagem , $email, $url)
    {
        return '<!DOCTYPE html>
					<meta charset="UTF-8">
					<html lang="pt-br">
					<head>
					<meta charset="UTF-8">
					</head>
					<title></title>
					<style type="text/css">
					  body {
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    margin:0 !important;
					    width: 100% !important;
					    -webkit-text-size-adjust: 100% !important;
					    -ms-text-size-adjust: 100% !important;
					    -webkit-font-smoothing: antialiased !important;
					  }
					  .tableContent img {
					    border: 0 !important;
					    display: block !important;
					    outline: none !important;
					  }
					  a{
					    color:#382F2E;
					  }
					  table.social {
					    /*  padding:15px; */
					    background-color: #fff;
					
					  }
					  p.callout {
					    padding:15px;
					    background-color:#f6f6f6;
					    margin-bottom: 15px;
					  }
					  .callout a {
					    font-weight:bold;
					    color: #2BA6CB;
					  }
					  .social .soc-btn {
					    width: 100px;
					    padding: 1px 7px;
					    font-size:12px;
					    margin-bottom:5px;
					    text-decoration:none;
					    color: #FFF;font-weight:bold;
					    display:block;
					    text-align:center;
					  }
					  a.fb { background-color: #3B5998!important; }
					  a.tw { background-color: #1daced!important; }
					
					  p{
					    text-align:left;
					    color:#4f4f4f;
					    font-size:14px;
					    font-weight:normal;
					    line-height:19px;
					  }
					
					  a.link1{
					    color:#382F2E;
					  }
					  a.link2{
					    font-size:16px;
					    text-decoration:none;
					    color:#ffffff;
					  }
					
					  div,p,ul,h1{
					    margin:0;
					  }
					
					  .bgBody{
					    background: #ffffff;
					  }
					  .bgItem{
					    background: #ffffff;
					  }
					
					  @media only screen and (max-width:480px)
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					    td[class="left_pad"] 
					    {
					      padding-left:15px !important;
					      padding-right:15px !important;
					    }
					
					  }
					
					  @media only screen and (max-width:540px) 
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					  }
					
					</style>
					
					<body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
					  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center"  style="font-family:Helvetica, Arial,serif;">
					    <tbody>
					      <tr>
					        <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
					          <tbody>
					            <tr>
					              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tbody>
					                  <tr>
					                    <td valign="top" width="40">&nbsp;</td>
					                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tbody>
					                        <tr>
					                          <td height="-12" class="spechide"></td>
					
					                        </tr>
					                        <tr>
					                          <td class="movableContentContainer" valign="top">
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="35"></td>
					                                  </tr>
					                                  <tr>
					                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                      <tbody>
					                                        <tr>
					                                          <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                               <p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">IDM Cursos</span></strong> </p>
																	<p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">'.$assunto.'</span></strong> </p>
					                                              </div>
					                                            </div></td>
					                                          </tr>
					                                        </tbody>
					                                      </table>
					                                    </td>
					                                  </tr>
					                                </tbody>
					                              </table>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					                                <tr>
					                                  <td align="left">
					                                  <br><br>
					                                    <div class="contentEditableContainer contentTextEditable">
					                                      <div class="contentEditable" align="center">
					                                        <p>
					                                          <p class="callout">'.$mensagem.'</p>
					                                        </p>
					                                      </div>
					                                    </div>
					                                  </td>
					                                </tr>
					
					                                <tr><td height="15"></td></tr>
					
					                                <tr>
					                                  <td align="center" >
					                                    <table>
					                                      <tr>
					                                        <td align="center" width="200px" bgcolor="#1A54BA" style="background:#005b95; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;">
					                                          <div class="contentEditableContainer contentTextEditable">
					                                            <div class="contentEditable" align="center">
					                                              <a target="_blank" href="'.$url.'" class="link2" style="color:#ffffff;"><font color="#fff">Acesse o Boleto</font></a>
					                                            </div>
					                                          </div>
					                                        </td>
					                                      </tr>
					                                    </table>
					                                  </td>
					                                </tr>
					                                <tr><td height="10"></td></tr>
					                              </table>
					                              <br>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="15">
					                                    </tr>
					                                    <tr>
					                                      <td  style="border-bottom:1px solid #DDDDDD;"></td>
					                                    </tr>
					                                    <tr><td height="15"></td></tr>
					                                    <tr>
					                                      <td>
					                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                        <tbody>
					                                          <tr>
					                                            <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                                <p  style="text-align:left;color:#4f4f4f;font-size:12px;font-weight:normal;line-height:20px;">
					                                                  <span style="font-weight:bold;">Copyright @ IDM Cursos '.date("Y").'</span>
					                                                   atendimento@cidadeaprendizagem.com.br
					                                                  <br>
					                                                </div>
					                                              </div></td>
					                                              <td valign="top" width="30" class="specbundle">&nbsp;</td>
					                                              <td valign="top" class="specbundle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					
					                                              </td>
					                                            </tr>
					                                          </tbody>
					                                        </table>
					                                      </td>
					                                    </tr>
					                                    <tr><td height="48"></td></tr>
					                                  </tbody>
					                                </table>
					
					                              </div>
					
					                              <!-- =============================== Rodapé ====================================== -->
					
					                            </td>
					                          </tr>
					                        </tbody>
					                      </table>
					                    </td>
					                    <td valign="top" width="40">&nbsp;</td>
					                  </tr>
					                </tbody>
					              </table>
					            </td>
					          </tr>
					        </tbody>
					      </table>
					    </td>
					  </tr>
					</tbody>
					</table>
					</body>
					</html>';
    }

    private function template_html($assunto, $mensagem, $email)
    {
        return '<!DOCTYPE html>
					<meta charset="UTF-8">
					<html lang="pt-br">
					<head>
					<meta charset="UTF-8">
					</head>
					<title></title>
					<style type="text/css">
					  body {
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    padding-top: 0 !important;
					    padding-bottom: 0 !important;
					    margin:0 !important;
					    width: 100% !important;
					    -webkit-text-size-adjust: 100% !important;
					    -ms-text-size-adjust: 100% !important;
					    -webkit-font-smoothing: antialiased !important;
					  }
					  .tableContent img {
					    border: 0 !important;
					    display: block !important;
					    outline: none !important;
					  }
					  a{
					    color:#382F2E;
					  }
					  table.social {
					    /*  padding:15px; */
					    background-color: #fff;
					
					  }
					  p.callout {
					    padding:15px;
					    background-color:#f6f6f6;
					    margin-bottom: 15px;
					  }
					  .callout a {
					    font-weight:bold;
					    color: #2BA6CB;
					  }
					  .social .soc-btn {
					    width: 100px;
					    padding: 1px 7px;
					    font-size:12px;
					    margin-bottom:5px;
					    text-decoration:none;
					    color: #FFF;font-weight:bold;
					    display:block;
					    text-align:center;
					  }
					  a.fb { background-color: #3B5998!important; }
					  a.tw { background-color: #1daced!important; }
					
					  p{
					    text-align:left;
					    color:#4f4f4f;
					    font-size:14px;
					    font-weight:normal;
					    line-height:19px;
					  }
					
					  a.link1{
					    color:#382F2E;
					  }
					  a.link2{
					    font-size:16px;
					    text-decoration:none;
					    color:#ffffff;
					  }
					
					  div,p,ul,h1{
					    margin:0;
					  }
					
					  .bgBody{
					    background: #ffffff;
					  }
					  .bgItem{
					    background: #ffffff;
					  }
					
					  @media only screen and (max-width:480px)
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					    td[class="left_pad"] 
					    {
					      padding-left:15px !important;
					      padding-right:15px !important;
					    }
					
					  }
					
					  @media only screen and (max-width:540px) 
					
					  {
					
					    table[class="MainContainer"], td[class="cell"] 
					    {
					      width: 100% !important;
					      height:auto !important; 
					    }
					    td[class="specbundle"] 
					    {
					      width:100% !important;
					      float:left !important;
					      font-size:13px !important;
					      line-height:17px !important;
					      display:block !important;
					      padding-bottom:15px !important;
					    }
					
					    td[class="spechide"] 
					    {
					      display:none !important;
					    }
					    img[class="banner"] 
					    {
					      width: 100% !important;
					      height: auto !important;
					    }
					  }
					
					</style>
					
					<body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
					  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center"  style="font-family:Helvetica, Arial,serif;">
					    <tbody>
					      <tr>
					        <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
					          <tbody>
					            <tr>
					              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tbody>
					                  <tr>
					                    <td valign="top" width="40">&nbsp;</td>
					                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tbody>
					                        <tr>
					                          <td height="-12" class="spechide"></td>
					
					                        </tr>
					                        <tr>
					                          <td class="movableContentContainer" valign="top">
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="35"></td>
					                                  </tr>
					                                  <tr>
					                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                      <tbody>
					                                        <tr>
					                                          <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                               <p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">IDM Cursos</span></strong> </p>
																	<p style="text-align:center;line-height: 34px; margin:0;font-family:Georgia,Time,sans-serif;font-size:18px;color:#005b95;"><strong><span class="font">'.$assunto.'</span></strong> </p>
					                                              </div>
					                                            </div></td>
					                                          </tr>
					                                        </tbody>
					                                      </table>
					                                    </td>
					                                  </tr>
					                                </tbody>
					                              </table>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					                                <tr>
					                                  <td align="left">
					                                  <br><br>
					                                    <div class="contentEditableContainer contentTextEditable">
					                                      <div class="contentEditable" align="center">
					                                        <p>
					                                          <p class="callout">'.$mensagem.'</p>
					                                        </p>
					                                      </div>
					                                    </div>
					                                  </td>
					                                </tr>
					
					                                <tr><td height="15"></td></tr>
					
					                                <tr>
					                                  <td align="center" >
					                                    <table>
					                                      <tr>
					                                        <td align="center" width="200px" bgcolor="#1A54BA" style="background:#005b95; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;">
					                                          <div class="contentEditableContainer contentTextEditable">
					                                            <div class="contentEditable" align="center">
					                                              <a target="_blank" href="'.base_url.'" class="link2" style="color:#ffffff;"><font color="#fff">Acessar Agora</font></a>
					                                            </div>
					                                          </div>
					                                        </td>
					                                      </tr>
					                                    </table>
					                                  </td>
					                                </tr>
					                                <tr><td height="10"></td></tr>
					                              </table>
					                              <br>
					                            </div>
					                            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
					                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                <tbody>
					                                  <tr>
					                                    <td height="15">
					                                    </tr>
					                                    <tr>
					                                      <td  style="border-bottom:1px solid #DDDDDD;"></td>
					                                    </tr>
					                                    <tr><td height="15"></td></tr>
					                                    <tr>
					                                      <td>
					                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                        <tbody>
					                                          <tr>
					                                            <td valign="top" class="specbundle">
					                                            <div class="contentEditableContainer contentTextEditable">
					                                              <div class="contentEditable">
					                                                <p  style="text-align:left;color:#4f4f4f;font-size:12px;font-weight:normal;line-height:20px;">
					                                                  <span style="font-weight:bold;">Copyright @ IDM Cursos '.date("Y").'</span>
					                                                   atendimento@cidadeaprendizagem.com.br
					                                                  <br>
					                                                </div>
					                                              </div></td>
					                                              <td valign="top" width="30" class="specbundle">&nbsp;</td>
					                                              <td valign="top" class="specbundle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					
					                                              </td>
					                                            </tr>
					                                          </tbody>
					                                        </table>
					                                      </td>
					                                    </tr>
					                                    <tr><td height="48"></td></tr>
					                                  </tbody>
					                                </table>
					
					                              </div>
					
					                              <!-- =============================== Rodapé ====================================== -->
					
					                            </td>
					                          </tr>
					                        </tbody>
					                      </table>
					                    </td>
					                    <td valign="top" width="40">&nbsp;</td>
					                  </tr>
					                </tbody>
					              </table>
					            </td>
					          </tr>
					        </tbody>
					      </table>
					    </td>
					  </tr>
					</tbody>
					</table>
					</body>
					</html>';
    }

    private function retirar_acentos($mensagem)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(ç)/","/(Ç)/"),explode(" ","a A e E i I o O u U n N c C"),$mensagem);
    }
    
    private function alterar_caracteres($mensagem)
    {
        return preg_replace(array("/(á)/", "/(à)/", "/(ã)/", "/(â)/", "/(Á)/", "/(À)/", "/(Ã)/", "/(Â)/", "/(é)/", "/(ê)/", "/(É)/", "/(Ê)/", "/(í)/", "/(Í)/", "/(ó)/", "/(õ)/", "/(ô)/", "/(Ó)/", "/(Õ)/", "/(Ô)/", "/(ú)/", "/(ü)/", "/(Ú)/", "/(Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"),
            explode(" ","&aacute; &agrave; &atilde; &acirc; &Aacute; &Agrave; &Atilde; &Acirc; &eacute; &ecirc; &Eacute; &Ecirc; &iacute; &Iacute; &oacute; &otilde; &ocirc; &Oacute; &Otilde; &Ocirc; &uacute; &uuml; &Uacute; &Uuml; &ntilde; &Ntilde; &ccedil; &Ccedil;"),$mensagem);
    }
}