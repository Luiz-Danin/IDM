<?php
class NotificacaoEmail
{
    private $db;
    
    public function __construct($conexao = null)
    {
        if($conexao instanceof PDO)
        {
            $this->db = $conexao;
        }
        elseif ($conexao==null) 
        {
            echo 'Falha em receber Objeto de conexão';
        }
    }
    
    public function notifica_credito()
    {
        $query = "SELECT t.id_transacao, a.nome, a.email, it.id_produto, t.id_transacao, t.data_transacao,
                        t.descricao, t.tipopagamento,it.valor, lt.cod_interme 
                    FROM transacao t
                    JOIN item_transacao it ON (t.id_transacao = it.transacao)
                    JOIN log_transacao lt ON (t.id_transacao = lt.transacao)
                    JOIN produto p ON (it.id_produto = p.id_produto)
                    JOIN aluno a ON (t.id_aluno = a.id_aluno)
                    WHERE a.email LIKE '%@%'  
                    AND  a.email NOT LIKE '% %' 
                    AND  a.email NOT LIKE '%;%'
                    AND t.situacao=4
                    AND it.id_produto IN  (7,8)
                    AND t.tipopagamento  LIKE '%cartao%'
                    AND t.id_transacao LIKE '%IDMC%'
                    AND date(t.data_transacao) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                    GROUP BY t.id_transacao
                    ORDER BY a.email, it.id_produto";
        
        $stmt = $this->db->query($query);
        return $stmt->fetchAll( PDO::FETCH_ASSOC );
    }

    public function notifica_debito()
    {
        $query = "SELECT t.id_transacao, a.nome, a.email, it.id_produto,t.id_transacao, t.data_transacao,
                        t.descricao, t.tipopagamento,it.valor, lt.cod_interme 
                    FROM transacao t
                    JOIN item_transacao it ON (t.id_transacao = it.transacao)
                    JOIN log_transacao lt ON (t.id_transacao = lt.transacao)
                    JOIN produto p ON (it.id_produto = p.id_produto)
                    JOIN aluno a ON (t.id_aluno = a.id_aluno)
                    WHERE a.email LIKE '%@%'  
                    AND  a.email NOT LIKE '% %' 
                    AND  a.email NOT LIKE '%;%'
                    AND t.situacao=2
                    AND it.id_produto IN  (7,8)
                    AND t.tipopagamento  LIKE '%debito%'
                    AND t.id_transacao LIKE '%IDMC%'
                    AND date(t.data_transacao) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                    GROUP BY t.id_transacao
                    ORDER BY a.email, it.id_produto";
        
        $stmt = $this->db->query($query);
        return $stmt->fetchAll( PDO::FETCH_ASSOC );
    }

    public function notifica_boleto()
    {
        $query = "SELECT t.id_transacao, a.nome, a.email, it.id_produto, t.id_transacao, t.data_transacao,
                        t.descricao, it.valor, lt.cod_interme FROM transacao t
                    JOIN item_transacao it ON (t.id_transacao = it.transacao)
                    JOIN log_transacao lt ON (t.id_transacao = lt.transacao)
                    JOIN produto p ON (it.id_produto = p.id_produto)
                    JOIN aluno a ON (t.id_aluno = a.id_aluno)
                    WHERE a.email LIKE '%@%'  
                    AND  a.email NOT LIKE '% %' 
                    AND  a.email NOT LIKE '%;%'
                    AND t.situacao=2
                    AND it.id_produto IN  (7,8)
                    AND t.tipopagamento  LIKE '%boleto%'
                    AND t.id_transacao LIKE '%IDMC%'
                    AND date(t.data_transacao) = DATE_SUB(CURDATE(), INTERVAL 3 DAY)
                    GROUP BY t.id_transacao
                    ORDER BY a.email, it.id_produto";
        
        $stmt = $this->db->query($query);
        return $stmt->fetchAll( PDO::FETCH_ASSOC );
    }

    public function matricula_sem_geracao_fatura()
    {
        $query = "SELECT a.id_aluno, a.nome, a.email, a.data_cadastro
                    FROM aluno a
                    LEFT JOIN transacao t ON (a.id_aluno = t.id_aluno)
                    JOIN aluno_unidade au  on (a.id_aluno = au.id_aluno)
                    JOIN aluno_instituicao ai ON (a.id_aluno = ai.id_aluno)
                    AND t.id_aluno IS NULL
                    AND ai.id_instituicao = 2
                    AND au.id_unidade =2
                    AND a.email like '%@%'  
                    AND  a.email not like '% %'
                    AND  a.email not like '%;%'
                    AND date(a.data_cadastro) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                    GROUP BY a.id_aluno
                    ORDER BY a.id_aluno DESC";
        
        $stmt = $this->db->query($query);
        return $stmt->fetchAll( PDO::FETCH_ASSOC );
    }
    
    public function curso_liberado()
    {
        $query = "SELECT a.nome, a.email,a.id_aluno, alc.id_curso, alc.id_instituicao_unidade
                    FROM aluno a  
                    JOIN aluno_turma al on(a.id_aluno=al.id_aluno) 
                    JOIN turma_ava ta on (ta.id_turma=al.id_turma) 
                    JOIN aluno_curso alc on(alc.id_aluno=a.id_aluno)   
                    WHERE alc.id_curso IN (635, 636) 
                    AND a.email like '%@%' 
                    AND  a.email not like '% %' 
                    AND a.email not like '%;%'
                    AND date(alc.primeiro_dia) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                    GROUP BY a.id_aluno";
        
        $stmt = $this->db->query($query);
        return $stmt->fetchAll( PDO::FETCH_ASSOC );
    }
}